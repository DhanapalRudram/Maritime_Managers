<div class="widget FeaturedPost" data-version="1" id="FeaturedPost1">

<h2 class="title">Featured Post</h2>

@foreach($featured as $fea)
<div class="post-summary">
<a href="#">
	<img class="image" src="{{$fea->blog_image}}"></a>
<h3><a href="#">{{$fea->title}}</a></h3>
<p>
    {{ str_limit($fea->description, $limit = 150, $end = '...') }}
</p>
<div class="continue_reading"><a href="#">Continue Reading</a></div>
</div>
@endforeach

<div class="clear"></div>
</div>
<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div id="product_container">
@foreach($datas as $data)   
     
<div class="post-outer">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="postbody">
  <img alt="#" class="post-image" src="{{$data->blog_image}}" height="400px" width="400px">
    <div class="jump-link"><a href="#"></a></div>
       <div class="top-label-home">
          <span class="post-labels">
            <a href="#" rel="tag">{{$data->category}}</a>
              </span>
                </div>
                  <h2 class="post-title entry-title-home" itemprop="headline">
                    <a href="/getBlog/{{$data->blog_id}}">{{$data->title}}</a>
                  </h2>
                <div id="summary2820770002208858471">
	         <div class="snippets">{{ str_limit($data->description, $limit = 150, $end = '...') }}
	      </div>
      </div>
  </div>
 <h3 class="top-vcard-home">
  <span class="post-author-vcard-home">
    <img src="assets/frontend/blogimg/LOGO 11.PNG">
      </span>
        <span class="post-timestamp">
          <abbr class="published" itemprop="datePublished">
	        <i class="fa fa-calendar"></i>April 13, 2016</abbr>
                </span>
              <a href="#" onclick="">
	       <i class="fa fa-comment-o" style="float: right; font-size: 35px; margin: 0px 4.3% 0px 0px;"><span class="comment-counter" style="font: 12px &quot;Montserrat&quot;,sans-serif ! important; color: #8a8a8a ! important; width: 35px; margin: 10px 0px 0px -35px; text-align: center;">0</span></i></a>
         </h3>
      <div style="clear: both;"></div>
	</div>
</div>
@endforeach
<div class="pagen">{{ $datas->links() }} </div>