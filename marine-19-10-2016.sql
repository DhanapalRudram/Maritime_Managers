-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2016 at 11:58 AM
-- Server version: 5.5.50-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `marine`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quiz_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_choice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `quiz_id`, `user_choice`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(15, '2', 'ThwVLc8M', 'php', 1, 1, '2016-10-15 08:26:56', '2016-10-15 08:26:56'),
(16, '3', 'ThwVLc8M', 'JAV', 1, 0, '2016-10-15 08:27:01', '2016-10-15 08:27:01');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `profession` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `author_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blog_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `blog_id`, `category`, `title`, `author_name`, `profession`, `author_image`, `blog_image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'q46YO7skri', 'DRAMA', 'Title 1', 'Author 1', 'Profession 1', 'uploads/blog/author/boat-large.jpg', 'uploads/blog/b1.jpg', 'This news release contains forward-looking statements within the meaning of the Private Securities Litigation Reform Act of 1995 (the “ACT”). In particular, when used in the preceding discussion, the words “estimated,” “believe,” “optimistic,” “expect,” a', 'Published', '2016-09-17 06:42:16', '2016-09-17 06:42:18'),
(2, 'AdZEzYuLiU', 'DRAMA', 'Title 2', 'Author 2', 'Profession 2', 'uploads/blog/author/2.jpg', 'uploads/blog/b2.jpg', 'This news release contains forward-looking statements within the meaning of the Private Securities Litigation Reform Act of 1995 (the “ACT”). In particular, when used in the preceding discussion, the words “estimated,” “believe,” “optimistic,” “expect,”', 'Published', '2016-09-17 06:42:43', '2016-09-17 06:42:45'),
(3, 'SANVR7hcqe', 'DRAMA', 'Title 3', 'Author 3', 'Profession 3', 'uploads/blog/author/3.jpg', 'uploads/blog/b3.jpg', 'This news release contains forward-looking statements within the meaning of the Private Securities Litigation Reform Act of 1995 (the “ACT”). In particular, when used in the preceding discussion, the words “estimated,” “believe,” “optimistic,” “expect,” a', 'Published', '2016-09-17 06:43:09', '2016-09-17 06:43:11'),
(4, 'TooLyGDWLe', 'DRAMA', 'Title 4', 'Author 4', 'Profession 4', 'uploads/blog/author/4.jpg', 'uploads/blog/b4.jpg', 'This news release contains forward-looking statements within the meaning of the Private Securities Litigation Reform Act of 1995 (the “ACT”). In particular, when used in the preceding discussion, the words “estimated,” “believe,” “optimistic,” “expect,” a', 'Published', '2016-09-17 06:43:41', '2016-09-23 05:27:54'),
(5, '0KeGGLe7PW', 'DRAMA', 'Title 5', 'Author 5', 'Profession 5', 'uploads/blog/author/boat-large.jpg', 'uploads/blog/b1.jpg', 'Headquartered in Blaine, WA, Hollund Industrial Marine Inc. (OTC PINK: HIMR) seeks to align the interests of businesses, communities, utilities and governments by offering an integrated business model for underwater forest management. Hollund’s model – in', 'Featured', '2016-09-17 06:49:39', '2016-09-17 06:49:47'),
(11, 'mpSSO0uxeq', 'DRAMA', 'Title 6', 'Author 6', 'Profession 6', 'uploads/blog/author/2.jpg', 'uploads/blog/b3.jpg', 'Headquartered in Blaine, WA, Hollund Industrial Marine Inc. (OTC PINK: HIMR) seeks to align the interests of businesses, communities, utilities and governments by offering an integrated business model for underwater forest management. Hollund’s model – in', 'Published', '2016-09-26 07:51:38', '2016-09-26 07:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `casestudy`
--

CREATE TABLE IF NOT EXISTS `casestudy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `casestudy_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `author_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `casestudy_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volume` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `casestudy`
--

INSERT INTO `casestudy` (`id`, `casestudy_id`, `category`, `author_name`, `author_image`, `title`, `casestudy_image`, `volume`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '9MSsff794m', 'CaseStudy', 'Author 1', 'uploads/casestudy/author/b1.jpg', 'Testing Title 1', 'uploads/casestudy/b1.jpg', '', 'Bootstrap, Foundation, or Materialize styles for StydeNet blade-pagination · GitHub PhillippOhlandt materializecss- laravel- paginatio Curso Laravel 5.1, Paginación - YouTube Manual Pagination Laravel - WordPress.com Laravel Packages - laravel-news.com La', 'Published', '2016-09-12 01:50:19', '2016-09-12 23:35:35'),
(3, '0cFZET2oF6', 'CaseStudy', 'Author 2', 'uploads/casestudy/author/b2.jpg', 'Testing Title 2', 'uploads/casestudy/b2.jpg', '', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, yo', 'Published', '2016-09-13 01:29:12', '2016-09-13 01:29:18'),
(4, 'S0fIIQklOB', 'CaseStudy', 'Author 3', 'uploads/casestudy/author/b4.jpg', 'Testing Title 3', 'uploads/casestudy/b4.jpg', '', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, yo', 'Published', '2016-09-13 01:29:43', '2016-09-13 01:29:44'),
(5, 'pnexUP13Fv', 'E-Books', 'Author 4', 'uploads/casestudy/author/b3.jpg', 'Testing Title 4', 'uploads/casestudy/pdf.pdf', 'Volume 1', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, yo', 'Published', '2016-09-13 02:34:47', '2016-09-13 02:34:48'),
(6, 'BR7Cv25Ygm', 'E-Books', 'Author 5', 'uploads/casestudy/author/b5.jpg', 'Testing Title 5', 'uploads/casestudy/b5.jpg', '', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, yo', 'Published', '2016-09-13 02:36:06', '2016-09-13 02:36:08'),
(7, 'jqSe7kISSY', 'E-Books', 'Author 6', 'uploads/casestudy/author/b3.jpg', 'Testing Title 6', 'uploads/casestudy/b3.jpg', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one ', 'Published', '2016-09-13 02:37:09', '2016-09-13 02:37:11'),
(8, 'lWpJTu62b9', 'E-Books', 'Author 7', 'uploads/casestudy/author/b2.jpg', 'Testing Title 7', 'uploads/casestudy/b2.jpg', '', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one ', 'Published', '2016-09-30 08:41:22', '2016-09-30 08:41:22'),
(11, 'pnexUP13Fn', 'Casestudy', 'Author 4', 'uploads/casestudy/author/b3.jpg', 'Testing Title 4', 'uploads/casestudy/b3.jpg', '', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, yo', 'Published', '2016-09-13 02:34:47', '2016-09-13 02:34:48');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `blog_id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'q46YO7skri', 'Dhanapal', 'dhana@gmail.com', 'hi very nice...', '2016-09-27 05:02:26', '2016-09-27 05:02:26'),
(2, 'AdZEzYuLiU', 'Dhanapal', 'dhana@gmail.com', 'hi', '2016-10-18 06:04:17', '2016-10-18 06:04:17');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `members_limit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tickets` int(20) NOT NULL,
  `event_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_id`, `title`, `company_image`, `location`, `start_date`, `end_date`, `price`, `members_limit`, `last_date`, `company_name`, `tickets`, `event_image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'R1mgrlGT2R', 'Testing Title', 'uploads/events/logo/images.png', 'Gopalapuram, Park Gate Road, ATT Colony, Coimbatore, Tamil Nadu 641018, India', '20 September 2016 - 10:00', '21 September 2016 - 10:00', '500', '50', '18 September 2016 - 10:00', 'google', 25, 'uploads/events/event2.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-09-17 00:58:31', '2016-09-23 08:00:30'),
(3, 'cmB5FHmf97', 'Testing Title1', 'uploads/events/logo/logo.jpg', 'Gopalapuram, Park Gate Road, ATT Colony, Coimbatore, Tamil Nadu 641018, India', '17 September 2016 - 10:10', '20 September 2016 - 12:00', '200', '50', '18 September 2016 - 10:00', 'Rudram Digital Agency', 10, 'uploads/events/event4.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."', 'Published', '2016-09-17 02:00:35', '2016-09-17 02:12:03'),
(5, 'GzjMzC3L3D', 'Testing Title3', 'uploads/events/logo/index.png', '5, Victoria Hostel Rd, Triplicane, Chennai, Tamil Nadu 600005, India', '25 September 2016 - 10:00', '20 September 2016 - 15:00', '500', '2', '18 September 2016 - 15:00', 'Marine', 25, 'uploads/events/event5.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided.', 'Recent', '2016-09-17 02:11:31', '2016-09-17 02:11:46'),
(6, 'DFiD349Ho9', 'Testing Title2', 'uploads/events/logo/IMG-20141022-WA0010.jpg', '5, Victoria Hostel Rd, Triplicane, Chennai, Tamil Nadu 600005, India', '25 September 2016 - 10:00', '26 September 2016 - 15:00', '1000', '2', '23 September 2016 - 15:00', 'google', 25, 'uploads/events/event6.jpg', 'The renovation was completed in 2011 and the old roofing with pillars that often blocked the view in the old stadium were replaced by light quad conical roofing held together by cables. The stadium can currently accommodate 38,000 spectators which will be expanded to 42,000. The stands are at a gradient of 36° and lets the sea breeze in to get the ground''s traditional swing back.[7]', 'Published', '2016-09-17 02:23:51', '2016-09-17 02:23:53'),
(7, 'oM6ROMNsku', 'Testing Title 4', 'uploads/events/logo/LOGO 1.PNG', 'Los Angeles, CA, United States', '20 October 2016 - 11:00', '21 October 2016 - 15:00', '100', '2', '18 October 2016 - 17:17', 'Testing Company1', 25, 'uploads/events/e1.jpg', '<p>"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', 'Published', '2016-10-05 06:21:03', '2016-10-05 06:22:29');

-- --------------------------------------------------------

--
-- Table structure for table `event_booking`
--

CREATE TABLE IF NOT EXISTS `event_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `location` varchar(500) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `institution_name` varchar(200) NOT NULL,
  `dob` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_slider`
--

CREATE TABLE IF NOT EXISTS `event_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(500) NOT NULL,
  `image1` varchar(500) NOT NULL,
  `image2` varchar(500) NOT NULL,
  `image3` varchar(500) NOT NULL,
  `image4` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `event_slider`
--

INSERT INTO `event_slider` (`id`, `image`, `image1`, `image2`, `image3`, `image4`, `created_at`, `updated_at`) VALUES
(1, 'uploads/events/slider/s1.jpg', 'uploads/events/slider/s2.jpg', 'uploads/events/slider/b3.jpg', 'uploads/events/slider/b4.jpg', 'uploads/events/slider/e1.jpg', '2016-10-13 05:03:26', '2016-10-13 05:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `jobportal`
--

CREATE TABLE IF NOT EXISTS `jobportal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vacancies` int(11) NOT NULL,
  `company_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `jobportal`
--

INSERT INTO `jobportal` (`id`, `job_id`, `company_name`, `category`, `title`, `vacancies`, `company_logo`, `company_image`, `description`, `start_date`, `end_date`, `location`, `link`, `status`, `created_at`, `updated_at`) VALUES
(2, '103TkyhqD0', 'Company 1', 'Experienced', 'Php Developer', 10, 'uploads/jobportal/logo/m2.jpg', 'uploads/jobportal/m3.jpg', '<p>"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', '30 September 2016 - 10:00', '05 October 2016 - 17:00', 'California, United States', '', 'Published', '2016-09-28 05:12:47', '2016-09-28 05:12:47'),
(4, 'blo624FgK5', 'Company 2', 'Experienced', 'Php Developer', 10, 'uploads/jobportal/logo/m1.png', 'uploads/jobportal/m4.jpg', '<p>"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', '30 September 2016 - 10:00', '05 October 2016 - 17:00', 'California, United States', '', 'Published', '2016-09-28 05:18:17', '2016-09-28 05:19:42'),
(5, '1Iw1fcPc6A', 'Company 3', 'Experienced', 'Maritime Managers', 4, 'uploads/jobportal/logo/m3.jpg', 'uploads/jobportal/b2.jpg', '<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains</p>', '10 October 2016 - 10:00', '15 October 2016 - 15:00', 'American Canyon, CA, United States', '', 'Pending', '2016-10-05 07:26:05', '2016-10-05 07:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_jobportal_table', 1),
('2014_10_12_000000_create_events_table', 2),
('2014_10_12_000000_create_quiz_table', 3),
('2014_10_12_000000_create_questions_table', 4),
('2014_10_12_000000_create_answers_table', 5),
('2014_10_12_000000_create_comments_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `quiz_id`, `question`, `option1`, `option2`, `option3`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(2, 'ThwVLc8M', 'what is php?', 'php', 'html', 'java', 'php', 'Published', '2016-09-21 12:41:43', '2016-09-21 12:41:43'),
(3, 'ThwVLc8M', 'what is JAVA?', 'html', 'css', 'JAVA', 'JAVA', 'Published', '2016-09-21 12:47:18', '2016-09-21 12:47:18'),
(4, 'Lk5WObYp', 'what is marine?', 'ship', 'sea', 'marine', 'marine', 'Published', '2016-09-27 05:07:49', '2016-09-27 05:07:49'),
(5, 'Lk5WObYp', 'What is Ship?', 'SHIP', 'ShIp', 'shIP', 'Ship', 'Published', '2016-09-27 05:08:55', '2016-09-27 05:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quiz_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_questions` int(11) NOT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_limit_questions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `quiz_id`, `category`, `title`, `quiz_image`, `no_of_questions`, `duration`, `time_limit_questions`, `description`, `status`, `created_at`, `updated_at`) VALUES
(3, 'ThwVLc8M', 'Html', 'Testing Title 2', 'public/uploads/quiz/q1.jpg', 40, '00:01:00', '1 Mins', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Published', '2016-09-21 13:20:50', '2016-09-21 13:20:52'),
(4, 'Lk5WObYp', 'Php', 'Testing Title 3', 'public/uploads/quiz/q2.jpg', 20, '01:00:00', '2mins', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Published', '2016-09-21 13:36:46', '2016-09-21 13:36:49');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_users`
--

CREATE TABLE IF NOT EXISTS `quiz_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `email` varchar(200) NOT NULL,
  `quiz_id` varchar(50) NOT NULL,
  `quiz_name` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `quiz_users`
--

INSERT INTO `quiz_users` (`id`, `name`, `email`, `quiz_id`, `quiz_name`, `created_at`, `updated_at`) VALUES
(1, 'Dhanapal', 'dhana@gmail.com', 'ThwVLc8M', 'Testing Title 2', '2016-10-15 07:48:51', '2016-10-15 07:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `speakershub`
--

CREATE TABLE IF NOT EXISTS `speakershub` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `speakers_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `profession` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `author_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `speakers_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `speakershub`
--

INSERT INTO `speakershub` (`id`, `speakers_id`, `category`, `title`, `location`, `link`, `author_name`, `profession`, `author_image`, `speakers_image`, `start_date`, `end_date`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dze6eeNfI3', 'Marine', 'Segments of Maritime Industry and its role', 'Coimbatore, Tamil Nadu, India', 'https://www.youtube.com/embed/T_byb0v03Fk', 'Kumaran .P', 'Sr. Manager', 'uploads/speakershub/author/Kumaran.JPG', 'uploads/speakershub/s2.jpg', '17 September 2016 - 15:03', '17 September 2016 - 15:15', 'Mr. Kumaran P has rich experience of 20 years in Maritime Industry. He started his career with EVERGREEN Line as Equipment Controller and promoted as Systems Manager in a span of 7 years.  \nHe then moved to DAMCO as Client coordinator SCM handling biggest retail giants like JCP, TESCO, and WALMART, with his strategic performance he has been promoted as Team Leader (Vendor Management) and Manager (Customer Service). \nHe has been offered a position in SAF MARINE as Regional Manager – Customer Service – West and North. \nCurrently, he is with Maersk Line as Sr. Manager – Customer Service. He is a super user of MS Excel and expert in Time Management. ', 'Published', '2016-09-17 06:37:32', '2016-09-17 06:37:34'),
(2, 'oeyFLzVD7O', 'Marine', 'Maritime Managers', 'Coimbatore, Tamil Nadu, India', '', 'Speaker 2', 'Profession 2', 'uploads/speakershub/author/a3.jpg', 'uploads/speakershub/s3.jpg', '17 September 2016 - 17:17', '17 September 2016 - 02:02', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."', 'Legend', '2016-09-17 06:38:28', '2016-09-17 06:40:35'),
(3, 'KXY8KXcjUa', 'Marine', 'Maritime Managers', 'Colorado Springs, CO, United States', '', 'Speaker 3', 'Profession 3', 'uploads/speakershub/author/a4.jpg', 'uploads/speakershub/s4.jpg', '15 September 2016 - 10:00', '20 September 2016 - 15:00', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."', 'Published', '2016-09-17 06:39:06', '2016-09-17 06:39:08'),
(4, 'AeZuCSbZfW', 'Marine', 'Maritime Managers', 'Chennai, Tamil Nadu, India', '', 'Speaker 4', 'Profession 4', 'uploads/speakershub/author/a1.jpg', 'uploads/speakershub/s1.jpg', '17 September 2016 - 17:00', '18 September 2016 - 11:00', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."', 'Published', '2016-09-17 06:39:48', '2016-09-17 06:39:50'),
(5, 'OSlz08MZbi', '', 'Testing Title 5', 'Los Angeles, CA, United States', '', 'Speaker5', 'Profession 5', 'uploads/speakershub/author/s6.jpg', 'uploads/speakershub/s5.jpg', '10 October 2016 - 11:00', '12 October 2016 - 11:11', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."', 'Published', '2016-10-05 06:30:33', '2016-10-05 06:30:38');

-- --------------------------------------------------------

--
-- Table structure for table `sub_events`
--

CREATE TABLE IF NOT EXISTS `sub_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` varchar(500) NOT NULL,
  `event_name` varchar(500) NOT NULL,
  `start_date` varchar(500) NOT NULL,
  `end_date` varchar(500) NOT NULL,
  `location` varchar(500) NOT NULL,
  `event_image` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sub_events`
--

INSERT INTO `sub_events` (`id`, `event_id`, `event_name`, `start_date`, `end_date`, `location`, `event_image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'oM6ROMNsku', 'Testing Title 11', '20 October 2016 - 10:00', '25 October 2016 - 10:00', 'Dubai - United Arab Emirates', 'uploads/events/subevents/e1.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-10-13 14:41:29', '2016-10-13 11:57:59'),
(2, 'oM6ROMNsku', 'Testing Title 12', '30 October 2016 - 10:00', '31 October 2016 - 17:00', 'American Fork, UT, United States', 'uploads/events/subevents/e1.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-10-13 14:41:35', '2016-10-13 11:59:30'),
(3, 'oM6ROMNsku', 'Testing Title 13', '25 October 2016 - 10:00', '27 October 2016 - 17:00', 'Long Island City, Queens, NY, United States', 'uploads/events/subevents/e1.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-10-13 14:41:38', '2016-10-13 12:00:33'),
(4, 'oM6ROMNsku', 'Testing Title 14', '27 October 2016 - 10:00', '29 October 2016 - 17:00', 'Los Angeles, CA, United States', 'uploads/events/subevents/e1.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-10-13 14:41:46', '2016-10-13 12:02:05'),
(5, 'R1mgrlGT2R', 'Testing Title 15', '15 October 2016 - 10:00', '20 October 2016 - 10:00', 'New York, NY, United States', 'uploads/events/subevents/e1.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-10-13 14:41:49', '2016-10-13 12:15:27'),
(6, 'DFiD349Ho9', 'Testing Title 20', '13 October 2016 - 18:18', '13 October 2016 - 18:00', 'Italy', 'uploads/events/subevents/e1.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-10-13 14:41:53', '2016-10-13 13:12:19'),
(7, 'cmB5FHmf97', 'Testing Title 30', '13 October 2016 - 14:14', '13 October 2016 - 14:14', 'Pakistan', 'uploads/events/subevents/e1.jpg', '"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal ', 'Published', '2016-10-13 14:42:00', '2016-10-13 13:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$/96F9xJ1654EiwrOhj6ty.Rxf1XqK2vP/JogPIOKVWQaR/1vSG6z.', 'jin2FYis77Ye4Pg1PBvP8atbTFMqVz1qKEiUMniwYTlyVuTuE7JBZEPkI6NP', '2016-08-17 19:01:29', '2016-10-17 13:47:47');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
