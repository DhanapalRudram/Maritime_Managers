@include('admin.includes.links')
<style>
	.a default
	{
		display:none;
	}
</style>
   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="/">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                            	<a href="/blog">Blog</a>
                                <span></span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn green btn-sm btn-outline dropdown-toggle" type="button" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="addBlog">
                                            <i class="fa fa-plus"></i> Add Blog</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
                        <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Blog </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                	<div class="mt-checkbox-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" checked="checked" id="publish"> Published
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" id="striped"> Draft
                                                <span></span>
                                            </label>
                                            
                                        </div>
                                     <p class="message"></p>   
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> Auth Name </th>
                                                <th> Author Image </th>
                                                <th> Title </th>
                                                <th> Blog Image </th>
                                                <th> Status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                         @if(count($datas)>0) 
                                         <?php $i=1;?>
                                        <tbody>
                                        	@foreach($datas as $data)
                                            <tr class="{{$data->blog_id}}">
                                            	
                                                <td> <?php echo $i;?></td>
                                                <td> {{$data->author_name}} </td>
                                                <td> <img src="public/{{$data->author_image}}" width="100px"> </td>
                                                <td> {{$data->title}} </td>
                                                <td> <img src="public/{{$data->blog_image}}" width="100px"> </td>
                                                <td> <span class="label label-sm label-success"> {{$data->status}} </span> </td>
                                                <td>
                                                	<input type="hidden" name="blog_id" value="{{$data->blog_id}}" id="blog_id"/>
                                                	<a class="btn btn-outline btn-circle green btn-sm purple" href="{{URL::to('editBlog',['id'=>$data->blog_id])}}">
                                                            <i class="fa fa-edit"></i> Edit </a>
                                                            
                                                     <a class="btn btn-outline btn-circle dark btn-sm black"  attr-id="{{$data->blog_id}}" href="javascript:;">
                                                            <i class="fa fa-trash-o"></i> Delete </a>
                                                    <a class="btn btn-outline btn-circle dark btn-sm setting"  attr-id="{{$data->blog_id}}" href="javascript:;">
                                                            <i class="icon-wrench"> Set us Featured Blog</i>       
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                            	@endforeach
                                        
                                         @else
											<tr><td colspan="7" align="center">Nodata </td></tr>
										 @endif
										 
                                       	
										 </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer')
       <script>
       	$(document).ready(function() {
			$('a.black').click(function(e) 
			{
				var id=$(this).attr("attr-id");
				e.preventDefault();
				var token = "{{ Session::getToken() }}";
		 		var blog_id =$("#blog_id").val();
				var parent = $(this).parent();
				
				$.ajax({
					type: 'delete',
					url:"{{URL::to('deleteBlog')}}",
					data:"_token="+token+"&blog_id="+id,
					beforeSend: function() {
						$("."+id).css("background-color","#fb6c6c");
					},
					success: function(data1) {
						parent.slideUp(300,function() {	
							if(data1==0)
							{								
								$("."+id).remove();
								$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
								$("#sample_2_info").html("Showing 0 entries");
							}
							else
							{
								$("."+id).remove();
								$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
							}
							
						});
					}
				});
			});
		});
		$("#striped").click(function(e)
		{
			var token = "{{ Session::getToken() }}";
			$.ajax({
					type: 'post',
					url:"{{URL::to('selectBlog')}}",
					data:"_token="+token,
					beforeSend: function() {
						
					},
					success: function(JSONObject) {
						 	var peopleHTML = "";
		var i=1;
			var data1=JSONObject.length;
      // Loop through Object and create peopleHTML  getting Data from json Response
	      for (var key in JSONObject) {
	      	i++;	
	        if (JSONObject.hasOwnProperty(key)) {
	          peopleHTML += "<tr>";
	           
	           peopleHTML += "<td>"+i+"</td>";
	           peopleHTML += "<td>" +JSONObject[key]["author_name"] + "</td></br>";
	           peopleHTML += "<td><img src=public/" +JSONObject[key]["author_image"] + " width='100px' ></td>";
	            peopleHTML += "<td>" +JSONObject[key]["title"] + "</td>";
	            peopleHTML += "<td><img src=public/" +JSONObject[key]["blog_image"] + " width='100px' ></td>";
	            peopleHTML += "<td><span class='label label-sm label-danger'> " +JSONObject[key]["status"] + "</span></td>";
	          peopleHTML += '<td><a class="btn btn-outline btn-circle green btn-sm purple" href="{{URL::to('editBlog',['id'=>'+JSONObject[key]["blog_id"] +'])}}"><i class="fa fa-edit"></i> Edit </a><a class="btn btn-outline btn-circle dark btn-sm black"  attr-id='+JSONObject[key]["blog_id"] +' href="javascript:;"><i class="fa fa-trash-o"></i> Delete </a> <a class="btn btn-outline btn-circle dark btn-sm setting"  attr-id="{{$data->blog_id}}" href="javascript:;"><i class="icon-wrench"> Set us Featured Blog</i>  </td>';
	          peopleHTML += "</tr>";
	        }		
	        																																																																																																																					
	      }									
	
	      // Replace table’s tbody html with peopleHTML
	       if(data1==0)
			{								
				$("#sample_2_paginate").hide();
				$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
				$("#sample_2_info").html("Showing 0 entries");
			}
			else
			{
				
				$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
			}
	      $("#sample_2 tbody").html(peopleHTML);
						}	
						});
		});
		$("#publish").click(function(e)
		{
			var token = "{{ Session::getToken() }}";
			$.ajax({
					type: 'post',
					url:"{{URL::to('selectallBlog')}}",
					data:"_token="+token,
					beforeSend: function() {
						
					},
					success: function(JSONObject) {
						 	var peopleHTML = "";
		var i=1;
			var data1=JSONObject.length;
      // Loop through Object and create peopleHTML  getting Data from json Response
	      for (var key in JSONObject) {
	        if (JSONObject.hasOwnProperty(key)) {
	          peopleHTML += "<tr>";
	           
	           peopleHTML += "<td>"+i+"</td>";
	           peopleHTML += "<td>" +JSONObject[key]["author_name"] + "</td></br>";
	           peopleHTML += "<td><img src=public/" +JSONObject[key]["author_image"] + " width='100px' ></td>";
	            peopleHTML += "<td>" +JSONObject[key]["title"] + "</td>";
	            peopleHTML += "<td><img src=public/" +JSONObject[key]["blog_image"] + " width='100px' ></td>";
	            peopleHTML += "<td><span class='label label-sm label-success'> " +JSONObject[key]["status"] + "</span></td>";
	           peopleHTML += '<td><a class="btn btn-outline btn-circle green btn-sm purple" href="{{URL::to('editBlog',['id'=>'+JSONObject[key]["blog_id"] +'])}}"><i class="fa fa-edit"></i> Edit </a><a class="btn btn-outline btn-circle dark btn-sm black"  attr-id='+JSONObject[key]["blog_id"] +' href="javascript:;"><i class="fa fa-trash-o"></i> Delete </a> <a class="btn btn-outline btn-circle dark btn-sm setting"  attr-id="{{$data->blog_id}}" href="javascript:;"><i class="icon-wrench"> Set us Featured Blog</i>  </td>';
	          peopleHTML += "</tr>";
	        }		
	        	i++;																																																																																																																					
	      }					
		 if(data1==0)
			{								
				$("#sample_2_paginate").hide();
				$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
				$("#sample_2_info").html("Showing 0 entries");
			}
			else
			{
				$("#sample_2_paginate").show();
				$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
			}
	      // Replace table’s tbody html with peopleHTML
	      $("#sample_2 tbody").html(peopleHTML);
						}	
						});
		});
// Featured Blog Part
	
		$("a.setting").click(function(e)
		{
			var token = "{{ Session::getToken() }}";
			var id=$(this).attr("attr-id");
			$.ajax({
					type: 'post',
					url:"{{URL::to('featuredBlog')}}",
					data:"_token="+token+"&&id="+id,
					beforeSend: function() {
						
					},
					success: function(JSONObject) {
						if(JSONObject=="0")
						{
						$(".message").html('Featured Blog Already Set').css("color","red");
						}	
						else {
							$(".message").html('Featured Blog Set Successfully').css("color","green");
                   		 setTimeout(function(){
 							window.location.href='/blog';
							}, 2000);					
							
						}
						}
						});
		});
       </script>
       