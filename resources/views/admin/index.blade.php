@include('admin.includes.links')
   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="/">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="/">Dashboard</a>
					</li>
				</ul>
				
			</div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			<h3 class="page-title">
			 <small></small>
			</h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-comments"></i>
						</div>
						<div class="details">
							<div class="number">
								 {{$blogs}}
							</div>
							<div class="desc">
								 Blogs
							</div>
						</div>
						<a class="more" href="/blog">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">
								 {{$speakers}}
							</div>
							<div class="desc">
								 Speakers
							</div>
						</div>
						<a class="more" href="/speakershub">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green-haze">
						<div class="visual">
							<i class="fa fa-shopping-cart"></i>
						</div>
						<div class="details">
							<div class="number">
								 {{$jobs}}
							</div>
							<div class="desc">
								 Jobs
							</div>
						</div>
						<a class="more" href="/jobportal">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat purple-plum">
						<div class="visual">
							<i class="fa fa-globe"></i>
						</div>
						<div class="details">
							<div class="number">
								 {{$events}}
							</div>
							<div class="desc">
								 Events
							</div>
						</div>
						<a class="more" href="/eventManager">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
			</div>

            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer')