@include('admin.includes.links')
   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="/">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                            	<a href="/jobportal">Quiz Report</a>
                                <span></span>
                            </li>
                        </ul>
                        <!--<div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn green btn-sm btn-outline dropdown-toggle" type="button" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="addJob">
                                            <i class="fa fa-plus"></i> Add Job</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>-->
                    </div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
                        <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Quiz Report Users List </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                	<div class="mt-checkbox-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" checked="checked" id="publish"> Published
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" id="striped"> Draft
                                                <span></span>
                                            </label>
                                            
                                        </div>
                                         <p class="message"></p>  
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> User Name</th>
                                                <th> Email </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                        	@foreach($datas as $data)
                                            <tr>
                                            	
                                                <td> {{$data->id}} </td>
                                                <td> {{$data->name	}} </td>
                                                <td> {{$data->email}} </td>
                                                
                                                 <td>     
                                                     <a class="btn btn-outline btn-circle dark btn-sm black"  attr-id="" href="javascript:;">
                                                            <i class="fa fa-trash-o"></i> Delete </a>       
                                                    <a href="/quizList/{{$data->email}}" class="btn dark btn-sm btn-outline sbold uppercase">
                                                            <i class="fa fa-share"></i> View </a>
                                                </td>
                                            </tr>
                                            	@endforeach
                                        
                                        
										 
                                       	
										 </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

            <!-- END QUICK SIDEBAR -->
            
       @include('admin.includes.footer')
      