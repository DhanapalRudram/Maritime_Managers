@include('admin.includes.links')
   @include('admin.includes.header')
     <?php $question_answer_id;$user_answer_id; ?>              
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="/">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                            	<a href="">Quiz View</a>
                                <span></span>
                            </li>
                        </ul>
                        <!--<div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn green btn-sm btn-outline dropdown-toggle" type="button" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="addJob">
                                            <i class="fa fa-plus"></i> Add Job</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>-->
                    </div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
                        <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Quiz Details </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body"> 
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> Quiz Name</th>
                                                <th> Category </th>
                                                <th> Total Questions </th>
                                            </tr>
                                        </thead>
                                        <?php $i=1;?>
                                        <tbody>
                                        	@foreach($datas as $data)
                                            <tr>
                                            	
                                                <td> 1 </td>
                                                <td> {{$data->title	}} </td>
                                                <td> {{$data->category}} </td>
                                                <td> {{$data->no_of_questions}} </td>
                                                
                                            </tr>
                                            	@endforeach
                                        
                                        
										 
                                       	
										 </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                        
               <!-- Question Details -->
               
                         <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Questions Details </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                       
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> Question Name</th>
                                                <th> options </th>
                                                <th>Correct Answer </th>
                                            </tr>
                                        </thead>
                                        <?php $i=1;?>
                                        <tbody>
                                        	@foreach($questions as $data)
                                        	
                                            <tr>
                                            	 
                                                <td> <?php echo $i;?> </td>
                                                <td> {{$data->question	}} </td>
                                                <td> {{$data->option1}}-{{$data->option2}}-{{$data->option3}} </td>
                                                <td> {{$data->answer}} </td>
                                                
                                            </tr>
                                            <?php $i++; ?>
                                            @endforeach
                                                
                                        
										 
                                       	
										 </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                        
                <!-- User Answer Details -->
                
                   <div class="col-md-12">
                
                            
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> User Result Detail </div>
                                    <div class="tools"> </div>
                                </div>
                                
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <h2> Score : <span style="border: 1px solid #000;border-radius: 135% !important;font-size: 20px;padding:19px;;color:#2592D0">{{$score}}/{{$count}}</span></h2>
                                        </br>
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> User Answer  </th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <?php $i=1;?>
                                        <tbody>
                                            
                                        	@foreach($results as $data)
                                            <tr>
                                                <td> <?php echo $i;?> </td>
                                                <td> {{$data->user_choice}} </td>
                                                <td> @if($data->status=='1')<img src="../public/assets/admin/blog/assets/img/success.png"> @else <img src="../public/assets/admin/blog/assets/img/error.png"> @endif</td>
                                            </tr>
                                            <?php $i++; ?>
                                            @endforeach
										 </tbody>
		
										 <!--<tr>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom" style="font-size:16px">
                                                     See Certificate</button>
                                            </div>
                                           
                                        </tr>-->
                                    </table>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>

            <!-- END QUICK SIDEBAR -->
            
       @include('admin.includes.footer')
      