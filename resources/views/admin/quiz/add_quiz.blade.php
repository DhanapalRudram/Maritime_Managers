@include('admin.includes.links')

<link rel="icon" type="image/png" href="public/assets/admin/blog/assets/img/favicon.ico">
<link href="public/assets/admin/blog/assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="public/assets/admin/blog/assets/css/gsdk-base.css" rel="stylesheet" />
<style>
	 .img{
		width:100px;
		height:100px;
	}
</style>
 
   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="/">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                                <span>Add Quiz</span>
                            </li>
                        </ul>
                        
                    </div>
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
<div class="image-container set-full-height" style="background-image: url('public/assets/admin/blog/assets/img/b2.jpg')">
    <!--   Creative Tim Branding   
    <a href="http://creative-tim.com">
         <div class="logo-container">
            <div class="logo">
                <img src="{{asset('assets/admin/magazine/img/new_logo.png')}}">
            </div>
            <div class="brand">
                Creative Tim
            </div>
        </div>
    </a>-->
    
    <!--   Big container   -->
    <div class="container">
        <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
           
            <!--      Wizard container        -->   
            <div class="wizard-container"> 
                
                <div class="card wizard-card ct-wizard-orange" id="wizardProfile">
                    
                <!--        You can switch "ct-wizard-orange"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->
                
                    	<div class="wizard-header">
                        	<h3>
                        	   <b>Fill</b> YOUR Company & Quiz Details <br>
                        	   <small>This information will let us know more about your Quiz.</small>
                        	</h3>
                    	</div>
                    	<ul>
                            <li><a href="#about" data-toggle="tab">Information</a></li>
                            <li><a href="#preview" data-toggle="tab">PREVIEW</a></li>
                            
                        </ul>
                        
                        <div class="tab-content">
                        	
                        		<!-- Megazine Form -->
                            <div class="tab-pane" id="about">
                            	<form class="megazine" method="post" enctype="multipart/form-data" id="episode">
                            	
                        		{{csrf_field()}}
                        		<input type="hidden" name="quiz_id" value="{{$quiz_id}}" id="quiz_id"/>
                        			
                              <div class="row">
                                  <h4 class="info-text"> </h4>
                                  <!-- <div class="col-sm-4 col-sm-offset-1">
                                    <div class="picture-container">
                                          <div class="picture">
                                              <img src="public/assets/admin/blog/assets/img/default-avatar.png" class="picture-src" id="wizardPicturePreview" title=""/>
                                              <input type="file" id="wizard-picture" name="image">
                                          </div>
                                          <h6>Choose Quiz Logo</h6>
                                      </div>
                                  </div>-->
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                        <label>Category</label><br>
                                             <select name="category" class="demo-htmlselect" id="category">
                                                <option value="Php">Php</option>
					                            <option value="Html">Html</option>
					                            
                                            </select>
                                      </div>
                                      <div class="form-group">
                                        <label> Title <small>(required)</small></label>
                                        <input name="title" type="text" class="form-control" placeholder="Quiz Title..." id="title">
                                      </div>
                                       <div class="form-group">
                                        <label>No.of Questions <small>(required)</small></label>
                                        <input name="questions" type="text" class="form-control" placeholder="No.of Questions..." id="questions">
                                      </div>
                                      <div class="form-group">
                                        <label>Total Duration <small>(required)</small></label>
                                        <input name="duration" type="text" class="form-control" placeholder="Total Duration..." id="duration">
                                      </div>
                                       <div class="form-group">
                                        <label>Duration Per Question <small>(required)</small></label>
                                        <input name="time_perquestion" type="text" class="form-control" placeholder="Duration Per Question..." id="time_perquestion">
                                      </div>
                                      <div class="form-group">
                                        <label>Quiz Event Image <small>(should be height-365px,width-1010px)</small></label>
                                        <input name="image" type="file" class="form-control"  id="image">
                                      </div>
                                  </div>
                                  
                                  
                                  <div class="col-sm-12 col-sm-offset-1" style="margin-left: 0;">
                                      <div class="form-group">
                                          <label>Quiz Description <small>(required)</small></label>
                                         <textarea cols="" rows="" id="summary" name="summary" style="width: 100%; height: 144px;"></textarea>
                                      </div>
                                  </div>
                                  
                              </div>
                              <div class="pull-right">
                                <input type='submit' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' id="Next" />
                                
                            </div>
                          </form>
                            </div>
                           <!-- Preview Form  -->
                           <div class="tab-pane" id="preview">                          
                                <div class="row">
                                <table id="people" border="1" style="width:100%;">
									  <thead>
									  								    
									    <th>Quiz Logo</th>
									    <th>Title</th>
									    <th>Interview Location</th>
									    <th>Description</th>
									  </thead>
									  <tbody>
									
									  </tbody>
								</table>
                              </div>
                              
                              <div class="pull-right">
                              	<input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                 <input type='button' class='btn btn-fill btn-default btn-wd btn-sm' name='Draft' value='Save as Draft' id="Draft" />
                                 <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Publish' id="Finish" />
        
                            </div>
                           
                            </div>
                        </div>
                        <div class="wizard-footer">                           
                            
                            <div class="pull-left">
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>	
                         <input type="hidden" name="doc_upload[]" id="doc_upload[]" value="">
                    
                </div>
            </div> <!-- wizard container -->
        </div>
        </div><!-- end row -->
    </div> <!--  big container -->
</div>


            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer') 
         
    <script src="public/assets/admin/blog/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="public/assets/admin/blog/assets/js/bootstrap.min.js" type="text/javascript"></script>
	  <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
    <script>
      tinymce.init({
      selector: 'textarea',
      height: 200,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools'
      ],
      toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
          ]
     });
    </script>	
	<!--   plugins 	 -->
	<script src="public/assets/admin/blog/assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
	
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="public/assets/admin/blog/assets/js/jquery.validate.min.js"></script>
	
    <!--  methods for manipulating the wizard and the validation -->
	<script src="public/assets/admin/blog/assets/js/wizard.js"></script>
	<script>
	
		 /// Megazine/Episode Part
  	$("form.megazine").submit(function(event){
//alert("dhana");
	  //disable the default form submission
	  event.preventDefault();
	 
	  //grab all form data  
	  var formData = new FormData($(this)[0]);
	 // alert(formData);
	  $.ajax({
	    url: "{{URL::to('quizUpload')}}",
	    type: "post",
	    data: formData,
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function (JSONObject) {
	    	var peopleHTML = "";
		
      // Loop through Object and create peopleHTML  getting Data from json Response
	      for (var key in JSONObject) {
	        if (JSONObject.hasOwnProperty(key)) {
	          peopleHTML += "<tr>";
	           peopleHTML += "<td><img src=" +JSONObject[key]["quiz_image"] + " width='100px' ></td>";
	           peopleHTML += "<td>" +JSONObject[key]["title"] + "</td>";
	           peopleHTML += "<td>" +JSONObject[key]["description"] + "</td>";
	          peopleHTML += "</tr>";
	        }																																																																																																																								
	      }									
	
	      // Replace table’s tbody html with peopleHTML
	      $("#people tbody").html(peopleHTML);
	    }
	  });																																																																																																																																																																																																																																																																																																																																																																																						
	 																																																																																																																																																																																																																																																																																																																											
	  return false;
	});
	
	$("#Draft").click(function(){
	   
        window.location.href="{{URL::to('quiz')}}";
            
	});														///Final Part 							
	
	$("#Finish").click(function(){
	     var token = "{{ Session::getToken() }}";
   	     var quiz_id =$("#quiz_id").val();// for documents id ,primary key
   	     $.ajax({
			        type: 'post',
			        url: "{{URL::to('quizPublish')}}",
			        data: "_token="+ token+"&quiz_id="+quiz_id,
			        success: function (returndata) {
            	      window.location.href="{{URL::to('quiz')}}";
            	    }
			        
		    	});
		    	
	});
	</script>