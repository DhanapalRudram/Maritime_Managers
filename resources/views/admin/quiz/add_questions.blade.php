@include('admin.includes.links')
   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                                <span>Datatables</span>
                            </li>
                        </ul>
                       <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn green btn-sm btn-outline dropdown-toggle" type="button" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="addQuiz">
                                            <i class="fa fa-plus"></i> Add Quiz Details</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
                        <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Quiz </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                
                                         <p class="message"></p>  
                                        <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">

                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <button id="sample_editable_1_new" class="btn green"> Add New
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                  
                                        </div>
                                    </div>
                                    
                                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                
                                                <th> Question </th>
                                                <th> Option 1 </th>
                                                <th> Option 2 </th>
                                                <th> Option 3 </th>
                                                <th> Answer </th>
                                                <th> Edit </th>
                                                <th> Delete </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                           
                                           
                                        	@foreach($datas as $data)
                                          <tr>  
                                                
                                                <td>{{$data->question}}  </td>
                                                <td> {{$data->option1}} </td>
                                                <td> {{$data->option2}} </td>
                                                <td> {{$data->option3}} </td>
                                                <td> {{$data->answer}} </td>
                                                <td>
                                                    <a class="edit" id="edit_Data" attr-x="{{$data->id}}" href="javascript:;"> Edit </a>
                                                </td>
                                                <td>
                                                    <a class="delete" href="javascript:;"> Delete </a>
                                                </td>
                                            </tr>
                                           
                                          <input type="hidden" name="quiz_id" value="{{$data->quiz_id}}" id="quiz_id"/>
                                            @endforeach
                                          
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer')
       <script>
       var TableDatatablesEditable = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow,question_id) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
           
            //jqTds[7].innerHTML = '<input type="hidden"  class="form-control input-small question" value="' + i + '">';
            jqTds[0].innerHTML = '<input type="text"  class="form-control input-small question" value="' + aData[0] + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small op1" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small op2" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<input type="text" class="form-control input-small op3" value="' + aData[3] + '">';
            jqTds[4].innerHTML = '<input type="text" class="form-control input-small ans" value="' + aData[4] + '">';
            jqTds[5].innerHTML = '<a class="edit" attr-x="'+question_id+'" href="">Save</a>';
            jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';
            
            
        }
        
        function saveRow(oTable, nRow,question_id) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
            oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
            oTable.fnDraw();
            
            
                    var question=jqInputs[0].value;
                    var op1=jqInputs[1].value;
                    var op2=jqInputs[2].value;
                    var op3=jqInputs[3].value;
                    var ans=jqInputs[4].value;
                	var token = "{{ Session::getToken() }}";
                	var quiz_id =$("#quiz_id").val();
                
                	if(question_id=="undefined")
                	{
                	$.ajax({
    					type: 'post',
    					url:"{{URL::to('questionsInsert')}}",
    					data:"_token="+token+"&question="+question+"&op1="+op1+"&op2="+op2+"&op3="+op3+"&ans="+ans+"&quiz_id="+quiz_id,
    					beforeSend: function() {
    					
    					},
    					success: function(data1) {
    					    
    					}
    				});
                	}
                	else
                	{
                	$.ajax({
					type: 'post',
					url:"{{URL::to('questionsUpdate')}}",
					data:"_token="+token+"&question="+question+"&op1="+op1+"&op2="+op2+"&op3="+op3+"&ans="+ans+"&quiz_id="+quiz_id+"&question_id="+question_id,
					beforeSend: function() {
					
					},
					success: function(data1) {
					    
					}
				});  
                	}
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
            oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1');

        var oTable = table.dataTable({

            
            "lengthMenu": [
                [6, 15, 20, -1],
                [6, 15, 20, "All"] // change per page values here
            ],

           
            // set the initial value
            "pageLength": 6,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new').click(function (e) {
            //alert("hi");
            e.preventDefault();
            var question_id="undefined";
            if (nNew && nEditing) {
                if (confirm("Previous row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing,question_id); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '','']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow,question_id);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            nNew = false;
            var question_id=$(this).attr("attr-x");
            //alert(question_id);
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing,question_id);
                nEditing = null;
                
                // Save Part
               
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                
                editRow(oTable, nRow,question_id);
                nEditing = nRow;
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable.init();
});
</script>
      