@include('admin.includes.links')
   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                                <span>Datatables</span>
                            </li>
                        </ul>
                       <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn green btn-sm btn-outline dropdown-toggle" type="button" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="addQuiz">
                                            <i class="fa fa-plus"></i> Add Quiz Details</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
                        <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Quiz </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                	<div class="mt-checkbox-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" checked="checked" id="publish"> Published
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" id="striped"> Draft
                                                <span></span>
                                            </label>
                                            
                                        </div>
                                         <p class="message"></p>  
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> Title </th>
                                                <th> Total Questions </th>
                                                <th> Category </th>
                                                <th> Status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <?php $i=1;?>
                                        <tbody>
                                        	 @if(count($datas)>0) 
                                        	@foreach($datas as $data)
                                            <tr class="{{$data->blog_id}}">
                                            	
                                                <td> <?php echo $i;?> </td>
                                                <td> {{$data->title}} </td>
                                                <td> {{$data->no_of_questions}} </td>
                                                <td> {{$data->category}} </td>
                                                <td> <span class="label label-sm label-success"> {{$data->status}} </span> </td>
                                                <td>
                                                
                                                	<input type="hidden" name="quiz_id" value="{{$data->quiz_id}}" id="quiz_id"/>
                                                
                                                     <a class="btn btn-outline btn-circle dark btn-sm black"  attr-id="{{$data->quiz_id}}" href="javascript:;">
                                                            <i class="fa fa-trash-o"></i> Delete </a>       
                                                     <a class="btn btn-outline btn-circle dark btn-sm setting"  attr-id="{{$data->quiz_id}}" href="javascript:;">
                                                            <i class="icon-plus"> Add Questions</i> </a> 
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                            	@endforeach
                                        
                                         @else
											<tr><td colspan="8" align="center">Nodata </td></tr>
										 @endif
										 
                                       	
										 </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer')
       <script>
       	$(document).ready(function() {
			$('a.black').click(function(e) 
			{
				var id=$(this).attr("attr-id");
				e.preventDefault();
				var token = "{{ Session::getToken() }}";
		 		var quiz_id =$("#quiz_id").val();
				var parent = $(this).parent();
				
				$.ajax({
					type: 'delete',
					url:"{{URL::to('deleteQuiz')}}",
					data:"_token="+token+"&quiz_id="+id,
					beforeSend: function() {
						$("."+id).css("background-color","#fb6c6c");
					},
					success: function(data1) {
						parent.slideUp(300,function() {	
							if(data1==0)
							{								
								$("."+id).remove();
								$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
								$("#sample_2_info").html("Showing 0 entries");
							}
							else
							{
								$("."+id).remove();
								$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
							}
							
						});
					}
				});
			});
		});
		$("#striped").click(function(e)
		{
			var token = "{{ Session::getToken() }}";
			$.ajax({
					type: 'post',
					url:"{{URL::to('selectdraftjob')}}",
					data:"_token="+token,
					beforeSend: function() {
						
					},
					success: function(JSONObject) {
						 	var peopleHTML = "";
						 	
		var i=1;
      // Loop through Object and create peopleHTML  getting Data from json Response
    	//alert(JSONObject.length);
    	var data1=JSONObject.length;
	      for (var key in JSONObject) {
	      	i++;	
	        if (JSONObject.hasOwnProperty(key)) {
	          peopleHTML += "<tr>";
	           
	           peopleHTML += "<td>"+i+"</td>";
	           peopleHTML += "<td>" +JSONObject[key]["company_name"] + "</td></br>";
	           peopleHTML += "<td>" +JSONObject[key]["location"] + "</td>";
	            peopleHTML += "<td>" +JSONObject[key]["start_date"] + "</td>";
	             peopleHTML += "<td>" +JSONObject[key]["title"] + "</td>";
	            peopleHTML += "<td>" +JSONObject[key]["end_date"] + "</td>";
	            peopleHTML += "<td><span class='label label-sm label-danger'> " +JSONObject[key]["status"] + "</span></td>";
	            peopleHTML += '<td><a class="btn btn-outline btn-circle dark btn-sm black deleteSpeakers"  attr-id='+JSONObject[key]["quiz_id"] +' href="javascript:;"><i class="fa fa-trash-o"></i> Delete </a> <a class="btn btn-outline btn-circle dark btn-sm setting"  attr-id="{{$data->quiz_id}}" href="javascript:;"><i class="icon-plus"> Add Questions</i></td>';
	          peopleHTML += "</tr>";
	        }		
	        																																																																																																																					
	      }									
	
	      // Replace table’s tbody html with peopleHTML
	      if(data1==0)
			{								
				$("#sample_2_paginate").hide();
				$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
				$("#sample_2_info").html("Showing 0 entries");
			}
			else
			{
				$("#sample_2_paginate").show();
				$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
			}
	      $("#sample_2 tbody").html(peopleHTML);
						}	
						});
		});
		$("#publish").click(function(e)
		{
			var token = "{{ Session::getToken() }}";
			$.ajax({
					type: 'post',
					url:"{{URL::to('selectallJob')}}",
					data:"_token="+token,
					beforeSend: function() {
						
					},
					success: function(JSONObject) {
					var peopleHTML = "";
		var i=1;
      // Loop through Object and create peopleHTML  getting Data from json Response
      	var data1=JSONObject.length;
	      for (var key in JSONObject) {
	        if (JSONObject.hasOwnProperty(key)) {
	          peopleHTML += "<tr>";
	           
	           peopleHTML += "<td>"+i+"</td>";
	           peopleHTML += "<td>" +JSONObject[key]["company_name"] + "</td></br>";
	           peopleHTML += "<td>" +JSONObject[key]["location"] + "</td>";
	           peopleHTML += "<td>" +JSONObject[key]["start_date"] + "</td>";
	           peopleHTML += "<td>" +JSONObject[key]["title"] + "</td>";
	           peopleHTML += "<td>" +JSONObject[key]["end_date"] + "</td>";
	           peopleHTML += "<td><span class='label label-sm label-success'> " +JSONObject[key]["status"] + "</span></td>";
	           peopleHTML += '<td><a class="btn btn-outline btn-circle dark btn-sm black deleteSpeakers"  attr-id='+JSONObject[key]["quiz_id"] +' href="javascript:;"><i class="fa fa-trash-o"></i> Delete </a><a class="btn btn-outline btn-circle dark btn-sm setting"  attr-id="{{$data->quiz_id}}" href="javascript:;"><i class="icon-plus"> Add Questions</i> </td>';
	          peopleHTML += "</tr>";
	        }		
	        	i++;																																																																																																																					
	      }									
	
	      // Replace table’s tbody html with peopleHTML
	      if(data1==0)
			{								
				$("#sample_2_paginate").hide();
				$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
				$("#sample_2_info").html("Showing 0 entries");
			}
			else
			{
				$("#sample_2_paginate").show();
				$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
			}
	      $("#sample_2 tbody").html(peopleHTML);
			}	
			});
		});
       </script>
       <script>
       	$('#sample_2 tbody').on('click', 'a.deleteSpeakers', function(e) {
    		var id=$(this).attr("attr-id");
				e.preventDefault();
				var token = "{{ Session::getToken() }}";
		 		var quiz_id =$("#quiz_id").val();
				var parent = $(this).parent();
					$.ajax({
					type: 'delete',
					url:"{{URL::to('deleteJob')}}",
					data:"_token="+token+"&quiz_id="+id,
					beforeSend: function() {
						$("."+id).css("background-color","#fb6c6c");
					},
					success: function(data1) {
						parent.slideUp(300,function() {	
							if(data1==0)
							{								
								$("."+id).remove();
								$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
								$("#sample_2_info").html("Showing 0 entries");
							}
							else
							{
								$("."+id).remove();
								$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
							}
							
						});
					}
				});
		});
		
		$('#sample_2 tbody').on('click', 'a.editJob', function(e) {
				var id=$(this).attr("attr-id");
				//alert("{{URL::to('editJob',['id'=>" +id+ "])}}");
				e.preventDefault();
				 window.location.href="editJob/"+id;
		});
		// Create Legend Part		
		$("a.setting").click(function(e)
		{
		//	var token = "{{ Session::getToken() }}";
			var id=$(this).attr("attr-id");
			 window.location.href="addQuestions/"+id;
		});
       </script>