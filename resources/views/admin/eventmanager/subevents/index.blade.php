@include('admin.includes.links')
   @include('admin.includes.header')

			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="/">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                            	 <a href="/eventManager">Event Manager</a>
                                <span></span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn green btn-sm btn-outline dropdown-toggle" type="button" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        
                                        <a href="/addSubEvent/{{$id}}">
                                            <i class="fa fa-plus"></i> Add SubEvent</a>
                                        
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			
			<!-- END PAGE HEADER-->
			
			
			<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
                        <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Event Manager </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                	<div class="mt-checkbox-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" checked="checked" id="publish"> Published
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="filter" id="striped"> Draft
                                                <span></span>
                                            </label>
                                            
                                        </div>
                                         <p class="message"></p> 
                                          
                                   <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> Event Name</th>
                                                <th> Location </th>
                                                <th> Start Date </th>
                                                <th> End Date </th>
                                                <th> Status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <?php $i=1;?>
                                        <tbody>
                                        	@foreach($events as $event)
                                            <tr>
                                            	
                                                <td> <?php echo $i;?> </td>
                                                <td> {{$event->event_name}} </td>
                                                <td> {{$event->location}} </td>
                                                <td> {{$event->start_date}} </td>
                                                <td> {{$event->end_date}} </td>
                                                <td> <span class="label label-sm label-success"> {{$event->status}} </span> </td>
                                                <td>
                                                 
                                                     <a class="btn btn-outline btn-circle dark btn-sm black"  attr-id="{{$event->id}}" href="javascript:;">
                                                            <i class="fa fa-trash-o"></i> Delete </a>       
                                                    
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                            	@endforeach
                                        
                                        
										 
                                       	
										 </tbody>
                                    </table>
                                   
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer')
          <script>
       	$('#sample_2 tbody').on('click', 'a.black', function(e) {
    		var id=$(this).attr("attr-id");
				e.preventDefault();
				var token = "{{ Session::getToken() }}";
		 		var event_id =$("#event_id").val();
				var parent = $(this).parent();
								$.ajax({
					type: 'delete',
					url:"{{URL::to('deletesubEvent')}}",
					data:"_token="+token+"&id="+id,
					beforeSend: function() {
						$("."+id).css("background-color","#fb6c6c");
					},
					success: function(data1) {
						parent.slideUp(300,function() {	
							if(data1==0)
							{								
								$("."+id).remove();
								$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
								$("#sample_2_info").html("Showing 0 entries");
							}
							else
							{
								$("."+id).remove();
								$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
							}
							
						});
					}
				});
		});
		
	
       </script>