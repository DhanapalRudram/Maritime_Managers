@include('admin.includes.links')


   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="/">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                                <span>Add SubEvent</span>
                            </li>
                        </ul>
                        
                    </div>
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
<div class="image-container set-full-height" style="background-image: url('public/assets/admin/blog/assets/img/b2.jpg')">
    <div class="portlet light bordered">
                                           
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="/subeventUpload" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                        		<input type="hidden" name="event_id" value="{{$event_id}}" id="event_id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Event Title</label>
                            <div class="col-md-4">
                                <input name="title" type="text" class="form-control" placeholder="Event Title..." id="title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Event Location</label>
                            <div class="col-md-4">
                                <input name="location" type="text"  class="form-control" placeholder="Event Location..." id="location"> 
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-md-3 control-label">Start Date & Time</label>
                            <div class="col-md-4">
                                <input name="sdate" type="text" class="form-control date-picker input-daterange" placeholder="Date & Time..." id="sdate">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">End Date & Time</label>
                            <div class="col-md-4">
                                <input name="edate" type="text" class="form-control date-picker input-daterange" placeholder="Date & Time..." id="edate">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Event Image (should be height-365px,width-1010px) </label>
                            <div class="col-md-4">
                                <input name="event_image" type="file" class="form-control"  id="event_image">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-1">
                                      <div class="form-group">
                                          <label>Event Description <small>(required)</small></label>
                                         <textarea cols="" rows="" id="summary" name="summary" style="width: 100%; height: 144px;"></textarea>
                                      </div>
                                  </div>
                        </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-4">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
</div>


            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer') 
         
    <script src="public/assets/admin/blog/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="public/assets/admin/blog/assets/js/bootstrap.min.js" type="text/javascript"></script>
	  <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
    <script>
      tinymce.init({
      selector: 'textarea',
      height: 200,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools'
      ],
      toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
          ]
     });
      $(".date-picker").datetimepicker({minDate: 0,isRTL:App.isRTL(),format:"dd MM yyyy - hh:ii",autoclose:!0,todayBtn:!0,minuteStep:1});
    </script>	