@include('admin.includes.links')
   @include('admin.includes.header')
                   
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="/">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            <li>
                            	 <a href="/eventManager">Event Manager</a>
                                <span></span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn green btn-sm btn-outline dropdown-toggle" type="button" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="/addEventSlider">
                                            <i class="fa fa-plus"></i> Add Slider</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong>{{ Session::get('flash_message') }}</strong> 
			</div>
	      @endif
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
                        <div class="col-md-12">
                
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i> Event Slider </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                	
                                         <p class="message"></p> 
                                          @if(count($datas)>0)  
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                            	
                                                <th> S.No</th>
                                                <th> Image 1</th>
                                                <th> Image 2 </th>
                                                <th> Image 3 </th>
                                                <th> Image 4 </th>
                                                <th> Image 5 </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                        	@foreach($datas as $data)
                                            <tr>
                                            	
                                                <td>{{$data->id}} </td>
                                                <td> <img src="public/{{$data->image}}" width="100px" height="100px"></td>
                                                <td> <img src="public/{{$data->image1}}" width="100px" height="100px"> </td>
                                                <td> <img src="public/{{$data->image2}}" width="100px" height="100px"> </td>
                                                <td> <img src="public/{{$data->image3}}" width="100px" height="100px"></td>
                                                <td> <img src="public/{{$data->image4}}" width="100px" height="100px"></span> </td>
                                                <td>
                                                	<a class="btn btn-outline btn-circle dark btn-sm black"  attr-id="{{$data->id}}" href="javascript:;">
                                                            <i class="fa fa-trash-o"></i> Delete </a>       
                                                     
                                                </td>
                                            </tr>
                                            	@endforeach
                                        
                                         @else
											<tr><td colspan="8" align="center">Nodata </td></tr>
										 @endif
										 
                                       	
										 </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

            <!-- END QUICK SIDEBAR -->
       @include('admin.includes.footer')

       <script>
       	$('#sample_2 tbody').on('click', 'a.black', function(e) {
    		var id=$(this).attr("attr-id");
				e.preventDefault();
				var token = "{{ Session::getToken() }}";
		 		var event_id =$("#event_id").val();
				var parent = $(this).parent();
								$.ajax({
					type: 'delete',
					url:"{{URL::to('deleteEventSlider')}}",
					data:"_token="+token+"&id="+id,
					beforeSend: function() {
						$("."+id).css("background-color","#fb6c6c");
					},
					success: function(data1) {
						parent.slideUp(300,function() {	
							if(data1==0)
							{								
								$("."+id).remove();
								$("#sample_2 tbody").html("<tr><td colspan='8'>Nodata </td></tr>");
								$("#sample_2_info").html("Showing 0 entries");
							}
							else
							{
								$("."+id).remove();
								$("#sample_2_info").html("Showing 1 to "+data1+" of "+data1+" entries");
							}
							
						});
					}
				});
		});
		
	
       </script>