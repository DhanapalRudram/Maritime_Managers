<title>Marine-About</title>
@include('frontend.blog.includes.links')
 <link rel="stylesheet" href="public/assets/frontend/event/css/forms.css" type="text/css">
      <link rel="stylesheet" href="public/assets/frontend/event/css/template.css" type="text/css">
<style>
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    background-color: #fff;
    color: #435469;
}
.btn:hover, .btn:focus {
    color: #fff;
    text-decoration: none;
    box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
    background-color: #435469;
  
}
.btn-theme {
    background-color: #2592d0;
    
    color: #fff;
    font-size: 14px;
    font-weight: 700;
    line-height: 1;
    padding: 15px 35px;
    text-transform: uppercase;
    transition: all 0.2s ease-in-out 0s;
    width: 227px;
}
.Label1 li {
    background: none;
    margin: 0 0 1px;
    padding: 5px 0;
}
.Label1 a {
    color: #333;
    font: 700 34px "Raleway",sans-serif;
    letter-spacing: 1px;
    padding: 5px 20px 5px 20px;
    text-transform: uppercase;
}
.Label2 a {
    color: #333;
    font: 700 14px "Raleway",sans-serif;
    letter-spacing: 1px;
    padding: 5px 20px 5px 20px;
    text-transform: uppercase;
}

.row {
  margin:5px;
}
.main.section {
    float: left;
    max-width: 695px;
    width: 100%;
}
.iconic-list li
{
    float:left;
    width:auto;
    
}
#sidebar {
    color: #515151;
    float: right;
    font: 13px/1.6 "Droid Serif",serif;
    max-width: 370px;
    text-align: center;
    width: 100%;
}
iframe {
    border: 5px solid #ccc;
    box-shadow: 1px 4px 5px #ccc;
    height: 887px;
    margin-left: 9px;
    width: 96%;
}

</style>
@include('frontend.blog.includes.header')

<!--<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
	<img draggable="false" src="public/assets/frontend/blogimg/12.jpg" width="100%" height="100%">
				
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>-->
<h1 align="center" style="margin-top: -50px;margin-bottom: 50px;text-transform:uppercase;">About Us</h1>
<div id="contentwrapper">
<div id="wrapper">
<div class="main section" id="main">

       <div id="product_container">

<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed casestudy">
   
   <div class="date-outer">
      <div class="date-posts">


<div class="post-outer1">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
	
	   <p style="padding-top:20px;line-height:40px;">
	   "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"
	     </p>
		
	 </div>
   
</div>
	
</div>



</div>

 </div>
</div>
      
</div>


</div></div>
<aside id="sidebar">
<div class="sidebar section" id="sidebar-widget"><div class="widget HTML" data-version="1" id="HTML1">

<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget Label" data-version="1" id="Label1">
<h2>About Us</h2>
<div class="widget-content">
	
<div class="about-me"><img alt="About Me" src="public/assets/frontend/blogimg/sailboat-water-stockholm-ship-large.jpg" style="width:100%;height:47%;">

</div>

</div>
</div>
</div>
</aside>
<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
<!--<script>
	$('form.contact_form').submit(function(event){
		 event.preventDefault();
		var token = "{{ Session::getToken() }}";
		var blog_id=$('#blog_id').val();
		var name=$('#name').val();
		var email=$('#email').val();
		var message=$('#message').val();
		$.ajax({
			        type: 'post',
			        url: "{{URL::to('sendmail')}}",
			        data: "_token="+ token+"&name="+name+"&email="+email+"&message="+message,
			        success: function (data) {
			        	if(data=='success')
			        	{
			        		$(".message1").html('Mail Send Successfully').css("color","green");
            	      		$("#Label1").load(location.href + " #Label1>*", "");
			        	}
            	      	else
            	      	{
            	      		$(".message1").html('Error').css("color","red");
            	      	}
            	    }
			        
		    	});
	
	});
</script>