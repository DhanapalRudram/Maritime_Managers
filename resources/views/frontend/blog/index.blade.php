<title>Marine-Blog</title>

@include('frontend.blog.includes.links')
<style>
.pagen
{
	width:100%;
	float:left;
}
.pagination
{
	
	float:right;
	margin-top:10px;
	margin-bottom:10px;
	
}
.pagination li
{
	
	display: inline-block;
	float:left;
	}
	.pagination li.disabled span
{
	//border-right:1px solid #ccc;
	padding:10px;
	font-size:18px;
	float:left;
	background:#ccc;
	margin-right:4px;
	}
	.pagination li.active span
{
	//border-right:1px solid #2592D0;
	padding:10px;
	margin-right:4px;
	font-size:18px;
	float:left;
	background:#2592D0;
	color:#fff;
	}
	.pagination li a
{
	//border-right:1px solid #ccc;
	padding:10px;
	font-size:18px;
	margin-right:2px;
	float:left;
	background:#fff;
	text-decoration: none;
	}
	.pagination li:hover a,.pagination li:hover span
	{
		background:#2592D0 !important;
		color:#fff;
	}
	#header-inner {
    background: #f6f6f6 none repeat scroll 0 0;
    padding: 6px 0 40px;
}
</style>
@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
	@foreach($featured as $fea)
	<img draggable="false" src="public/{{$fea->blog_image}}" style="width:100%;height:550px;">
	@endforeach			
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper">
<div id="wrapper">
	
<div class="main section" id="main">
<div class="widget FeaturedPost" data-version="1" id="FeaturedPost1">

<h2 class="title">Featured Post</h2>

@foreach($featured as $fea)
<div class="post-summary">
<a href="/getBlog/{{$fea->blog_id}}">
	<img class="image" src="public/{{$fea->blog_image}}" style="width: 100%;height: 300px;position:relative;" ></a>
<h3><a href="#">{{$fea->title}}</a></h3>
<p>
    {{ str_limit($fea->description, $limit = 150, $end = '...') }}
</p>
<div class="continue_reading"><a href="/getBlog/{{$fea->blog_id}}">Continue Reading</a></div>
</div>
@endforeach

<div class="clear"></div>
</div>
       @include('frontend.blog.presult')
</div>

 </div>
</div>
      
</div>


</div></div>

<aside id="sidebar">
<div class="sidebar section" id="sidebar-widget"><div class="widget HTML" data-version="1" id="HTML1">
<h2 class="continue_reading">About Us</h2>
<div class="widget-content">
	@foreach($featured as $fea)
<div class="about-me"><img alt="About Me" src="public/{{$fea->author_image}}">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.... <a href="#">Read more</a>

</div>
@endforeach
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget Label" data-version="1" id="Label1">
<h2>Categories</h2>
<div class="widget-content list-label-widget-content">
<ul>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">1</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">7</span>
</li>
<li>
<a dir="ltr" href="#">Category</a>
<span dir="ltr">1</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">1</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">6</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">1</span>
</li>
</ul>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
</div>
<div class="widget HTML" data-version="1" id="HTML5">
<h2 class="title">Latest Posts</h2>
<div class="widget-content">
<div id="recent-posts">
<ul class="recent_posts_with_thumbs"><li class="clearfix">
	<a href="#">
		<img class="recent_thumb" src="public/assets/frontend/blogimg/port-boat-ship-black-large.jpg" height="150" width="150"></a>
		<a href="#">Where does it come from?</a><br>
		<div class="at-posts">May 21, 2016</div>
		</li>
		<li class="clearfix">
			<a href="#">
				<img class="recent_thumb" src="public/assets/frontend/blogimg/pexels-photo-large.jpg" height="150" width="150"></a>
			<a href="#">Where does it come from?</a>
			<br><div class="at-posts">April 13, 2016</div></li>
			<li class="clearfix">
				<a href="#">
			<img class="recent_thumb" src="public/assets/frontend/blogimg/sailboat-water-stockholm-ship-large.jpg" height="150" width="150"></a>
			<a href="#">Where does it come from?</a><br>
			<div class="at-posts">April 13, 2016</div>
			</li></ul>
			<div class="clear"></div>
			</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="public/assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget HTML" data-version="1" id="HTML7">
<h2 class="title">Space For Ad</h2>
<div class="widget-content">
<!--<iframe scrolling="no" allowtransparency="true" style="border:none; overflow:hidden; width:300px; height:185px;" src="></iframe>-->
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="public/assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
</aside>
<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
<script>
 $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getData(page);
            }
        }
    });
$(document).ready(function()
{
     $(document).on('click', '.pagination a',function(event)
    {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl = $(this).attr('href');
       var page=$(this).attr('href').split('page=')[1];
       getData(page);
    });
});
function getData(page){
        $.ajax(
        {
            url: '?page=' + page,
            type: "get",
            datatype: "html",
            // beforeSend: function()
            // {
            //     you can show your loader 
            // }
        })
        .done(function(data)
        {
            console.log(data);
            
            $("#product_container").empty().html(data);
            location.hash = page;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
}
  </script>