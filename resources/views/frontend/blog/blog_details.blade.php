<title>Marine-Blog</title>
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../public/assets/frontend/event/css/forms.css" type="text/css">
<link rel="stylesheet" href="../public/assets/frontend/event/css/template.css" type="text/css">
<style>
	.overlay_te
	 {
	 	position: absolute;
		top: 35%;
		color: rgb(255, 255, 255);
		font-size: 60px;
		text-align: center;
		width: 100%;
	 }
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    background-color: #fff;
    color: #435469;
}
input, textarea {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #cdcdcd;
    border-radius: 3px;
    color: #333;
    font: 300 12px "Raleway",sans-serif;
    height: 41px;
    padding: 0 15px;
    position: relative;
    text-overflow: ellipsis;
    text-shadow: none;
    width: 45%;
}
.btn:hover, .btn:focus {
    color: #fff;
    text-decoration: none;
    box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
    background-color: #435469;
  
}
.btn-theme {
    background-color: #2592d0;
    
    color: #fff;
    font-size: 14px;
    font-weight: 700;
    line-height: 1;
    padding: 15px 35px;
    text-transform: uppercase;
    transition: all 0.2s ease-in-out 0s;
    width: 227px;
}
.row {
  margin:5px;
}
.pagen
{
	width:100%;
	float:left;
}
.pagination
{
	
	float:right;
	margin-top:10px;
	margin-bottom:10px;
	
}
.pagination li
{
	
	display: inline-block;
	float:left;
	}
	.pagination li.disabled span
{
	//border-right:1px solid #ccc;
	padding:10px;
	font-size:18px;
	float:left;
	background:#ccc;
	margin-right:4px;
	}
	.pagination li.active span
{
	//border-right:1px solid #2592D0;
	padding:10px;
	margin-right:4px;
	font-size:18px;
	float:left;
	background:#2592D0;
	color:#fff;
	}
	.pagination li a
{
	//border-right:1px solid #ccc;
	padding:10px;
	font-size:18px;
	margin-right:2px;
	float:left;
	background:#fff;
	text-decoration: none;
	}
	.pagination li:hover a,.pagination li:hover span
	{
		background:#2592D0 !important;
		color:#fff;
	}
	#header-inner {
    background: #f6f6f6 none repeat scroll 0 0;
    padding: 18px 0 30px;
}
</style>
@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
  @foreach($datas as $data)   
	<img draggable="false" src="../public/{{$data->blog_image}}" style="width:100%;height:455px;">
  
	@endforeach			
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper">
<div id="wrapper">
<div class="main section" id="main">

       
<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div id="product_container">
@foreach($datas as $data)   
     <input type="hidden" name="blog_id" id="blog_id" value="{{$data->blog_id}}"/>
<div class="post-outer1">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="postbody">
  
    <div class="jump-link"><a href="#"></a></div>
      
                <div class="post-summary">
                	<h2 style="margin-top: 10px;" align="center">{{$data->title}}</h2>
	<p style="padding-top: 20px;font-size: 14px;line-height: 40px;"> {{$data->description}}</p></div>
  </div>
 <h3 class="top-vcard-home">
  <span class="post-author-vcard-home">
    <img src="../public/{{$data->author_image}}">
      </span>
        <span class="post-timestamp">
          <abbr class="published" itemprop="datePublished">
	        <i class="fa fa-calendar"></i>{{$data->created_at}}</abbr>
                </span>
              <a href="#" onclick="">
	        <i class="fa fa-comment-o" style="float: right; font-size: 35px; margin: 0px 4.3% 0px 0px;"><span class="comment-counter" style="font: 12px &quot;Montserrat&quot;,sans-serif ! important; color: #8a8a8a ! important; width: 35px; margin: 10px 0px 0px -35px; text-align: center;">{{count($comments)}}</span></i>
	       </a>
         </h3>
      <div style="clear: both;"></div>
	</div>
</div>
@endforeach



</div><!--<h2>Leave a comment</h2>
<p class="message1"></p>
<form id="contact_form" method="post" enctype="multipart/form-data" class="contact_form" >
			{{csrf_field()}}
  <div class="row">
    
    <input id="name" class="input" name="name" type="text" value="" placeholder="Your Name" required /><br />
    <span id="name_validation" class="error_message"></span>
  </div>
  <div class="row">
   
    <input id="email" class="subscribe-email" name="email" type="text" value="" placeholder="Your Email Address" required/><br />
    <span id="email_validation" class="error_message"></span>
  </div>
  <div class="row">
    
    <textarea id="message" class="input" name="message" rows="7" cols="30" style="width: 329px; height: 101px;" placeholder="Your Message"required ></textarea><br />
    <span id="message_validation" class="error_message"></span>
  </div>
    
    <input style="float: left;width: 100px !important;text-align: center;padding: 0px;" class="btn btn-theme subscribe-form-submit wysija-submit wysija-submit-field" value="Send!" type="submit">
</form>

@foreach($comments as $comment)
	<div class="post-summary">
	<p style="padding-top: 20px;font-size: 14px;line-height: 40px;background:#86c5fa;"><b>{{$comment->name}}:</b>{{$comment->message}} </p></div>
	
@endforeach
<div class="pagn">{{$comments->links()}}</div>-->
 </div>
</div>
      
</div>


</div></div>
@foreach($datas as $data)
<aside id="sidebar">
<div class="sidebar section" id="sidebar-widget"><div class="widget HTML" data-version="1" id="HTML1">
<h2 class="continue_reading">About Us</h2>
<div class="widget-content">
<div class="about-me"><img alt="About Me" src="../public/{{$data->author_image}}">

</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget Label" data-version="1" id="Label1">
<h2>Categories</h2>
<div class="widget-content list-label-widget-content">
<ul>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">1</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">7</span>
</li>
<li>
<a dir="ltr" href="#">Category</a>
<span dir="ltr">1</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">1</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">6</span>
</li>
<li>
<a dir="ltr" href="#">Category 1</a>
<span dir="ltr">1</span>
</li>
</ul>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
</div>
<div class="widget HTML" data-version="1" id="HTML5">
<h2 class="title">Latest Posts</h2>
<div class="widget-content">
<div id="recent-posts">
<ul class="recent_posts_with_thumbs"><li class="clearfix">
	<a href="#">
		<img class="recent_thumb" src="../public/assets/frontend/blogimg/port-boat-ship-black-large.jpg" height="150" width="150"></a>
		<a href="#">Where does it come from?</a><br>
		<div class="at-posts">May 21, 2016</div>
		</li>
		<li class="clearfix">
			<a href="#">
				<img class="recent_thumb" src="../public/assets/frontend/blogimg/pexels-photo-large.jpg" height="150" width="150"></a>
			<a href="#">Where does it come from?</a>
			<br><div class="at-posts">April 13, 2016</div></li>
			<li class="clearfix">
				<a href="#">
			<img class="recent_thumb" src="../public/assets/frontend/blogimg/sailboat-water-stockholm-ship-large.jpg" height="150" width="150"></a>
			<a href="#">Where does it come from?</a><br>
			<div class="at-posts">April 13, 2016</div>
			</li></ul>
			<div class="clear"></div>
			</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../public/assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget HTML" data-version="1" id="HTML7">
<h2 class="title">Space For Ad</h2>
<div class="widget-content">
<!--<iframe scrolling="no" allowtransparency="true" style="border:none; overflow:hidden; width:300px; height:185px;" src="></iframe>-->
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../public/assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
</aside>
@endforeach
<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
<script>
	$('form.contact_form').submit(function(event){
		 event.preventDefault();
		var token = "{{ Session::getToken() }}";
		var blog_id=$('#blog_id').val();
		var name=$('#name').val();
		var email=$('#email').val();
		var message=$('#message').val();
		$.ajax({
			        type: 'post',
			        url: "{{URL::to('insertComments')}}",
			        data: "_token="+ token+"&blog_id="+blog_id+"&name="+name+"&email="+email+"&message="+message,
			        success: function (data) {
			        	if(data=='success')
			        	{
			        		$(".message1").html('Comment Sumitted Successfully').css("color","green");
            	      		$(".post-summary").load(location.href + " .post-summary>*", "");
			        	}
            	      	else
            	      	{
            	      		$(".message1").html('Error').css("color","red");
            	      	}
            	    }
			        
		    	});
	
	});
</script>