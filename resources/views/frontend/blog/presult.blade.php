<style>
  .post-summary h3 {
    font: 700 31px "Raleway",sans-serif;
    letter-spacing: -1px;
    margin: 0 0 20px;
    padding: 0px 1.1%;
    text-align: center;
}
.continue_reading {
    font: 400 11px "Raleway",sans-serif;
    letter-spacing: 1px;
    margin: 10px 0 10px 00px;
    text-align: center;
    text-transform: uppercase;
}

</style>

<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div id="product_container">
@foreach($datas as $data)   
     
<div class="post-outer">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="postbody">
  <img alt="#" class="post-image" src="public/{{$data->blog_image}}" height="400px" width="400px">
    <div class="jump-link"><a href="/getBlog/{{$data->blog_id}}"></a></div>
       
                  <h2 class="post-title entry-title-home" itemprop="headline" style="padding-left:10px;">
                    <a href="/getBlog/{{$data->blog_id}}">{{$data->title}}</a>
                  </h2>
                <div id="summary2820770002208858471" style="padding-left: 13px;padding-top: 10px;line-height: 30px;">
	         
	      <div class="post-body">
            <div class="post-excerpt">
                {{ str_limit($data->description, $limit = 140) }}<a href="/getBlog/{{$data->blog_id}}" style="color:#2592D0;">Read more</a>
            </div>
        </div>
      </div>
  </div>
 <h3 class="top-vcard-home">
  <span class="post-author-vcard-home">
    <img src="public/{{$data->author_image}}" alt="{{$data->author_name}}">
      </span>
        <span class="post-timestamp">
          <abbr class="published" itemprop="datePublished">
	        <i class="fa fa-calendar"></i>April 13, 2016</abbr>
                </span>
              <a href="#" onclick="">
	       <i class="fa fa-comment-o" style="float: right; font-size: 35px; margin: 0px 4.3% 0px 0px;"><span class="comment-counter" style="font: 12px &quot;Montserrat&quot;,sans-serif ! important; color: #8a8a8a ! important; width: 35px; margin: 10px 0px 0px -35px; text-align: center;">0</span></i></a>
         </h3>
      <div style="clear: both;"></div>
	</div>
</div>
@endforeach

<div class="pagen">{{ $datas->links() }} </div>