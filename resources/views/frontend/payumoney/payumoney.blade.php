<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "fB7m8s";

// Merchant Salt as provided by Payu
$SALT = "eRis5Chv";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in/_payment";

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<title>Marine-Payu Form</title>
@include('frontend.blog.includes.links')
 <link rel="stylesheet" href="public/assets/frontend/event/css/forms.css" type="text/css">
      <link rel="stylesheet" href="public/assets/frontend/event/css/template.css" type="text/css">
<style>
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    background-color: #fff;
    color: #435469;
}
.btn:hover, .btn:focus {
    color: #fff;
    text-decoration: none;
    box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
    background-color: #435469;
  
}
.btn-theme {
    background-color: #2592d0;
    
    color: #fff;
    font-size: 14px;
    font-weight: 700;
    line-height: 1;
    padding: 15px 35px;
    text-transform: uppercase;
    transition: all 0.2s ease-in-out 0s;
    width: 227px;
}
.Label1 li {
    background: none;
    margin: 0 0 1px;
    padding: 5px 0;
}
.Label1 a {
    color: #333;
    font: 700 34px "Raleway",sans-serif;
    letter-spacing: 1px;
    padding: 5px 20px 5px 20px;
    text-transform: uppercase;
}
.Label2 a {
    color: #333;
    font: 700 14px "Raleway",sans-serif;
    letter-spacing: 1px;
    padding: 5px 20px 5px 20px;
    text-transform: uppercase;
}

.row {
  margin:5px;
}
.main.section {
    float: left;
    max-width: 695px;
    width: 100%;
}
.iconic-list li
{
    float:left;
    width:auto;
    
}
#sidebar {
    color: #515151;
    float: right;
    font: 13px/1.6 "Droid Serif",serif;
    max-width: 370px;
    text-align: center;
    width: 100%;
}
iframe {
    border: 5px solid #ccc;
    box-shadow: 1px 4px 5px #ccc;
    height: 887px;
    margin-left: 9px;
    width: 96%;
}
.b-form_box_field input[type="text"], .b-form_box_field input[type="password"], .b-form_box_field textarea, .b-form_box_field select, .b-form_box_field .e-select {
    position: relative;
    padding: 0 15px;
    width: 40%;
    height: 42px;
    font: 300 12px 'Raleway', sans-serif;
    text-overflow: ellipsis;
    text-shadow: none;
    color: #333;
    background: #fff;
    border: 1px solid #cdcdcd;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
</style>
 <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      
      var payuForm = document.forms.payuForm;
      //alert(payuForm);return false;
      payuForm.submit();
    }
  </script>
@include('frontend.blog.includes.header')
<body onload="submitPayuForm()">
<h1 align="center" style="margin-top: -50px;margin-bottom: 50px;text-transform:uppercase;">payu form</h1>
<div id="contentwrapper" style="height:510px;">
<div id="wrapper">
<div class="main section" id="main">
                    	 <!-- Sign Up section -->
      <section class="b-signUp b-section b-section__background" id="signUp">
          <div class="b-section__background_overlay">

              <div class="b-section_container col-xs-12">
                  <div class="b-signUp_form b-form">
                    <?php if($formError) { ?>
	
                      <span style="color:red;font-size:16px;">Please fill all mandatory fields.</span>
                      <br/>
                      <br/>
                    <?php } ?>
                    
                     <form action="<?php echo $action; ?>" method="post" name="payuForm">
                       {{csrf_field()}}
                        <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                        <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                        <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
                       
                        <table style="font-size:16px;">
                            
                            <tr>
                              <td>Amount: </td>
                              <td><input name="amount" value="<?php echo (empty($posted['amount'])) ? $price : $posted['amount'] ?>" type="text" /></td>
                              <td>First Name: </td>
                              <td><input type="text" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? $fname : $posted['firstname'] ?>" /></td>
                            </tr>
                            <tr>
                              <td>Email: </td>
                              <td><input type="text" name="email" id="email" value="<?php echo (empty($posted['email'])) ? $email : $posted['email'] ?>" /></td>
                              <td>Phone: </td>
                              <td><input type="text" name="phone" value="<?php echo (empty($posted['phone'])) ? $phone : $posted['phone'] ?>" /></td>
                            </tr>
                            <tr>
                             
                              <td colspan="3"><input type="hidden" name="productinfo" size="64" value="<?php echo (empty($posted['productinfo'])) ? $event_name : $posted['productinfo'] ?>" /></td>
                            </tr>
                            <tr>
                             
                              <td colspan="3"><input type="hidden" name="surl" value="https://maritimemanagers-dhanapalrandy.c9users.io/paymentsuccess" size="64" /></td>
                            </tr>
                            <tr>
                              
                              <td colspan="3"><input type="hidden" name="furl" value="https://maritimemanagers-dhanapalrandy.c9users.io/paymentfailure" size="64" /></td>
                            </tr>
                    
                            <tr>
                              <td colspan="3"><input type="hidden" name="service_provider" value="payu_paisa" size="64" /></td>
                            </tr>

                         <!-- <tr>
                            <td><b>Optional Parameters</b></td>
                          </tr>
                          <tr>
                            <td>Last Name: </td>
                            <td><input name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></td>
                            <td>Cancel URI: </td>
                            <td><input name="curl" value="" /></td>
                          </tr>
                          <tr>
                            <td>Address1: </td>
                            <td><input name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></td>
                            <td>Address2: </td>
                            <td><input name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
                          </tr>
                          <tr>
                            <td>City: </td>
                            <td><input name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></td>
                            <td>State: </td>
                            <td><input name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></td>
                          </tr>
                          <tr>
                            <td>Country: </td>
                            <td><input name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></td>
                            <td>Zipcode: </td>
                            <td><input name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></td>
                          </tr>
                          <tr>
                            <td>UDF1: </td>
                            <td><input name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" /></td>
                            <td>UDF2: </td>
                            <td><input name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" /></td>
                          </tr>
                          <tr>
                            <td>UDF3: </td>
                            <td><input name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" /></td>
                            <td>UDF4: </td>
                            <td><input name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" /></td>
                          </tr>
                          <tr>
                            <td>UDF5: </td>
                            <td><input name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></td>
                            <td>PG: </td>
                            <td><input name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></td>
                          </tr>-->
                          
                          <tr>
                            <?php if(!$hash) { ?>
                              <td colspan="4"><div class="b-signUp_form_bottom b-form_bottom b-form_bottom__center">
                  </br>
                              <button type="submit" id="tickets1" class="btn btn-theme animated flipInY visible" data-label="Get ticket"><span>Buy Now </span> <i class="fa fa-arrow-circle-right"></i></button>
                              <!--<a href="#"  class="btn btn-theme animated flipInY visible" data-animation="flipInY" data-animation-delay="300" data-label="Get ticket">BUY Now <i class="fa fa-arrow-circle-right"></i></a>-->

                          </div>
                             </td>
                            <?php } ?>
                          </tr>
                        </table>
                       
                        </form>
                  </div>

              </div>
          </div>
      </section>
</div>

<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
