 <!-- FOOTER -->
    <footer id="footer" style="background-image: url('public/assets/frontend/img/footer.png');background-size: 100% 300%;
background-repeat: no-repeat;margin-top: -254px;">

<!-- Main Footer -->
<div id="main-footer" class="smallest-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3">
				<div id="text-2" class="widget widget_text">
					<div class="textwidget">
						<img src="public/assets/frontend/img/LOGO 1.PNG" alt="logo" width="80px">
							</br>
							<ul class="iconic-list" style="margin-top: 20px;">
							<li>
							<i class="icons icon-location-7"></i>
							Phoenix Inc.<br>
							 PO Box 21177 <br>
							 Little Lonsdale St, Melbourne <br>
							 Victoria 8011 Australia </li>
							<li>
							<i class="icons icon-mobile-6"></i>
							Phone: (415) 124-5678 <br>
							 Fax: (415) 124-5678 </li>
							<li>
							<i class="icons icon-mail-7"></i>
							support@yourname.com </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<div id="latest-posts-3" class="widget widget_latest_posts_entries">
					<h4>Menu</h4>
						<ul class="iconic-list">
						    <li class="tooltip-ontop" title="Quiz"><a href="/Quiz">Mock-Test </a></li>
							<li class="tooltip-ontop" title="Contact us"><a href="/Contact">Contact Us </a></li>
							<li class="tooltip-ontop" title="Faq"><a href="/Faq">Faq</a></li>
							<li class="tooltip-ontop" title="Google Plus"><a href="#">SiteMap</a></li>
							
						</ul>
				
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<div id="text-4" class="widget widget_text">
					<h4>Social</h4>
					<div class="textwidget">
						<ul class="iconic-list">
							<li class="tooltip-ontop" title="Facebook"><a href="#">Facebook  <i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
							<li class="tooltip-ontop" title="Twitter"><a href="#">Twitter &nbsp;&nbsp; <i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
							<li class="tooltip-ontop" title="Skype"><a href="#">Skype  <i class="fa fa-skype" aria-hidden="true"></i></a></li>
							<li class="tooltip-ontop" title="Google Plus"><a href="#">Google+  <i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
							<li class="tooltip-ontop" title="Linkedin"><a href="#">Linkedin  <i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<div id="wysija-3" class="widget widget_wysija">
					<h4>Subscribe to our Newsletter</h4>
					<p class="message1"></p>
					<div class="widget_wysija_cont">
						<p>By subscribing to our mailing list you will get the latest news from us.</p>
						<form class="subscribe-form" action="/subscribe" method="post">
							{{ csrf_field()}}
							<div class="b-form_box_field i-icon i-ion-ios-telephone ">
                                 <input  type="text" name="email" id="email" placeholder="Your Email Address" required>
                           </div><br>
                             <input class="btn btn-theme subscribe-form-submit wysija-submit wysija-submit-field" type="submit" value="Subscribe!">
                            <label class="subscribe-form-result"></label>
                        </form>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- /Main Footer -->
<!-- Lower Footer -->
<div id="lower-footer">
	<div class="container">
		<span class="copyright">© 2014 Marine. All Rights Reserved <span> Design by <a>Rudram Digital Agency</a></span></span>
	</div>
</div>
<!-- /Lower Footer -->
</footer>
    <!-- /FOOTER -->

    <div class="to-top" style="bottom: -100px;"><i class="fa fa-angle-up"></i></div>

</div>
<!-- /Wrap all content -->
<!-- Overlay Search -->

<div class="overlay_search">
    <div class="container">
        <div class="row">
            <div class="form_search-wrap">
            	<div class="Modal-body">
		<div class="row">
			<div class="col-md-12">
				<h2>Filter</h2>
			</div>
		</div>

		<div class="row" style="margin-top: 1.5rem;">
			<div class="col-md-8">

				
				<nav class="FilterNavigation"><ul id="menu-insights" class=""><li><a>Topics</a>
<ul class="sub-menu">
	<li class="current-menu-item"><a href="#">All</a></li>
	<li><a href="#">Engagement</a></li>
	<li><a href="#">Publishing</a></li>
	<li><a href="#">Analytics</a></li>
	<li><a href="#">Advocacy</a></li>
	<li><a href="#">Product Updates</a></li>
	<li><a href="#">Team Sprout</a></li>
	<li><a href="#">Engineering</a></li>
	<li><a href="#">Español</a></li>
	<li><a href="#">Português</a></li>
</ul>
</li>
<li><a>Networks</a>
<ul class="sub-menu">
	<li><a href="#">Facebook</a></li>
	<li><a href="#">Twitter</a></li>
	<li><a href="#">Instagram</a></li>
	<li><a href="#">LinkedIn</a></li>
	<li><a href="#">Google+</a></li>
</ul>
</li>
<li><a>Resources</a>
<ul class="sub-menu">
	<li><a href="#">All</a></li>
	<li><a href="#">Data</a></li>
	<li><a href="#">Guides</a></li>
	<li><a href="#">Case Studies</a></li>
	<li><a href="#">Infographics</a></li>
	<li><a href="#">Webinars</a></li>
</ul>
</li>
<li><a>Community</a>
<ul class="sub-menu">
	<li><a href="#">Events</a></li>
	<li><a href="#">Press</a></li>
	<li><a href="#">All Stars</a></li>
	<li><a href="#">#SproutChat</a></li>
</ul>
</li>
</ul></nav>
			</div>
		</div>
	</div>
                <form>
                    <input class="overlay_search-input" placeholder="Type and hit Enter..." type="text">
                    <a href="#" class="overlay_search-close">
                        <span></span>
                        <span></span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- End Overlay Search -->

<!--filter -->

<!-- JS Global -->

<!--[if lt IE 9]><script src="public/assets/plugins/jquery/jquery-1.11.1.min.js"></script><![endif]-->
<!--[if gte IE 9]><!-->
<script src="public/assets/frontend/js/jquery-2.1.1.min.js"></script>
<script src="public/assets/frontend/js/modernizr.custom.js"></script>
<script src="public/assets/frontend/js/bootstrap.min.js"></script>
<script src="public/assets/frontend/js/bootstrap-select.min.js"></script>
<script src="public/assets/frontend/js/superfish.js"></script>
<script src="public/assets/frontend/js/jquery.prettyPhoto.js"></script>
<script src="public/assets/frontend/js/placeholdem.min.js"></script>
<script src="public/assets/frontend/js/jquery.smoothscroll.min.js"></script>
<script src="public/assets/frontend/js/jquery.easing.min.js"></script>

<!-- JS Page Level -->
<script src="public/assets/frontend/js/owl.carousel.min.js"></script>
<script src="public/assets/frontend/js/waypoints.min.js"></script>
<script src="public/assets/frontend/js/jquery.plugin.min.js"></script>
<script src="public/assets/frontend/js/jquery.countdown.min.js"></script>

<script src="public/assets/frontend/js/theme-ajax-mail.js"></script>
<script src="public/assets/frontend/js/theme.js"></script>
<script src="public/assets/frontend/js/custom.js"></script>

<!--[if (gte IE 9)|!(IE)]><!
<script src="public/assets/frontend/js/jquery.cookie.js"></script>
<script src="public/assets/frontend/js/theme-config.js"></script>-->

<script>
	$("#tickets").click(function(e){
		
		$("#signUp").show("slow");
		e.preventDefault();
		
	});
	$(".close_signup").click(function(e){
		
		$("#signUp").hide("slow");
		e.preventDefault();
		
	});
	$(".overlay_search-close").on('click', function () {
        $("body").removeClass('open');
        return false;
    });
    $(".js-open-search").on('click', function () {
        $("body").toggleClass('open');
        $('.overlay_search-input').focus();
    });
    
</script>
 <script type="text/javascript">
	jQuery(window).load(function () {
        jQuery('#loading').hide();
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        theme.init();
        theme.initMainSlider();
        theme.initCountDown();
        theme.initPartnerSlider();
        theme.initTestimonials();
        theme.initGoogleMap();
    });
    jQuery(window).load(function () {
        theme.initAnimation();
    });

    jQuery(window).load(function () { jQuery('body').scrollspy({offset: 100, target: '.navigation'}); });
    jQuery(window).load(function () { jQuery('body').scrollspy('refresh'); });
    jQuery(window).resize(function () { jQuery('body').scrollspy('refresh'); });

    jQuery(document).ready(function () { theme.onResize(); });
    jQuery(window).load(function(){ theme.onResize(); });
    jQuery(window).resize(function(){ theme.onResize(); });

    jQuery(window).load(function() {
        if (location.hash != '') {
            var hash = '#' + window.location.hash.substr(1);
            if (hash.length) {
                jQuery('html,body').delay(0).animate({
                    scrollTop: jQuery(hash).offset().top - 44 + 'px'
                }, {
                    duration: 1200,
                    easing: "easeInOutExpo"
                });
            }
        }
    });

</script>
<script type="text/javascript" src="public/assets/frontend/blogjs/1238587809-widgets.js"></script>
<script gapi_processed="true" type="text/javascript" src="public/assets/frontend/blogjs/plusone.js"></script>
</body></html>