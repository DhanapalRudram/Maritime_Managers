
<body id="home" class="body-light wide ltr">
<div id="loading">
        <img src="public/assets/frontend/img/load.gif" alt="Loading..." />
   </div>

<!-- Wrap all content -->
<div class="wrapper">

    <!-- HEADER -->
    <header class="header fixed">
        <div class="container">
            <div class="header-wrapper clearfix">

            <!-- Logo -->
            <div class="logo">
                <img src="public/assets/frontend/img/LOGO 1.PNG" class="scroll-to" width="50px">
                    
                </img>
            </div>
            <!-- /Logo -->
			<!--<form>
					  <input type="text" name="search" placeholder="Search..">
					</form>
            <!-- Navigation -->
            <div id="mobile-menu"></div>
            <nav class="navigation closed clearfix">
                <a href="#" class="menu-toggle btn"><i class="fa fa-bars"></i></a>
                
                <ul class="sf-menu nav sf-js-enabled sf-arrows">
                	
                     <?php  $path=url()->current();
                            $da=end((explode('/', rtrim($path, '/'))));
                     ?>
                    <li class="<?php if($da=="Frontend"){ echo  "active"; } ?>" ><a href="/Frontend">Home</a></li>
                    <li class="<?php if($da=="About"){ echo  "active"; } ?>"><a href="About">About</a></li>
                    <li class="<?php if($da=="Speakershub"){ echo  "active"; } ?>"><a href="Speakershub">Speakers Hub</a></li>
                    <li class="<?php if($da=="Events"){ echo  "active"; } ?>" ><a href="/Events">Events</a></li>
                    <li class="<?php if($da=="Blog"){ echo  "active"; } ?>"><a href="/Blog">Blog</a></li>
                    <li class="<?php if($da=="Jobportal"){ echo  "active"; } ?>"><a href="/Jobportal">Jobs</a></li>
                    <li class="<?php if($da=="CaseStudy"){ echo  "active"; } ?>"><a  href="/CaseStudy">Digital Library</a></li>
                    <li class="search search_main">
                        <a href="#" class="js-open-search">
                           <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        
                    </li>
                   
                </ul>
            </nav>
            <!-- /Navigation -->

            </div>
        </div>
    </header>
    <!-- /HEADER -->