<title>Marine-ContactUs</title>
@include('frontend.blog.includes.links')
<!-- TEMPLATE STYLES -->
      <link rel="stylesheet" href="public/assets/frontend/event/css/forms.css" type="text/css">
      <link rel="stylesheet" href="public/assets/frontend/event/css/template.css" type="text/css">
      
<style>
#contentwrapper {
    background: #f6f6f6 none repeat scroll 0 0;
    margin: 0 auto 50px;
    height:660px;
    max-width: 100%;
}
#wrapper {
    background: #f6f6f6 none repeat scroll 0 0;
    margin: 0 auto;
   
    overflow: hidden;
    max-width: 100%;
}

#sidebar h2 {
    background: none repeat scroll 0 0;
    color: #fff;
    font: 700 11px "Raleway",sans-serif;
    letter-spacing: 2px;
    margin: 0 0 25px;
   padding: 13px 20px;
    border-bottom:1px solid #fff;
    
    text-transform: uppercase;
    text-align: center;
}
#sidebar-widget .widget-content {
    margin: 0 0 10px;
}
#footer-wrapper {
    text-align: center;
    width: 100%;
    clear: both;
    
}
.main.section {
    
    max-width: 100% !important;
    position: absolute;
    left:0;
}
#sidebar {
    color: #fff;
    margin-top: 15px;
right: 20px;
    font: 13px/1.6 "Droid Serif",serif;
    max-width: 370px;
    text-align: center;
    width: 100%;
    z-index: 1000000;
    position: absolute;
    border-radius: 16px;
    padding-top: 10px;
    background: #2592D0;
    padding-bottom: 15px;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    background-color: #fff;
    color: #435469;
}
.btn:hover, .btn:focus {
    color: #fff;
    text-decoration: none;
    box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
    background-color: #435469;
  
}
.btn-theme {
    background-color: #2592d0;
    
    color: #fff;
    font-size: 14px;
    font-weight: 700;
    line-height: 1;
    padding: 15px 35px;
    text-transform: uppercase;
    transition: all 0.2s ease-in-out 0s;
    width: 227px;
}
.Label1 li {
    background: none;
    margin: 0 0 1px;
    padding: 5px 0;
}
.Label1 a {
    color: #fff;
    font: 700 34px "Raleway",sans-serif;
    letter-spacing: 1px;
    padding: 5px 20px 5px 20px;
    text-transform: uppercase;
}

.row {
  margin:5px;
}
.main.section {
    float: left;
    max-width: 695px;
    width: 100%;
}
.iconic-list li
{
    float:left;
    width:auto;
    
}
#sidebar {
    color: #515151;
    float: right;
    font: 13px/1.6 "Droid Serif",serif;
    max-width: 370px;
    text-align: center;
    width: 100%;
}
iframe {
    border: 5px solid #ccc;
    box-shadow: 1px 4px 5px #ccc;
    height: 887px;
    margin-left: 9px;
    width: 96%;
}
.Label2 a {
    color: #fff;
    font: 700 14px "Raleway",sans-serif;
    letter-spacing: 1px;
    padding: 5px 20px 5px 20px;
    text-transform: uppercase;
}
</style>
@include('frontend.blog.includes.header')

<!--<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
	<img draggable="false" src="public/assets/frontend/blogimg/12.jpg" width="100%" height="100%">
				
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>-->
<h1 align="center" style="margin-top: -50px;margin-bottom: 50px;">Contact Us</h1>
<div id="contentwrapper">
<div id="wrapper">
    
<div class="main section" id="main">

       <div id="product_container">
    
<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed casestudy">
   
   <div class="date-outer">
      <div class="date-posts">


<div class="post-outer1">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
	<img src="public/assets/frontend/img/map.jpg" style="height: 700px;width:100%"></img>
	 </div>
   
</div>
	
</div>



</div>

 </div>
</div>
      
</div>


</div></div>
<aside id="sidebar">
<div class="sidebar section" id="sidebar-widget"><div class="widget HTML" data-version="1" id="HTML1">

<div class="widget Label2" data-version="1" id="Label1">
<h2 style=" border-top:none;">contact details</h2>
<div class="widget-content list-label-widget-content">
<ul>
<li>
<a dir="ltr" href="#">demo@demo.com</a>
</li>
<li>
<a dir="ltr" href="#">1234567890</a>
</li>
</ul>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget Label" data-version="1" id="Label1">
<h2>Send Message</h2>
<div class="widget-content list-label-widget-content">
<p class="message1"></p>
	<form id="contact_form" method="post" enctype="multipart/form-data" class="contact_form" >
			{{csrf_field()}}
  <div class="row">
    
    <input id="name" class="input" name="name" type="text" value="" placeholder="Your Name" required /><br />
    <span id="name_validation" class="error_message"></span>
  </div>
  <div class="row">
   
    <input id="email" class="subscribe-email" name="email" type="text" value="" placeholder="Your Email Address" required/><br />
    <span id="email_validation" class="error_message"></span>
  </div>
  <div class="row">
    
    <textarea id="message" class="input" name="message" rows="7" cols="30" style="width: 327px; height: 185px;" placeholder="Your Message"></textarea><br />
    <span id="message_validation" class="error_message"></span>
  </div>
    
    <input style="float: left;width: 100px !important;text-align: center;padding: 0px;margin-left: 22px;" class="btn btn-theme subscribe-form-submit wysija-submit wysija-submit-field" value="Send!" type="submit">
</form>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
</div>
<div class="widget Label1" data-version="1" id="Label1">
<h2>Connect</h2>
<div class="widget-content list-label-widget-content">
<ul class="iconic-list">
	<li class="tooltip-ontop" title="Facebook"><a href="dfsdfsdfscvc"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
	<li class="tooltip-ontop" title="Twitter"><a href="dfsder"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
	<li class="tooltip-ontop" title="Skype"><a href="fsdf"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
	<li class="tooltip-ontop" title="Google Plus"><a href="sdfs"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
	<li class="tooltip-ontop" title="Linkedin"><a href="fsdfsd"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
	
</ul>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
</div>
</div>
</aside>
<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
<script>
	$('form.contact_form').submit(function(event){
		 event.preventDefault();
		var token = "{{ Session::getToken() }}";
		var blog_id=$('#blog_id').val();
		var name=$('#name').val();
		var email=$('#email').val();
		var message=$('#message').val();
		$.ajax({
			        type: 'post',
			        url: "{{URL::to('sendmail')}}",
			        data: "_token="+ token+"&name="+name+"&email="+email+"&message="+message,
			        success: function (data) {
			        	if(data=='success')
			        	{
			        		$(".message1").html('Mail Send Successfully').css("color","green");
            	      		$("#Label1").load(location.href + " #Label1>*", "");
			        	}
            	      	else
            	      	{
            	      		$(".message1").html('Error').css("color","red");
            	      	}
            	    }
			        
		    	});
	
	});
</script>