<style>
.pagen
{
	width:95%;
	float:left;
	margin-top: 0px;
}
.pagination
{
	
	float:right;
	margin-top:15px;
	margin-bottom:10px;
	
}
.pagination li
{
	
	display: inline-block;
	float:left;
	}
	.pagination li.disabled span
{

	padding:10px;
	font-size:18px;
	float:left;
	background:#ccc;
	margin-right:4px;
	}
	.pagination li.active span
{
	
	padding:10px;
	margin-right:4px;
	font-size:18px;
	float:left;
	background:#2592D0;
	color:#fff;
	}
	.pagination li a
{
	
	padding:10px;
	font-size:18px;
	margin-right:2px;
	float:left;
	background:#fff;
	text-decoration: none;
	}
	.pagination li:hover a,.pagination li:hover span
	{
		background:#2592D0 !important;
		color:#fff;
	}
	.col-md-8{width:24%;
	 padding-left: 0;
    padding-right: 5px   
	}
	.serv-block {
    position: relative;
    margin-left: 10px;
    border-radius: 5px;
    box-shadow: 1px 1px 1px rgba(0,0,0,0.1);
    padding: 40px 20px;
    background: #fff;
    text-align: center;
    margin-bottom: 30px;
    transition: all .2s ease-out;
    -webkit-transition: all .2s ease-out;
    -moz-transition: all .2s ease-out;
    -ms-transition: all .2s ease-out;
}
.continue_reading {
    font: 400 11px "Raleway",sans-serif;
    letter-spacing: 1px;
    margin: 40px 0 0px 40px;
    text-align: center;
    text-transform: uppercase;
    margin: 40px 0 0 0px;
}
.continue_reading a {
    background: #eee none repeat scroll 0 0;
    color: #061C42;
    padding: 10px 20px;
    width: 100%;
}
</style>
<div id="product_container" style="margin-bottom:40px;">

        <!-- MARKET SHOW -->
        <section class="page-section light" id="speakers">
            <div class="container" style="background:none;">
               <!-- <h1 class="section-title">
                    <span data-animation="flipInY" data-animation-delay="300" class="icon-inner animated flipInY visible">
                        <span class="fa-stack">
                        <i class="fa fa-microphone fa-2x"></i></span></span>
                    <span data-animation="fadeInUp" data-animation-delay="500" class="title-inner animated fadeInUp visible">Esteemed Speakers</span>
                </h1>-->

                <!-- Speakers row -->
                <div class="row  clear">
                	@foreach($datas as $data)
                	<!-- -->
                	<div class="time-entry">
                    <div class="col-md-8 serv-block">
                        <article class="post-wrap  visible" data-animation="fadeInUp" data-animation-delay="100" style="background:#fff;">
                            <div class="post-media">
                                
                                <img src="public/{{$data->author_image}}" alt="" style="width:100%;height:300px">
                            </div>
                            <div class="post-header">
                                <h2 class="post-title"><a href="getCasestudy/{{$data->casestudy_id}}"><b>{{ str_limit($data->title, $limit = 15, $end = '...') }}</b></a></h2>
                                <div class="post-meta">
                                    <span class="post-date">
                                       {{$data->author_name}}
                                    </span>
                                    
                                </div>
                            </div>
                            <div class="continue_reading" ><a href="getCasestudy/{{$data->casestudy_id}}" class="btn btn-theme animated flipInY visible" data-animation="flipInY" data-animation-delay="300">Read</a></div>
                        </article>
                    </div>
                    </div>
                    @endforeach
                    
                </div>
                <!-- /Speakers row -->

                
            </div>
        </section>
        <!-- /MARKET SHOW -->


<div class="pagen">{{ $datas->links() }} </div>
        