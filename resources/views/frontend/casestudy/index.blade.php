<title>Marine-CaseStudy</title>
<link href="public/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<link href="public/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
<style>
.overlay_te
 {
 	position: absolute;
	top: 35%;
	color: rgb(255, 255, 255);
	font-size: 60px;
	text-align: center;
	width: 100%;
 }
 #menu1,#menu1  ul 
{
	width:100%;float:left;
}
#menu1 ul li
{
	width:33%;float:left;list-style-type:none;padding: 30px 8%;
}
#menu1 ul li a
{
	color:#000;font-size:20px;padding-bottom:10px;width:60%;float: left;
	text-align:center;font-family: Raleway,sans-serif;
}
#menu1 ul li.active a,#menu1 ul li a:hover
{
	border-bottom:2px solid #2592D0 ;
	color:#2592D0;
}
#header-inner {
    background: #f6f6f6 none repeat scroll 0 0;
    padding: 0px 0 30px;
 }
 #footer-wrapper
 {
 background: #2592D0 none repeat 	
 }
 .container {
    
    margin: 0 auto !important;
}
 .page1 .container {
    
   width:100%;
   padding:0;
}
.toolbar {
 background: #2592D0;
 padding: 10px 10px 10px 356px;
 width: 100%;
}
#txtSearchPage
{
	width:50%;
	
}
</style>
@include('frontend.blog.includes.header')
    <!-- Content area -->
    <div class="content-area">

	        <div id="main">
        <!-- SLIDER -->
        <section class="page-section no-padding background-img-slider page1">
            <div class="container" style="background:none;">
         
            <div id="main-slider" class="owl-carousel owl-theme owl-loaded">
            	 <img alt="" src="public/assets/frontend/blogimg/Library Cover.jpg" style="width:100%;height:500px;">
                </div>
            </div>


        </section>
        <!-- /SLIDER -->
        </div>
        <br>
       <!-- Search Part -->
      <div class="toolbar">
            <input id="txtSearchPage" type="search" placeholder="Search Author name/ Book Title" /><br/>
       </div>
       <!-- search end -->
     <section class="page-section">
     	<div id="menu1">
     <ul>
         <?php  $path=url()->current();
              $da=end((explode('/', rtrim($path, '/'))));
         ?>
       
        <li class="active" ><a href="/CaseStudy">All</a></li>
        
        <li class="" ><a href="/caseStudies">Case Study</a></li>
        
        <li class="" ><a href="/Ebooks">E-Books</a></li>
       
     </ul>
    </div>    
     <!--<p align="center">
     	<input type="checkbox" name=""/>All
     	<input type="checkbox" name=""/>Casestudy 
        <input type="checkbox" name="" >E-Books
       </p>-->
	</section>
	@include('frontend.casestudy.result')
	</div>
	

 @include('frontend.blog.includes.footer')
   <script src="public/assets/frontend/js/jquery-2.1.1.min.js"></script>
   
	<script>
		 $(window).on('hashchange', function() {
		        if (window.location.hash) {
		            var page = window.location.hash.replace('#', '');
		            if (page == Number.NaN || page <= 0) {
		                return false;
		            }else{
		                getData(page);
		            }
		        }
		    });
		$(document).ready(function()
		{
		     $(document).on('click', '.pagination a',function(event)
		    {
		        $('li').removeClass('active');
		        $(this).parent('li').addClass('active');
		        event.preventDefault();
		        var myurl = $(this).attr('href');
		       var page=$(this).attr('href').split('page=')[1];
		       getData(page);
		    });
		});
		function getData(page){
		        $.ajax(
		        {
		            url: '?page=' + page,
		            type: "get",
		            datatype: "html",
		            // beforeSend: function()
		            // {
		            //     you can show your loader 
		            // }
		        })
		        .done(function(data)
		        {
		            console.log(data);
		            
		            $("#product_container").empty().html(data);
		            location.hash = page;
		        })
		        .fail(function(jqXHR, ajaxOptions, thrownError)
		        {
		              alert('No response from server');
		        });
		}
		

  </script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>        
  	<script>
  		    $.expr[":"].containsNoCase = function (el, i, m) {
        var search = m[3];
        if (!search) return false;
          return new RegExp(search,"i").test($(el).text());
    };

    $.fn.searchFilter = function (options) {
        var opt = $.extend({
            // target selector
            targetSelector: "",
            // number of characters before search is applied
            charCount: 1
        }, options);

        return this.each(function () {
            var $el = $(this);
            $el.on("keyup",function () {
                var search = $(this).val();

                var $target = $(opt.targetSelector);
                $target.show();

                if (search && search.length >= opt.charCount)
                    $target.not(":containsNoCase(" + search + ")").hide();
            });
        });
    };
$("#txtSearchPage").keyup(function() {
        var search = $(this).val();
        $(".time-entry").show();
        if (search)
            $(".time-entry").not(":containsNoCase(" + search + ")").hide();
});

$.expr[":"].containsNoCase = function (el, i, m) {
    var search = m[3];
    if (!search) return false;
      return new RegExp(search,"i").test($(el).text());
};


// jQuery Plug-in example
$("#txtSearchPagePlugin")
    .searchFilter({targetSelector: ".time-entry"})


  	</script>