<title>Marine-CaseStudy-Details</title>
<link href="../public/assets/frontend/cbm/slider.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/web.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
	<style>
	body
	{
		background:#F6F6F6;
	}
		.overlay_te
		 {
		 	position: absolute;
			top: 35%;
			color: rgb(255, 255, 255);
			font-size: 60px;
			text-align: center;
			width: 100%;
		 }
		iframe img
		{
			width:100% !important;
			
		}
		.serv-block {
    background: #fff none repeat scroll 0 0;
    border-radius: 5px;
    box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
    margin-bottom: 30px;
    margin-left: 10px;
    padding: 40px 20px;
    position: relative;
    text-align: center;
    transition: all 0.2s ease-out 0s;
}
	</style>
<body>
	<!--<div id="loading">
        <img src="../public/assets/frontend/img/load.gif" alt="Loading..." />
 </div>-->
<div class="top-bar section" id="top-bar"><div class="widget HTML" data-version="1" id="HTML4">
<h2 class="title" style="display: none;">Top Bar</h2>
<div class="widget-content">
<div id="top-bar">
<div id="top-bar-menu">
    <img alt="" id="Header1_headerimg" src="../public/assets/frontend/img/LOGO 1.PNG" style="inline-block" height="100px; " width="30px; ">
   <div id="menu">
     <ul>
         <?php  $path=url()->current();
              $da=end((explode('/', rtrim($path, '/'))));
         ?>
      <li class="<?php if($da=="Frontend"){ echo  "active"; } ?>" ><a href="/Frontend">Home</a></li>
        <li class="<?php if($da=="About"){ echo  "active"; } ?>"><a href="/About">About</a></li>
        <li class="<?php if($da=="Speakershub"){ echo  "active"; } ?>"><a href="/Speakershub">Speakers Hub</a></li>
        <li class="<?php if($da=="Events"){ echo  "active"; } ?>" ><a href="/Events">Events</a></li>
        <li class="<?php if($da=="Blog"){ echo  "active"; } ?>"><a href="/Blog">Blog</a></li>
        <li class="<?php if($da=="Jobportal"){ echo  "active"; } ?>"><a href="/Jobportal">Jobs</a></li>
        <li class="<?php if($da=="CaseStudy"){ echo  "active"; } ?>"><a  href="/CaseStudy">Digital Library</a></li>
        <li class="search search_main">
            <a href="#" class="js-open-search">
               <i class="fa fa-search" aria-hidden="true"></i>
            </a>
            
        </li>
     </ul>
    </div>
  
</div>
</div>


<div class="menu">
<i class="fa fa-list-ul"></i>
</div>

<div id="css-menu">
<div id="cssmenu">
<ul>
<li class="home"><span class="close-menu" style="" title="Close"><i class="fa fa-times"></i></span></li>
<li><a href="http://simplegant.blogspot.in/" title="Home">Home</a></li>
<li class="has-sub"><a href="#">Features</a>
<ul>
<li class="even"><a href="#">Standard Post</a></li>
<li class="odd"><a href="#">Video Post</a></li>
<li class="even"><a href="#">Static Page</a></li>
<li class="odd"><a href="#">Comments Content</a></li>
<li class="even"><a href="#">Search Results</a></li>
<li class="odd"><a href="#">Typography</a></li>
<li class="even"><a href="#">404 Error Page</a></li>
</ul>
</li>
<li><a href="#">Lifestyle</a></li>
<li><a href="#">Music</a></li>
<li><a href="#">About Me</a></li>
<li><a href="#" style="color: #ddbe5c;">Enter Your Search</a></li>
<li style="border-top: 1px solid #161616; border-bottom: 1px solid #161616;">
    <form method="get" style="display: inline; padding: 15px;" action="/search">
        <input placeholder="Search and hit enter..." name="q" id="s" type="search">
    </form>
</li>
</ul>
</div>
</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="public/assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<header id="header-wrapper">
<div class="header section" id="header"><div class="widget Header" data-version="1" id="Header1">
<div id="header-inner">
<!--<h1>
<a href="#" style="display: inline-block">
<img alt="" id="Header1_headerimg" src="public/assets/frontend/img/LOGO 1.PNG" style="inline-block" height="157px; " width="379px; ">
</a></h1>-->
</div>

</div></div>
<div class="clear"></div>
</header>
<div class="clear"></div>
<!--<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
	   
<div id="slider1"><div class="flexslider">
	<img draggable="false" src="../public/assets/frontend/blogimg/Library Cover.jpg" style="width:100%;height:500px;">
					
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>-->

<div id="contentwrapper">
<aside id="sidebar1">
	<div class="sidebar section" id="sidebar-widget">
		<div class="widget HTML" data-version="1" id="HTML1">
	
	<div class="widget-content">
	<div class="col-md-8 serv-block">
		@foreach($datas as $data)
        <article class="post-wrap  visible" data-animation="fadeInUp" data-animation-delay="100" style="background:#fff;">
            <div class="post-media">
                
                <img src="../public/{{$data->author_image}}" alt="" style="width:100%;height:300px">
            </div></br>
            <div class="post-header">
                <h3>{{$data->title}}</h3></br>
                <b>{{$data->author_name}}</b></br></br>
                <b>{{$data->volume}}</b>
                </br></br>
                <p align="left">{{$data->description}}</p>   
            </div>
            
        </article>
        @endforeach
    </div>
	</div>
	
	<div class="clear"></div>
	<span class="widget-item-control">
	<span class="item-control blog-admin">
	<a class="quickedit" href="#">
	<img alt="" src="#" height="18" width="18">
	</a>
	</span>
	</span>
	<div class="clear"></div>
	</div>

	</div>
</aside>
<!-- Right Side Nav -->
<aside id="sidebar3">
	<div class="sidebar section" id="sidebar-widget">
		<div class="widget HTML" data-version="1" id="HTML1">

	<div class="widget-content">
	<div class="about-me">
		@foreach($datas as $data)
		<iframe src="../public/{{$data->casestudy_image}}" alt="" width="100%" ></iframe>
		@endforeach
	</div>
	
	</div>
	<div class="clear"></div>

	<span class="widget-item-control">
	<span class="item-control blog-admin">
	<a class="quickedit" href="#">
	<img alt="" src="#" height="18" width="18">
	</a>
	</span>
	</span>
	<div class="clear"></div>
	</div>
		
	</div>
	
</aside>
<!-- Right side Nav End -->
</div>

	<!-- Sliding div ends here -->
@include('frontend.blog.includes.footer')
<script src="../public/assets/frontend/cbm/slider.js" rel="javascript" ></script>

<script src="../public/assets/frontend/cbm/cbm-small-mini.js" rel="javascript" ></script>
<script src="../public/assets/frontend/cbm/plusone.js" rel="javascript" ></script>

<!-- Sliding div starts here -->
@include('frontend.casestudy.cbm')