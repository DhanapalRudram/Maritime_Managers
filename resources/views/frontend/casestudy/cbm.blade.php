	<div id="slider11" style="right:-382px;">
		<div id="sidebar13" onclick="open_panel()">
			<img src="../public/assets/frontend/cbm/calculator.png" id="img"/>
			
		</div>
		<div id="header">
				
					<div class="secondside1">
    <div class="minicbmcalculator">
    	
		<form method="get" name="cbmform">
			 
        <div class="DimBlue">
            <div class="dimcontents">
                	<div class="radiobuttons">
		                <input type="radio" onclick="setupControlsCM();" onchange="setupControlsCM();" checked="checked" value="Cubic Meters" id="cubicmeter" name="chkunit">
						<label for="cubicmeter">Cubic Meter</label>
	                    <input type="radio" onclick="setupControlsCF();" onchange="setupControlsCF();" value="Cubic Feet" id="cubicfeet" name="chkunit">
						<label for="cubicfeet">Cubic Feet</label>       
                    </div>
                <div class="dimfirstside">

                    <div>
	                    <label id="lblLength">Length (cm)</label>
                    </div>
                    <div>
	                    <label id="lblWidth">Width (cm)</label>
                    </div>
                    <div>
	                    <label id="lblHeight">Height (cm)</label>
                    </div>
                    <div>
	                    <label id="lblGW">Gross Weight (kg)</label>
                    </div>
                    <div>
	                    <label id="lblqty">Quantity</label>
                    </div>
                </div>
                <div class="dimsecondside">
                    <div>
                        <input type="text" onkeyup="calculateCBM();" onchange="calculateCBM();" id="txtl" class="diminput">
                    </div>
                    <div>
              	        <input type="text" onkeyup="calculateCBM();" onchange="calculateCBM();" class="diminput" id="txtw"> 
                    </div>
                    <div>
                        <input type="text" onkeyup="calculateCBM();" onchange="calculateCBM();" id="txth" class="diminput">
                    </div>
                    <div>
                        <input type="text" onkeyup="calculateCBM();" onchange="calculateCBM();" id="txtcartongw" class="diminput">
                    </div>
                    <div>
                 	    <input type="text" onkeyup="calculateCBM();" onchange="calculateCBM();" value="1" id="txtcartonqty" class="diminput">
                    </div>
                </div>
            </div>
        </div>
        <div class="WeightVolWhite">
            <div class="wtcontents">
            	<div class="wtfirstside">
                    <div>
	                    <label id="">Weight</label>
                    </div>
                    <div>
	                    <label id="">Volume Weight</label>
                    </div>
                    <div>
                        <label></label>
                    </div>

                    <div>
	                    <label id="">Volume</label>
                    </div>
                </div>
                <div class="wtsecondside">
                	
                    <div class="wtchildfirst">
                        <div>
	                        <label>kg</label>
                        </div>
						<div>
							<input type="text" readonly="true" id="txtkgweight" class="wtinput">
						</div>
						<div>
							<input type="text" readonly="true" id="txtkgvolweight" class="wtinput">
						</div>
                        <div>
    	                    <label>m3</label>
        	            </div>
                        <div>
                            <input type="text" readonly="true" id="txtmetervolume" class="wtinput">
                        </div> 			
                    </div>
                    <div class="wtchildsecond">
							
						<div>
	                        <label>lb</label>
                        </div>
                            <div>
                            <input type="text" readonly="true" id="txtlbweight" class="wtinput">
                        </div>
                        <div>
                            <input type="text" readonly="true" id="txtlbvolweight" class="wtinput">
	                    </div>
						<div>
							<label>ft3</label>
						</div>
						<div>
							<input type="text" readonly="true" id="txtfeetvolume" class="wtinput">
						</div>				
                    </div>
                </div>
                
                 
            </div>
        </div>
            
        <div class="ContainerBlue">
            <div class="concontents">
                <div class="confirstside">
                <div>
	                    <label id="">20 FT Container</label>
                    </div>
                    <div>
	                    <label id="">40 FT Container</label>
                    </div>
                    <div>
	                    <label id="">40 FT HC Container</label>
                    </div>
                </div>
                <div class="consecondside">
                    <div class="conchildfirst">
                        <div>
	                    <label>Minimum</label>
                        </div>
                        <div>
                        <input type="text" readonly="true" id="txt20min" class="coninput">
                        </div>
                       	<div>
                        <input type="text" readonly="true" id="txt40min" class="coninput">
                        </div>
                        <div>
                 	    <input type="text" readonly="true" id="txt40hcmin" class="coninput">
                        </div>
                    </div>
                    <div class="conchildsecond">
                        <div>
	                    <label>Maximum</label>
                        </div>
                        <div>
                        <input type="text" readonly="true" id="txt20max" class="coninput">
                        </div>
                       	<div>
                        <input type="text" readonly="true" id="txt40max" class="coninput">
                        </div>
                        <div>
                 	    <input type="text" readonly="true" id="txt40hcmax" class="coninput">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="UPSWhite">
            <div class="osowcontents">
                <div class="colone">
                    <div>
	                    <label id=""></label>
                    </div>
                    <div>
	                    <label id="">Over Weight</label>
                    </div>
                    <div>
	                    <label id="">Over Size</label>
                    </div>
                </div>
                <div class="coltwo">
                <div>
	                <label id="">UPS</label>
                    </div>
                    <div>
	                    <img width="10" height="10" src="../public/assets/frontend/cbm/gray-button.png" id="upsow">
                    </div>
                    <div>
						<img width="10" height="10" src="../public/assets/frontend/cbm/gray-button.png" id="upsos">
                    </div>
                </div>
                <div class="colthree">
                    <div>
	                <label id="">DHL</label>
                    </div>
                    <div>
						<img width="10" height="10" src="../public/assets/frontend/cbm/gray-button.png" id="dhlow">
                    </div>
                    <div>
	                    <img width="10" height="10" src="../public/assets/frontend/cbm/gray-button.png" id="dhlos">
                    </div>
                </div>
                <div class="colfour">
                    <div>
	                <label id="">Fedex</label>
                    </div>
                    <div>
	                    <img width="10" height="10" src="../public/assets/frontend/cbm/gray-button.png" id="fedexow">
                    </div>
                    <div>
	                    <img width="10" height="10" src="../public/assets/frontend/cbm/gray-button.png" id="fedexos">
                    </div>
                </div>
            </div>
        </div>
        
    </form>
    
    </div>
    </div>
				
		</div>
	</div>
	
	