<title>Marine-Quiz</title>
<link href="../public/assets/frontend/cbm/slider.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/web.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
<link href="../public/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
	<img draggable="false" src="../public/assets/frontend/blogimg/quiz inner.jpeg" style="width:100%;height:550px;">
				
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper">
<div id="wrapper">
<div class="main section" id="main">


<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div id="product_container">
@foreach($datas as $data)   
     
<div class="post-outer1">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
	
	<h3>{{$data->title}}</h3>
<div class="continue_reading" style="margin: -80px 0 23px 648px;" >
	<a href="" style="background-color:green;color:#fff;" data-toggle="modal" data-target="#myModal{{$data->quiz_id}}" attr-id="{{$data->quiz_id}}" attr-name="{{$data->title}}" class="test">Start</a></div>

	    <p style="padding-top: 20px;font-size: 14px;line-height: 40px;"> {{$data->description}}</p>
		
	 </div>
	</div>
</div>


</div>


 </div>
</div>
      
</div>


</div></div>
<aside id="sidebar">
<div class="sidebar section" id="sidebar-widget"><div class="widget HTML" data-version="1" id="HTML1">
<h2 class="continue_reading">About Quiz</h2>
<div class="widget-content">
<div class="about-me"><img alt="About Quiz" src="../{{$data->quiz_image}}">


</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>

<div class="widget Label" data-version="1" id="Label1">
<h2>information</h2>
<div class="widget-content list-label-widget-content">
<ul>
<li>
<a dir="ltr" href="#">Total Questions</a>
<span dir="ltr">{{$data->no_of_questions}}</span>
</li>
<li>
<a dir="ltr" href="#">Total Duration</a>
<span dir="ltr">{{$data->duration}}</span>
</li>
<li>
<a dir="ltr" href="#">Duration </a>
<span dir="ltr">{{$data->time_limit_questions}}</span>
</li>
<li>
<a dir="ltr" href="#">Category</a>
<span dir="ltr">{{$data->category}}</span>
</li>
</ul>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
</div>
<div class="widget HTML" data-version="1" id="HTML7">
<h2 class="title">Space For Ad</h2>
<div class="widget-content">
<!--<iframe scrolling="no" allowtransparency="true" style="border:none; overflow:hidden; width:300px; height:185px;" src="></iframe>-->
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../public/assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
</aside>
<div class="clear"></div>
</div>
</div>
@endforeach

  <!-- Large modal -->
 @foreach($datas as $data)
<div class="modal fade" id="myModal{{$data->quiz_id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                
            </div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-8" style="margin-left: 229px;">
                        <!-- Nav tabs 
                        <ul class="nav nav-tabs">
                            
                            <li class="active"><a href="#Registration" data-toggle="tab">Registration</a></li>
                        </ul>-->
                        <!-- Tab panes -->
                        <div class="tab-content">
       						 
                            <div class="tab-pane active" id="Registration1">
                            	
                                <form role="form"  method="post" enctype="multipart/form-data" class="form-horizontal" id="Registration{{$data->quiz_id}}" name="Registration">
                                	{{csrf_field()}}
                                 
                                <div class="form-group">
                                    
                                    <div class="col-sm-12">
                                        <input type="email" class="form-control" id="email" placeholder="Your E-mail" name="email">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    
                                    <div class="col-sm-12">
                                        <input type="name" class="form-control" id="name" placeholder="Your Full Name" name="name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <button type="submit" id="tickets1" class="btn btn-theme animated flipInY visible" data-label="Get ticket"><span>Play Now </span> <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
                                <!--<input type="hidden" name="quiz_id" id="quiz_id" value="{{$data->quiz_id}}">
                                <input type="hidden" name="quiz_name" id="quiz_name" value="{{$data->title}}">-->
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach 
	<!-- Sliding div ends here -->
@include('frontend.blog.includes.footer')
<script src="../public/assets/frontend/cbm/slider.js" rel="javascript" ></script>
<script src="../public/assets/frontend/js/bootstrap.min.js"></script>
<script src="../public/assets/frontend/cbm/cbm-small-mini.js" rel="javascript" ></script>
<script src="../public/assets/frontend/cbm/plusone.js" rel="javascript" ></script>
  <script>
 $('a.test').click(function(){
     var quiz_id=$(this).attr("attr-id");
     var quiz_name=$(this).attr("attr-name");
      //alert(quiz_id);return false;;
    $('form#Registration'+quiz_id).submit(function(event){
    event.preventDefault();
   	var token = "{{ Session::getToken() }}";
    
    var name =$("#name").val();
     var email =$("#email").val();
     
     		$.ajax({
					type: 'post',
					url:"{{URL::to('insertQuizUsers')}}",
					data:"_token="+token+"&quiz_id="+quiz_id+"&name="+name+"&email="+email+"&quiz_name="+quiz_name,
					
					success: function(data) {
					  window.location.href="/playQuiz/"+quiz_id;
					}
				});
	
    });
 });
  </script>