<!DOCTYPE html>
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:b="http://www.google.com/2005/gml/b" xmlns:data="http://www.google.com/2005/gml/data" xmlns:expr="http://www.google.com/2005/gml/expr"><head>

<link href="../publicassets/frontend/blogcss/css.css" rel="stylesheet" type="text/css">
<link href="../../public/assets/frontend/blogcss/font-awesome.css" rel="stylesheet">
<link href="../public/assets/frontend/blogimg/s1600/s.png" rel="shortcut icon" sizes="62x62" type="image/vnd.microsoft.icon">
<script src="../public/assets/frontend/blogjs/ca-pub-8866810659977430.js"></script>
<script async="" src="../public/assets/frontend/blogjs/cbgapi.loaded_1"></script>
<script async="" src="../public/assets/frontend/blogjs/cbgapi.loaded_0"></script>
<script src="../public/assets/frontend/blogjs/jquery-1.js" type="text/javascript"></script>
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<title>Marine-Quiz</title>
<!-- </head> -->
</head>
<style>
.pagen
{
	width:100%;
	float:left;
}
.pagination
{
	
	float:right;
	margin-top:10px;
	margin-bottom:10px;
	
}
.pagination li
{
	
	display: inline-block;
	float:left;
	}
	.pagination li.disabled span
{
	//border-right:1px solid #ccc;
	padding:10px;
	font-size:18px;
	float:left;
	background:#ccc;
	margin-right:4px;
	}
	.pagination li.active span
{
	//border-right:1px solid #2592D0;
	padding:10px;
	margin-right:4px;
	font-size:18px;
	float:left;
	background:#2592D0;
	color:#fff;
	}
	.pagination li a
{
	//border-right:1px solid #ccc;
	padding:10px;
	font-size:18px;
	margin-right:2px;
	float:left;
	background:#fff;
	text-decoration: none;
	}
	.pagination li:hover a,.pagination li:hover span
	{
		background:#2592D0 !important;
		color:#fff;
	}
</style>
<body>
	<!--<div id="loading">
        <img src="../../public/assets/frontend/img/load.gif" alt="Loading..." />
 </div>-->
<div class="top-bar section" id="top-bar"><div class="widget HTML" data-version="1" id="HTML4">
<h2 class="title" style="display: none;">Top Bar</h2>
<div class="widget-content">
<div id="top-bar">
<div id="top-bar-menu">
    <img alt="" id="Header1_headerimg" src="../public/assets/frontend/img/LOGO 1.PNG" style="inline-block" height="100px; " width="30px; ">
   <div id="menu">
     <ul>
      <li ><a href="/Frontend">Home</a></li>
        <li class=""><a href="/About">About</a></li>
        <li class=""><a href="#">Speakers hub</a></li>
        <li class=""><a href="/Events">Events</a></li>
        <li class="active"><a href="/Blog">Blog</a></li>
        <li class=""><a href="/Jobportal">Jobs</a></li>
        <li><a href="/CaseStudy">Digital Library</a></li>
        <li class="search search_main">
            <a href="#" class="js-open-search">
               <i class="fa fa-search" aria-hidden="true"></i>
            </a>
            
        </li>
     </ul>
    </div>
  
</div>
</div>


<div class="menu">
<i class="fa fa-list-ul"></i>
</div>

<div id="css-menu">
<div id="cssmenu">
<ul>
<li class="home"><span class="close-menu" style="" title="Close"><i class="fa fa-times"></i></span></li>
<li><a href="http://simplegant.blogspot.in/" title="Home">Home</a></li>
<li class="has-sub"><a href="#">Features</a>
<ul>
<li class="even"><a href="#">Standard Post</a></li>
<li class="odd"><a href="#">Video Post</a></li>
<li class="even"><a href="#">Static Page</a></li>
<li class="odd"><a href="#">Comments Content</a></li>
<li class="even"><a href="#">Search Results</a></li>
<li class="odd"><a href="#">Typography</a></li>
<li class="even"><a href="#">404 Error Page</a></li>
</ul>
</li>
<li><a href="#">Lifestyle</a></li>
<li><a href="#">Music</a></li>
<li><a href="#">About Me</a></li>
<li><a href="#" style="color: #ddbe5c;">Buy This Theme</a></li>
<li style="border-top: 1px solid #161616; border-bottom: 1px solid #161616;">
    <form method="get" style="display: inline; padding: 15px;" action="/search">
        <input placeholder="Search and hit enter..." name="q" id="s" type="search">
    </form>
</li>
</ul>
</div>
</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../public/assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<header id="header-wrapper">
<div class="header section" id="header"><div class="widget Header" data-version="1" id="Header1">

</div></div>
<div class="clear"></div>
</header>
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
		<img draggable="false" src="../public/assets/frontend/blogimg/quiz inner.jpeg" style="width:100%;height:550px;">		
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>

<div id="contentwrapper">
<div id="wrapper">
<div class="main section1" id="main">

@foreach($timer as $time)    
<div style="text-align: right;font-size: 20px;border: 1px solid #000;width: 30%;float: right;
padding: 10px;">Quiz will be closes in <span id="time">{{$time->duration}}</span>!</div>
<input type="hidden" name="time1" id="time1" value="{{$time->duration}}"/>
<input type="hidden" name="user_id" id="user_id" value="{{$id}}"/>
@endforeach
      @include('frontend.quiz.question_result')
</div>

 </div>
</div>
      
</div>


</div></div>
<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
<script>
 $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getData(page);
            }
        }
    });
$(document).ready(function()
{
     $(document).on('click', '.pagination a',function(event)
    {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl = $(this).attr('href');
       var page=$(this).attr('href').split('page=')[1];
       getData(page);
    });
});
function getData(page){
        $.ajax(
        {
            url: '?page=' + page,
            type: "get",
            datatype: "html",
            // beforeSend: function()
            // {
            //     you can show your loader 
            // }
        })
        .done(function(data)
        {
            console.log(data);
            
            $("#product_container").empty().html(data);
            location.hash = page;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
}

$('.pagen li').click(function(){
    
   	var token = "{{ Session::getToken() }}";
    var quiz_id =$("#quiz_id").val();
    var user_id =$("#user_id").val();
    var question_id =$("#question_id").val();
     var answer =$("#options").val();
     var user_answer=$('input[type="radio"][class="className"]:checked').val();
  
     		$.ajax({
					type: 'post',
					url:"{{URL::to('insertAnswers')}}",
					data:"_token="+token+"&quiz_id="+quiz_id+"&question_id="+question_id+"&user_answer="+user_answer+"&user_id="+user_id,
					beforeSend: function() {
						$(".pagen").css("background-color","#fb6c6c");
					},
					success: function(data1) {
					
					}
				});
	
    });
  </script>
 <!-- <script>
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    //alert(timer);
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;
 
        if (--timer < 0) {
            timer = duration;
           
        }
        else if(timer == 0)
        {
            alert("Times Up");
            window.location.href="/Quiz";
        }
    }, 1000);
    
}

window.onload = function () {
    var time=$("#time1").val().split(':');
    var fiveMinutes = time[1] * 60,
        display = document.querySelector('#time');
        startTimer(fiveMinutes, display);
      //alert(fiveMinutes);
};
</script>-->