<link href="public/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
<style>
.pagen
{
	width:95%;
	float:left;
	margin-top: 0px;
}
.pagination
{
	
	float:right;
	margin-top:15px;
	margin-bottom:10px;
	
}
.pagination li
{
	
	display: inline-block;
	float:left;
	}
	.pagination li.disabled span
{

	padding:10px;
	font-size:18px;
	float:left;
	background:#ccc;
	margin-right:4px;
	}
	.pagination li.active span
{
	
	padding:10px;
	margin-right:4px;
	font-size:18px;
	float:left;
	background:#2592D0;
	color:#fff;
	}
	.pagination li a
{
	
	padding:10px;
	font-size:18px;
	margin-right:2px;
	float:left;
	background:#fff;
	text-decoration: none;
	}
	.pagination li:hover a,.pagination li:hover span
	{
		background:#2592D0 !important;
		color:#fff;
	}
	.col-md-8{width:48%;
	 padding-left: 0;
    padding-right: 5px   
	}
	.serv-block {
    position: relative;
    margin-left: 10px;
    border-radius: 5px;
    box-shadow: 1px 1px 1px rgba(0,0,0,0.1);
    padding: 40px 20px;
    background: #fff;
    text-align: center;
    margin-bottom: 30px;
    transition: all .2s ease-out;
    -webkit-transition: all .2s ease-out;
    -moz-transition: all .2s ease-out;
    -ms-transition: all .2s ease-out;
}
.continue_reading {
    font: 400 11px "Raleway",sans-serif;
    letter-spacing: 1px;
    margin: 40px 0 0px 40px;
    text-align: center;
    text-transform: uppercase;
    float: left;
}
abbr .fa 
{
  margin-right: 8px;
}
abbr {
    text-decoration: none;
    font-size: 12px;
    color:#2592D0;
}
.post-image {
    margin: 0 0 4%;
    object-fit: cover;
    width: 99%;
    height: 300px;
}
</style>
<div id="product_container" style="margin-bottom:40px;">

        <!-- MARKET SHOW -->
        <section class="page-section light" id="speakers">
            <div class="container" style="background:none;">
               <!-- <h1 class="section-title">
                    <span data-animation="flipInY" data-animation-delay="300" class="icon-inner animated flipInY visible">
                        <span class="fa-stack">
                        <i class="fa fa-microphone fa-2x"></i></span></span>
                    <span data-animation="fadeInUp" data-animation-delay="500" class="title-inner animated fadeInUp visible">Esteemed Speakers</span>
                </h1>-->

                <!-- Quiz row -->
                <div class="row  clear">
                	@foreach($datas as $data)
                	<!-- -->
                	 <input type="hidden" name="quiz_id" id="quiz_id" value="{{$data->quiz_id}}">
                    <input type="hidden" name="quiz_name" id="quiz_name" value="{{$data->title}}">
                    <div class="col-md-8 serv-block">
                        <article class="post-wrap  visible" data-animation="fadeInUp" data-animation-delay="100" style="background:#fff;">
                            <div class="post-media">
                                
                               <img alt="#" class="post-image" src="{{$data->quiz_image}}" height="400px" width="400px">
                            </div>
                            <div class="post-header">
                                <h2 class="post-title"><a href="/getQuiz/{{$data->quiz_id}}"><b>{{str_limit($data->title,$limit = 15, $end = '...')}}</b></a></h2>
                                <div class="post-meta">
                                  <div class="top-label-home">
                                    <span class="post-timestamp">
                                     <abbr class="published" itemprop="datePublished">
                        	             <i class="fa fa-calendar"></i>{{$data->created_at}}</abbr>
                                        </span></br>
                                        
                                        </div>
                                      </div>
                                      <p align="left">{{str_limit($data->description,$limit = 180, $end = '...')}}</p>
                            </div>
                            <div class="continue_reading"  ><a href="" style="background-color:green;color:#fff;" data-toggle="modal" data-target="#myModal{{$data->quiz_id}}" attr-id="{{$data->quiz_id}}" attr-name="{{$data->title}}" class="test">Start</a></div>
                            <div class="continue_reading" ><a href="/getQuiz/{{$data->quiz_id}}" style="text-decoration:none">...Read</a></div>
                             
                        </article>
                    </div>
                    
                    @endforeach
                    
                </div>
                <!-- /Speakers row -->

                
            </div>
        </section>
        <!-- /MARKET SHOW -->
    

<div class="pagen">{{ $datas->links() }} </div>

    <!-- Large modal -->
 @foreach($datas as $data)
<div class="modal fade" id="myModal{{$data->quiz_id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                
            </div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-8" style="margin-left: 229px;">
                        <!-- Nav tabs 
                        <ul class="nav nav-tabs">
                            
                            <li class="active"><a href="#Registration" data-toggle="tab">Registration</a></li>
                        </ul>-->
                        <!-- Tab panes -->
                        <div class="tab-content">
       						 
                            <div class="tab-pane active" id="Registration1">
                            	
                                <form role="form"  method="post" enctype="multipart/form-data" class="form-horizontal" id="Registration{{$data->quiz_id}}" name="Registration">
                                	{{csrf_field()}}
                                 
                                <div class="form-group">
                                    
                                    <div class="col-sm-12">
                                        <input type="email" class="form-control" id="email" placeholder="Your E-mail" name="email">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    
                                    <div class="col-sm-12">
                                        <input type="name" class="form-control" id="name" placeholder="Your Full Name" name="name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <button type="submit" id="tickets1" class="btn btn-theme animated flipInY visible" data-label="Get ticket"><span>Play Now </span> <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
                                <!--<input type="hidden" name="quiz_id" id="quiz_id" value="{{$data->quiz_id}}">
                                <input type="hidden" name="quiz_name" id="quiz_name" value="{{$data->title}}">-->
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach 
<script src="public/assets/frontend/js/jquery-2.1.1.min.js"></script>
<script src="public/assets/frontend/js/bootstrap.min.js"></script>
 <script>
 $('a.test').click(function(){
     var quiz_id=$(this).attr("attr-id");
     var quiz_name=$(this).attr("attr-name");
      //alert(quiz_id);return false;;
    $('form#Registration'+quiz_id).submit(function(event){
    event.preventDefault();
   	var token = "{{ Session::getToken() }}";
    
    var name =$("#name").val();
     var email =$("#email").val();
     
     		$.ajax({
					type: 'post',
					url:"{{URL::to('insertQuizUsers')}}",
					data:"_token="+token+"&quiz_id="+quiz_id+"&name="+name+"&email="+email+"&quiz_name="+quiz_name,
					
					success: function(id) {
					    
					  window.location.href="/playQuiz/"+id;
					}
				});
	
    });
 });
  </script>