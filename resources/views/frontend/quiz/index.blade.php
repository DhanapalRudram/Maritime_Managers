<title>Marine-Quiz</title>

<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<link href="public/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
<style>
.overlay_te
 {
 	position: absolute;
	top: 35%;
	color: rgb(255, 255, 255);
	font-size: 60px;
	text-align: center;
	width: 100%;
 }
 #menu1,#menu1  ul 
{
	width:100%;float:left;
}
#menu1 ul li
{
	width:33%;float:left;list-style-type:none;padding: 30px 8%;
}
#menu1 ul li a
{
	color:#000;font-size:20px;padding-bottom:10px;width:60%;float: left;
	text-align:center;font-family: Raleway,sans-serif;
}
#menu1 ul li.active a,#menu1 ul li a:hover
{
	border-bottom:2px solid #2592D0 ;
	color:#2592D0;
}
#header-inner {
    background: #f6f6f6 none repeat scroll 0 0;
    padding: 0px 0 30px;
 }
 #footer-wrapper
 {
 background: #2592D0 none repeat 	
 }
 .container {
    
    margin: 0 auto !important;
}
 .page1 .container {
    
   width:100%;
   padding:0;
}
</style>
@include('frontend.blog.includes.header')
    <!-- Content area -->
    <div class="content-area">

	        <div id="main">
        <!-- SLIDER -->
        <section class="page-section no-padding background-img-slider page1">
            <div class="container" style="background:none;">
         
            <div id="main-slider" class="owl-carousel owl-theme owl-loaded">
            	 <img alt="" src="public/assets/frontend/blogimg/Quiz Cover.jpeg" style="width:100%;height:500px;">
                </div>
            </div>


        </section>
        <!-- /SLIDER -->
        </div>

	
	
	@include('frontend.quiz.result')
	
	</div>
 @include('frontend.blog.includes.footer')
   
   
	<script>
		 $(window).on('hashchange', function() {
		        if (window.location.hash) {
		            var page = window.location.hash.replace('#', '');
		            if (page == Number.NaN || page <= 0) {
		                return false;
		            }else{
		                getData(page);
		            }
		        }
		    });
		$(document).ready(function()
		{
		     $(document).on('click', '.pagination a',function(event)
		    {
		        $('li').removeClass('active');
		        $(this).parent('li').addClass('active');
		        event.preventDefault();
		        var myurl = $(this).attr('href');
		       var page=$(this).attr('href').split('page=')[1];
		       getData(page);
		    });
		});
		function getData(page){
		        $.ajax(
		        {
		            url: '?page=' + page,
		            type: "get",
		            datatype: "html",
		            // beforeSend: function()
		            // {
		            //     you can show your loader 
		            // }
		        })
		        .done(function(data)
		        {
		            console.log(data);
		            
		            $("#product_container").empty().html(data);
		            location.hash = page;
		        })
		        .fail(function(jqXHR, ajaxOptions, thrownError)
		        {
		              alert('No response from server');
		        });
		}
		

  </script>
 