<title>Marine-SpeakersHub</title>
@include('frontend.blog.includes.links')
<style>
.pagen
{
	width:100%;
	float:left;
}
.pagination
{
	
	float:right;
	margin-top:10px;
	margin-bottom:10px;
	
}
.pagination li
{
	
	display: inline-block;
	float:left;
	}
	.pagination li.disabled span
{
	
	padding:10px;
	font-size:18px;
	float:left;
	background:#ccc;
	margin-right:4px;
	}
	.pagination li.active span
{
	
	padding:10px;
	margin-right:4px;
	font-size:18px;
	float:left;
	background:#2592D0;
	color:#fff;
	}
	.pagination li a
{
	
	padding:10px;
	font-size:18px;
	margin-right:2px;
	float:left;
	background:#fff;
	text-decoration: none;
	}
	.pagination li:hover a,.pagination li:hover span
	{
		background:#2592D0 !important;
		color:#fff;
	}
	#header-inner {
    background: #f6f6f6 none repeat scroll 0 0;
    padding: 0px 0 30px;
}
</style>
@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<!--<div class="widget-content">
<div id="slider1"><div class="flexslider">
	<img draggable="false" src="public/assets/frontend/blogimg/12.jpg" width="100%" height="100%">
				
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>-->
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper">
<div id="wrapper">
<div class="main section1" id="main">

       @include('frontend.speakershub.result')
</div>

 </div>
</div>
      
</div>


</div></div>
<!--<aside id="sidebar">
<div class="sidebar section" id="sidebar-widget"><div class="widget HTML" data-version="1" id="HTML1">
<h2 class="continue_reading">About Us</h2>
<div class="widget-content">
<div class="about-me">
	<img alt="About Me" src="assets/frontend/blogimg/boat-large.jpg">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.... <a href="#">Read more</a>

</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget Label" data-version="1" id="Label1">
<h2>Categories</h2>
<div class="widget-content list-label-widget-content">
<ul>
<li>
<a dir="ltr" href="/casestudySpeakers">CaseStudy</a>
<span dir="ltr">{{$casestudy}}</span>
</li>
<li>
<a dir="ltr" href="/ebookSpeakers">E-Books</a>
<span dir="ltr">{{$ebook}}</span>
</li>

</ul>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="#" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
</div>
<div class="widget HTML" data-version="1" id="HTML5">
<h2 class="title">Latest Posts</h2>
<div class="widget-content">
<div id="recent-posts">
<ul class="recent_posts_with_thumbs">
	@foreach($latest as $posts)	
	
	<li class="clearfix">
	  <a href="getSpeakers/{{$posts->speakers_id}}">
		<img class="recent_thumb" src="public/{{$posts->speakers_image}}" height="150" width="150"></a>
		<a href="#">{{$posts->title}}</a><br>
	  <div class="at-posts">{{$posts->created_at}}</div>
   </li>
   @endforeach		
</ul>
			<div class="clear"></div>
			</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget HTML" data-version="1" id="HTML7">
<h2 class="title">Space For Ad</h2>
<div class="widget-content">

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
</aside>-->
<div class="clear"></div>
</div>
</div>

<!-- Overlay Search -->

<div class="overlay_search">
    <div class="container">
        <div class="row">
            <div class="form_search-wrap">
            	<div class="Modal-body">
		<div class="row" style="margin-top: 1.5rem;">
			<div class="col-md-8">
			</div>
		</div>
	</div>
                <form action="/speakerSearch" method="post">
                	{{csrf_field()}}
                    <input class="overlay_search-input" name="search" placeholder="Type and hit Enter..." type="text">
                    <a href="#" class="overlay_search-close">
                        <span></span>
                        <span></span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('frontend.blog.includes.footer')
<script>
 $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getData(page);
            }
        }
    });
$(document).ready(function()
{
     $(document).on('click', '.pagination a',function(event)
    {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl = $(this).attr('href');
       var page=$(this).attr('href').split('page=')[1];
       getData(page);
    });
});
function getData(page){
        $.ajax(
        {
            url: '?page=' + page,
            type: "get",
            datatype: "html",
            // beforeSend: function()
            // {
            //     you can show your loader 
            // }
        })
        .done(function(data)
        {
            console.log(data);
            
            $("#product_container").empty().html(data);
            location.hash = page;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
}


  </script>