<title>Marine-Speakers Hub Details</title>
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<style>
	.overlay_te
	 {
	 	position: absolute;
		top: 50%;
		color: rgb(255, 255, 255);
		font-size: 60px;
		text-align: center;
		width: 100%;
	 }
	 .post-outer3 
	 {
	    background: #fff none repeat scroll 0 0;
	    border-radius: 1px;
	    box-shadow: 0 0 5px rgba(0, 0, 0, 0.02);
	    margin: 0 auto;
	    min-height: auto;
	    width: 49%;
	    position: relative;
	}
	.post-outer {
    background: #fff none repeat scroll 0 0;
    border-radius: 1px;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.02);
    float: left;
    margin: 0.5%;
    min-height: auto;
    width: 100%;
    height: auto;
}
.post-summary {
    background: #fff none repeat scroll 0 0;
    border-radius: 1px;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.02);
    padding: 0 0 1.5%;
    float: left;
    width: 100%;
}
#header-inner {
    background: #f6f6f6 none repeat scroll 0 0;
    padding: 10px 0 40px;
}
</style>

@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
	@foreach($datas as $data)   
	<img draggable="false" src="../public/{{$data->speakers_image}}" width="100%" height="100%">
	
	@endforeach			
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper" style="margin-top: -143px;">
<div id="wrapper">
<div class="main section1" id="main">


<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div id="product_container">
@foreach($datas as $data)   
     
<div class="post-outer3" style="background:none !important;">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>
 <p align="center"><img src="../public/{{$data->author_image}}" style="border-radius: 134%;height: 200px;width:38%;"></p>	
	 
<div class="post-summary">
	  <p align="center" style="font-size:20px;text-transform:uppercase;padding: 15px 0 0;">{{$data->author_name}}</p>
	  <p align="center" style="font-size:20px;text-transform:uppercase;padding: 15px 0 0;">{{$data->profession}}</p>	
	  	<p align="center" style="float:left;width:28%;">
            <i class="fa fa-map-marker fa-2x">{{$data->location}}</i>
           
	  	</p>	
	  	<p align="center" style="float:left;width:28%;border-left: 1px solid #000;border-right: 1px solid #000;">
            <i class="fa fa-map-marker fa-2x">{{$data->title}}</i>
           
	  	</p>	
	  	<p align="center" style="float:left;width:28%;">
            <i class="fa fa-map-marker fa-2x">{{$data->start_date}}</i>
           
	  	</p>	
	   
	 </div>
	 
	</div>
</div>
@endforeach

</div>

<div id="product_container">
@foreach($datas as $data)   
     
<div class="post-outer">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
	  
	   <p style="padding-top: 20px;font-size: 14px;line-height: 40px;">
	    {{$data->description}}
	     </p>
		<p>
			@if($data->link!=null)
		      <iframe width="560" height="315" src="{{$data->link}}" frameborder="0" allowfullscreen></iframe>
			@endif
		</p>
	 </div>
	 
	</div>
</div>
@endforeach

</div>
 </div>
</div>
      
</div>


</div></div>



<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
