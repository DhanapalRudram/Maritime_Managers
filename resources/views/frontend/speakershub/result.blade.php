<style>
    .caption-box {
    position: absolute;
    top: 34px;
    left: 32px;
    letter-spacing: 1px;
    text-transform: uppercase;
    text-align: left;
    min-width: 350px;
    display: block;
    padding: 17px 65px 15px 17px;
    color: rgba(255,255,255,0.75);
    text-shadow: 0 0 10px #000000;
}
.caption-box a, .item-block .caption-box .text-holder {
    display: block;
    color: #FFF;
    color: rgba(255,255,255,0.75);
    text-shadow: 0 0 10px #000000;
}
.post-summary1
{
position: absolute;
margin-top: -105px;
margin-left: 55%;
letter-spacing: 1px;
text-transform: uppercase;
text-align: left;
width: 270px;
display: block;
padding: 17px 65px 15px 17px;
color: rgba(255, 255, 255, 0.75);
z-index:8;
}
.post-summary1 a
{
    color:#fff;
    font-size:30px;
    font-weight:400;
}

.post-summary2
{
position: absolute;
margin-top: -120px;
margin-left: 55%;
letter-spacing: 1px;
text-transform: uppercase;
text-align: left;
width: 270px;
display: block;
padding: 10px 65px 5px 17px;
color: rgba(255, 255, 255, 0.75);
z-index:8;
}
.post-summary2 a
{
    color:#fff;
    font-size:30px;
    font-weight:400;
}
.post-summary2 h3 a {
    color: #fff;
}
.post-summary2 p {
    color: #fff;
    font: 14px/22px "Raleway",sans-serif;
    margin: 0 0 10px;
    
}

.post-summary2 h3 {
    font: 700 31px "Raleway",sans-serif;
    letter-spacing: -1px;
    margin: 0 0 0px;
    padding: 1px 0.1%;
    text-align: left;
}
.overlay {
    
height: 300px;
width:100%;
opacity: 0.5;
margin-top: 0;
left: 0;
position: absolute;
padding: 0;
z-index:5;
}
.post-outer1:hover .post-summary1 {
    text-decoration: none;
    background: url(public/assets/frontend/img/arrow.png) no-repeat 100% 50%;
   
border: 2px solid #FFF;
border: 2px solid rgba(255,255,255,0.75);
box-shadow: 0 0 6px rgba(0,0,0,0.7);
-webkit-box-shadow: 0 0 6px rgba(0,0,0,0.7);
-ms-box-shadow: 0 0 6px rgba(0,0,0,0.7);
text-decoration: none;
}
.post-outer1:hover .post-summary2 {
    text-decoration: none;
    background: url(public/assets/frontend/img/arrow.png) no-repeat 100% 50%;
    border: 2px solid #FFF;
    border: 2px solid rgba(255,255,255,0.75);
    box-shadow: 0 0 6px rgba(0,0,0,0.7);
    -webkit-box-shadow: 0 0 6px rgba(0,0,0,0.7);
    -ms-box-shadow: 0 0 6px rgba(0,0,0,0.7);
    text-decoration: none;
}
.continue_reading {
    font: 400 11px "Raleway",sans-serif;
    letter-spacing: 1px;
    margin: 0 0 30px 400px;
    text-align: center;
    text-transform: uppercase;
    position: absolute;
    margin-top: -80px;
    right: 170px;
    z-index: 8;
}
.post-outer1 {
    background: #fff none repeat scroll 0 0;
    border-radius: 1px;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.02);
    float: left;
    width: 100%;
}
</style>
<div id="product_container">
<div class="widget FeaturedPost" data-version="1" id="FeaturedPost1">

<h2 class="title">Featured Speaker </h2>

@foreach($featured as $fea)
<a href="getSpeakers/{{$fea->speakers_id}}">      
<div class="post-outer1" style="background:none !important;">
  
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>
 <a href="getSpeakers/{{$fea->speakers_id}}"> 
 <div class="image" style="position: relative;">
      <div class="overlay"></div>
      <img src="public/{{$fea->speakers_image}}" style="width: 100%;height: 300px;position:relative;" />
  </div></a>
<div class="post-summary2">
	<h3><a href="getSpeakers/{{$fea->speakers_id}}">{{ $fea->author_name }}</a></h3>
	   <p>
	    {{$fea->profession}}
	     </p>
	
	 </div>
	 
</div>
	
</div></a>
@endforeach

<div class="clear"></div>
</div>
<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed casestudy">
   
   <div class="date-outer">
      <div class="date-posts">

@foreach($datas as $data)   
<a href="getSpeakers/{{$data->speakers_id}}">      
<div class="post-outer1" style="background:none !important;">
  
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>
 <a href="getSpeakers/{{$data->speakers_id}}"> 
 <div class="image" style="position: relative;">
      <div class="overlay"></div>
      <img src="public/{{$data->speakers_image}}" style="width: 100%;height: 300px;position:relative;" />
  </div></a>
<div class="post-summary1">
	<h3><a href="getSpeakers/{{$data->speakers_id}}">{{ $data->author_name }}</a></h3>
	   <p>
	    {{$data->profession}}
	     </p>
	
	 </div>
	 <!--	<div class="continue_reading"><a href="getSpeakers/{{$data->speakers_id}}">Continue Reading</a></div>
	 
   <div class="continue_reading1"><a href=""><i class="fa fa-download" aria-hidden="true">Download</i></a>
  </div>    -->
</div>
	
</div></a>
@endforeach

<div class="pagen">{{ $datas->links() }} </div>