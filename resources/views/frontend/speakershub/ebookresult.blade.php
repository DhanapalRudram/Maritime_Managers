<div id="product_container">
<div class="widget FeaturedPost" data-version="1" id="FeaturedPost1">

<h2 class="title">Legend Post</h2>

@foreach($featured as $fea)
<div class="post-summary">
<a href="#">
	<img class="image" src="{{$fea->blog_image}}"></a>
<h3><a href="#">{{$fea->title}}</a></h3>
<p>
    {{ str_limit($fea->description, $limit = 150, $end = '...') }}
</p>
<div class="continue_reading"><a href="#">Continue Reading</a></div>
</div>
@endforeach

<div class="clear"></div>
</div>
<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed casestudy">
   
   <div class="date-outer">
      <div class="date-posts">

@foreach($datas as $data)   
     
<div class="post-outer1">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
	<h3><a href="/getCasestudy/{{$data->casestudy_id}}">{{$data->title}}</a></h3>
	   <p>
	    {{ str_limit($data->description, $limit = 150, $end = '...') }}
	     </p>
		<div class="continue_reading"><a href="#">Continue Reading</a></div>
	 </div>
   <!--<div class="continue_reading1"><a href=""><i class="fa fa-download" aria-hidden="true">Download</i></a>
  </div>    -->
</div>
	
</div>
@endforeach

<div class="pagen">{{ $datas->links() }} </div>