<link href="../public/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
<style>
    /*
	  ==============================================================
		   Schedule Wrap Css Start
	  ==============================================================
*/
section {
    float: left;
    width: 100%;
}
.kf_party_schedule_bg .container {
    background: #fff none repeat scroll 0 0;
    margin: 0px 0 0;
    width: 100%;
}
/*.kf_party_schedule_bg{
	padding:68px 0px 70px;	
}
/*Day 1 Style*/
.kf_party_schedule_day1{
	float:left;
	width:100%;
	position:relative;	
}
.kf_party_schedule_day1 figure{
	float:left;
	width:100%;
	position:relative;
	overflow:hidden;	
}
.kf_party_schedule_day1 figure:before{
	content:"";
	position:absolute;
	top:0px;
	left:0px;
	width:100%;
	height:100%;
	background-color:rgba(0,0,0,0.7);	
}
.kf_party_schedule_day1 figure img{
	width:100%;
	height: 244px;;	
}
.Label a {
    color: #333;
    font: 700 11px "Raleway",sans-serif;
    letter-spacing: 1px;
    padding: 5px 90px 10px 20px;
    text-transform: uppercase;
    float: left;
}
#sidebar li {
    list-style: outside none none;
    min-height: 32px;
    text-align: left;
}
.kf_party_schedule_day_2 figure img {
    height: 204.5px !important;
}
.kf_party_schedule_day1 a{
	display:inline-block;
	position:absolute;
	top:30px;
	left:20px;
	background-color:#2592D0;
	color:#fff;
	padding:10px 15px;
	border-radius:6px;
	font-size:18px;
	line-height:normal;
	font-weight:600;	
}
.no_padding
{
padding:0px;
}
.kf_party_schedule_day1 a i{
	margin-right:10px;	
}
.kf_party_schedule_day1 span{
	display:inline-block;
	position:absolute;
	bottom:30px;
	left:20px;
	color:#fff;
	text-transform:capitalize;
	font-size:36px;	
}
/*Day 1 Descerption Style*/
.kf_schedule_day1_des{
	float:left;
	width:100%;
	position:relative;
	padding:30px 30px 29px 30px;
	height:165px;
}
.kf_schedule_day1_des h6,
.kf_schedule_day1_des p{
	color:#fff;
	font-size:18px;
	line-height:normal;	
}
.kf_schedule_day1_des h6{
	margin:0px 0px 10px;	
}
.kf_schedule_day1_des p{
	margin:0px 0px 12px;	
}
.kf_schedule_day1_des ul{
	background-color:#2592D0;
	float:left;	
	width:auto;
	border-radius:6px;
}
.kf_schedule_day1_des ul a{
	float:left;
	padding:10px 5px;
	color:#fff;	
}
.kf_schedule_day1_des ul a i{
	margin-right:10px;	
}
/*Day 2 Style*/
.kf_party_schedule_day_2{
	float:left;
	width:100%;
	position:relative;	
}
.kf_party_schedule_day_2:before{
	content:"";
	position:absolute;
	top:0px;
	left:0px;
	width:0px;
	height:100%;
	z-index:10;	
}
.kf_party_schedule_day_2:after{
	content:"";
	position:absolute;
	right:0px;
	top:0px;
	width:0px;
	height:100%;
	z-index:10;	
}
.kf_party_schedule_day_2 figure{
	width:100%;
	float:left;
	position:relative;
	overflow:hidden;	
}
.kf_party_schedule_day_2 figure:before{
	content:"";
	position:absolute;
	top:0px;
	left:0px;
	width:100%;
	height:100%;
	opacity:0.5;	
}

.kf_party_schedule_day_des{
	position:absolute;
	top:25px;
	left:20px;
	z-index:20;	
}
.kf_party_schedule_day_des h5{
	color:#fff;
	font-size:24px;
	margin:0px 0px 10px;	
}
.kf_party_schedule_day_des ul{
	float:left;
	width:100%;	
}
.kf_party_schedule_day_des ul a{
	display:inline-block;
	color:#fff;
	font-size:14px;
	padding:0px 4px;	
}
.kf_party_schedule_day_des ul a:first-child{
	padding-left:0px;	
}
.kf_party_schedule_day_des ul a i{
	color:#fff;
	margin:0px 10px 0px 0px;	
}
.kf_party_schedule_detail{
	position:absolute;
	bottom:25px;
	left:20px;
	z-index:20;	
}
.kf_party_schedule_detail:before{
	content:"";
	position:absolute;
	top:0px;
	left:-20px;
	border-left:5px solid #ff4b7b;
	height:110%;	
}
.kf_party_schedule_detail span{
	color:#ffcfd8;
	display:block;	
}
.kf_party_schedule_detail p{
	color:#fff;
	margin:0px;		
}
/*Hover Style*/
.kf_party_schedule_day_2:hover:before,
.kf_party_schedule_day_2:hover:after{
	width:50%;	
}
/*
	  ==============================================================
		   Schedule Wrap Css End
	  ==============================================================
*/
.continue_reading {
    font: 400 11px "Raleway",sans-serif;
    letter-spacing: 1px;
    margin: 0 0 30px 300px;
    text-align: center;
    text-transform: uppercase;
}
 #tickets1:hover {
    background:#435469;
    color:#fff;
}
#tickets1 {
   
    color: #435469;
    font-size: 14px;
    font-weight: 700;
    line-height: 1;
    padding: 15px 35px;
    text-transform: uppercase;
    transition: all 0.2s ease-in-out 0s;
    width: 227px;
}
</style>


<!--Schedule Wrap Start-->
<div id="product_container">
        <div class="kf_party_schedule_bg">
        	<div class="container">
        		
            	<div class="kf_heading_1">
                	<h2 class="title">Event Name</h2>
                	<!--<span>Ipsum dolor sit amet, consectetuer adipiscing elit consectetuer</span>-->
                </div>
                
                <!--Schedule List Wrap Start-->
                <div class="row">
                	@foreach($featured as $fea)
                	<div class="col-md-6 no_padding" style="height:410px;">
                    	<div class="kf_party_schedule_day1">
                        	<figure>
                            	<img src="public/{{$fea->event_image}}" alt="Image Here">
                            </figure>
                          <a href="#"  data-toggle="modal" data-target="#myModal"> <i class="fa fa-calendar"></i>Book Now</a>
                            <span><a href="getEvent/{{$fea->event_id}}"> {{$fea->title}}</a></span>
                        </div>
                        
                        <div class="kf_schedule_day1_des">
                        
                            <p><i class="fa fa-calendar"></i>  {{$fea->location}}</p>
                            <ul>
                            	
                                <a href="#"><i class="fa fa-clock-o"></i>{{$fea->start_date}}</a>
                            </ul>
                            <div class="continue_reading" style="margin-top:50px;"><a href="getEvent/{{$fea->event_id}}" >...Readmore</a></div>
                        </div>
                        
                    </div>
                    @endforeach
                    @foreach($datas as $data)   
                    <div class="col-md-3 no_padding">
                    	<div class="kf_party_schedule_day_2">
                        	<figure>
                            	<img src="public/{{$data->event_image}}" alt="Image Here">
                            </figure>
                            <div class="kf_party_schedule_day_des">
                            	<h5><a href="getEvent/{{$data->event_id}}" style="color:#fff;">{{$data->title}}</a></h5>
                                <ul>
                                	<a href="#"><i class="fa fa-calendar"></i>{{$data->location}}</a>
                                    <a href="#"><i class="fa fa-clock-o"></i>{{$data->start_date}}</a>
                                </ul>
                                
                            </div>
                           
                        </div>
                    </div>
                    @endforeach
                    
                </div>
                <!--Schedule List Wrap End-->
                
            </div>
        </div>
        


 @foreach($datas as $data)
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            
                            <li class="active"><a href="#Registration" data-toggle="tab">Registration</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
       						 
                            <div class="tab-pane active" id="Registration">
                            	
                                  <form role="form" action="/send" method="post" enctype="multipart/form-data" class="form-horizontal" id="Registration" name="Registration">
                                	{{csrf_field()}}
                                 <div class="form-group">
                                    
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="fname" class="form-control" id="email" placeholder="Your First Name" />
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="lname" class="form-control" id="email" placeholder="Your Last Name" />
                                    </div>
                                    </div>
                                
                                <div class="form-group">
                                    
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="dob" class="form-control" id="dob" placeholder="Your Date of Birth" />
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="phone" class="form-control" id="phone" placeholder="Your Phone Number" />
                                    </div>
                                    </div>
                                <div class="form-group">
                                    
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="email" class="form-control" id="email" placeholder="Your Email" />
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="location" class="form-control" id="location" placeholder="Your Location" />
                                    </div>
                                    </div>
                                <div class="form-group">
                                    
                                    <div class="col-sm-12">
                                        <input type="institution" class="form-control" id="institution" placeholder="Your Institution Name" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <button type="submit" id="tickets1" class="btn btn-theme animated flipInY visible" data-label="Get ticket"><span>Get Now </span> <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>{{csrf_field()}}
                                <input type="hidden" name="event_id" id="event_id" value="{{$data->event_id}}">
								<input type="hidden" name="event_name" id="event_name" value="{{$data->title}}">
								<input type="hidden" name="price" id="price" value="{{$data->price}}">
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-4">
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3>
                                   Location</h3>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <p><b>{{$data->location}}</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3>
                                   Date & Time</h3>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <p><b>{{$data->start_date}}</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3>
                                   Price Per Ticket</h3>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <p><b>{{$data->price}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach     	
<script src="../public/assets/frontend/js/bootstrap.min.js"></script>