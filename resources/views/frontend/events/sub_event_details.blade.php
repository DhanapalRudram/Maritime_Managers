<title>Marine-Events</title>

<link href="../public/assets/frontend/cbm/slider.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/web.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
<!--<link rel="stylesheet" href="../assets/frontend/event/css/forms.css" type="text/css">
<link rel="stylesheet" href="../assets/frontend/event/css/template.css" type="text/css">
<link href="../assets/frontend/css/theme.css" rel="stylesheet">
<link href="../assets/frontend/css/theme-red-1.css" rel="stylesheet" id="theme-config-link">
<link href="../assets/frontend/css/custom.css" rel="stylesheet">-->
<link href="../public/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
	<style>
	.form-control
	{
	position: relative;
padding: 0 15px;
width: 100%;
height: 42px;
font: 300 12px 'Raleway', sans-serif;
text-overflow: ellipsis;
text-shadow: none;
color: #333;
background: #fff;
border: 1px solid #cdcdcd;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
}
		.overlay_te
		 {
		 	position: absolute;
			top: 35%;
			color: rgb(255, 255, 255);
			font-size: 60px;
			text-align: center;
			width: 100%;
		 }
		.continue_reading {
            font: 400 11px "Raleway",sans-serif;
            letter-spacing: 1px;
            margin: 40px 0 0px 40px;
            text-align: center;
            text-transform: uppercase;
            float: left;
        }
        #tickets1:hover {
            background:#435469;
            color:#fff;
        }
        #tickets1 {
   
    color: #435469;
    font-size: 14px;
    font-weight: 700;
    line-height: 1;
    padding: 15px 35px;
    text-transform: uppercase;
    transition: all 0.2s ease-in-out 0s;
    width: 227px;
}
	</style>
	
<div id="slider1"><div class="flexslider">
	@foreach($datas as $data) 
	<img draggable="false" src="../public/{{$data->event_image}}" style="width:100%;height:500px;">
				
		@endforeach
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper">
<div id="wrapper">
<div class="main section" id="main">
 
<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div class="post-outer1">
      
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>
 
<div class="post-summary">
    @foreach($datas as $data)
    <h1 align="left">{{$data->event_name}}</h1>
     
	<p style="padding-top: 20px;font-size: 14px;line-height: 40px;"> {{$data->description}}</p>
		@endforeach
	
	</div>

	</div>
	
</div>
 </div>
</div>
      
</div>


</div></div>
<aside id="sidebar">
	

<div class="sidebar section" id="sidebar-widget">

<div class="widget HTML" data-version="1" id="HTML5">
<h2 class="title">Location</h2>
<div class="widget-content">
<div id="recent-posts">
  <p> {{$data->location}}</p>
</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
<div class="widget HTML" data-version="1" id="HTML5">
<h2 class="title">Date and Time</h2>
<div class="widget-content">
<div id="recent-posts">
  <p>  <b>Sarting Date:</b> {{$data->start_date}}</p>
  <p>  <b>Ending Date:</b> {{$data->end_date}}</p>
</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget HTML" data-version="1" id="HTML7">

<div class="widget-content">
<!--<iframe scrolling="no" allowtransparency="true" style="border:none; overflow:hidden; width:300px; height:185px;" src="></iframe>-->
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
</aside>
<div class="clear"></div>
</div>
</div>
  	
@include('frontend.blog.includes.footer')
<script src="../public/assets/frontend/js/bootstrap.min.js"></script>
<!--<script>
	
	$('form#Registration').submit(function (){
		var formData = new FormData($(this)[0]);
		 var token = "{{ Session::getToken() }}";
		$.ajax({
		    url: "{{URL::to('getCheckout')}}",
		    type: "post",
		    data: formData,
		    async: false,
		    cache: false,
		    contentType: false,
		    processData: false,
		    success: function (JSONObject) {
		
		    }
	  });
	});
    
</script>-->