<title>Marine-Events</title>

<link href="../public/assets/frontend/cbm/slider.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/blogcss/web.css" rel="stylesheet" type="text/css">
<link href="../public/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
<!--<link rel="stylesheet" href="../assets/frontend/event/css/forms.css" type="text/css">
<link rel="stylesheet" href="../assets/frontend/event/css/template.css" type="text/css">
<link href="../assets/frontend/css/theme.css" rel="stylesheet">
<link href="../assets/frontend/css/theme-red-1.css" rel="stylesheet" id="theme-config-link">
<link href="../assets/frontend/css/custom.css" rel="stylesheet">-->
<link href="../public/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
	<style>
	.form-control
	{
	position: relative;
padding: 0 15px;
width: 100%;
height: 42px;
font: 300 12px 'Raleway', sans-serif;
text-overflow: ellipsis;
text-shadow: none;
color: #333;
background: #fff;
border: 1px solid #cdcdcd;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
}
		.overlay_te
		 {
		 	position: absolute;
			top: 35%;
			color: rgb(255, 255, 255);
			font-size: 60px;
			text-align: center;
			width: 100%;
		 }
		.continue_reading {
            font: 400 11px "Raleway",sans-serif;
            letter-spacing: 1px;
            margin: 40px 0 0px 40px;
            text-align: center;
            text-transform: uppercase;
            float: left;
        }
        #tickets1:hover {
            background:#435469;
            color:#fff;
        }
        #tickets1 {
   
    color: #435469;
    font-size: 14px;
    font-weight: 700;
    line-height: 1;
    padding: 15px 35px;
    text-transform: uppercase;
    transition: all 0.2s ease-in-out 0s;
    width: 227px;
}
	</style>
	
<div id="slider1"><div class="flexslider">
	@foreach($datas as $data) 
	<img draggable="false" src="../public/{{$data->event_image}}" style="width:100%;height:500px;">
				
		@endforeach
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper">
<div id="wrapper">
<div class="main section" id="main">
 
<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div id="product_container">

 @foreach($datas as $data)       
<div class="post-outer1">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
    <h1 align="left">{{$data->title}}</h1>
     <div class="continue_reading" ><a href="#" style="text-decoration:none;float:right;margin-top: -85px;margin-right:-675px; background-color:#061c42;color:#fff;" data-toggle="modal" data-target="#myModal">Book Now</a></div>
   
	<p style="padding-top: 20px;font-size: 14px;line-height: 40px;"> {{$data->description}}</p>
	
	
	</div>
	</div>
</div>
@endforeach

</div>


 </div>
</div>
      
</div>


</div></div>
<aside id="sidebar">
	

<div class="sidebar section" id="sidebar-widget">

<div class="widget HTML" data-version="1" id="HTML5">
<h2 class="title">Location</h2>
<div class="widget-content">
<div id="recent-posts">
  <p> {{$data->location}}</p>
</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div>
<div class="widget HTML" data-version="1" id="HTML5">
<h2 class="title">Date and Time</h2>
<div class="widget-content">
<div id="recent-posts">
  <p>  <b>Sarting Date:</b> {{$data->start_date}}</p>
  <p>  <b>Ending Date:</b> {{$data->end_date}}</p>
</div>
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div><div class="widget HTML" data-version="1" id="HTML7">

<div class="widget-content">
<!--<iframe scrolling="no" allowtransparency="true" style="border:none; overflow:hidden; width:300px; height:185px;" src="></iframe>-->
</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#">
<img alt="" src="../assets/frontend/blogimg/icon18_wrench_allbkg.png" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
</aside>
<div class="clear"></div>
</div>
</div>
    <!-- Large modal -->
 @foreach($datas as $data)
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                
            </div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                        <!-- Nav tabs 
                        <ul class="nav nav-tabs">
                            
                            <li class="active"><a href="#Registration" data-toggle="tab">Registration</a></li>
                        </ul>-->
                        <!-- Tab panes -->
                        <div class="tab-content">
       						 
                            <div class="tab-pane active" id="Registration">
                            	
                                <form role="form" action="#" method="post" enctype="multipart/form-data" class="form-horizontal" id="Registration" name="Registration">
                                	{{csrf_field()}}
                                 <div class="form-group">
                                    
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="fname" class="form-control" id="email" placeholder="Your First Name" />
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="lname" class="form-control" id="email" placeholder="Your Last Name" />
                                    </div>
                                    </div>
                                
                                <div class="form-group">
                                    
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="dob" class="form-control" id="dob" placeholder="Your Date of Birth" />
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="phone" class="form-control" id="phone" placeholder="Your Phone Number" />
                                    </div>
                                    </div>
                                <div class="form-group">
                                    
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="email" class="form-control" id="email" placeholder="Your Email" />
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <input type="location" class="form-control" id="location" placeholder="Your Location" />
                                    </div>
                                    </div>
                                <div class="form-group">
                                    
                                    <div class="col-sm-12">
                                        <input type="institution" class="form-control" id="institution" placeholder="Your Institution Name" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <button type="submit" id="tickets1" class="btn btn-theme animated flipInY visible" data-label="Get ticket"><span>Get Now </span> <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>{{csrf_field()}}
                                <input type="hidden" name="event_id" id="event_id" value="{{$data->event_id}}">
								<input type="hidden" name="event_name" id="event_name" value="{{$data->title}}">
								<input type="hidden" name="price" id="price" value="{{$data->price}}">
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-4">
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3>
                                   Location</h3>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <p><b>{{$data->location}}</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3>
                                   Date & Time</h3>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <p><b>{{$data->start_date}}</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3>
                                   Price Per Ticket</h3>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <p><b>{{$data->price}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach     	
@include('frontend.blog.includes.footer')
<script src="../public/assets/frontend/js/bootstrap.min.js"></script>
<!--<script>
	
	$('form#Registration').submit(function (){
		var formData = new FormData($(this)[0]);
		 var token = "{{ Session::getToken() }}";
		$.ajax({
		    url: "{{URL::to('getCheckout')}}",
		    type: "post",
		    data: formData,
		    async: false,
		    cache: false,
		    contentType: false,
		    processData: false,
		    success: function (JSONObject) {
		
		    }
	  });
	});
    
</script>-->