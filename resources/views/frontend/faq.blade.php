<title>Marine-FAQ</title>
@include('frontend.includes.links')
@include('frontend.blog.includes.links')

<style>
.widget {
    padding: 7px 0px;
}
    .container1 {
    width: 970px;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-top: -50px;
}
.row1 {
    margin-right: -15px;
    margin-left: -15px;
}
.col-md-6 {
    width: 50%;
}.row.faq .tab-content {
    background-color: #fdfdfd;
    border: 1px solid #435469;
    border-radius: 2px;
    font-size: 14px;
    line-height: 24px;
    padding: 6px 30px 10px;
    position: relative;
} 
.row.faq .tab-content {
    background-color: #fdfdfd;
    border: 1px solid #435469;
    border-radius: 2px;
    font-size: 14px;
    height: 257px;
    line-height: 24px;
    padding: 25px 30px 10px;
    position: relative;
}
.row.faq .nav li a {
    padding-bottom: 13px !important;
    padding-left: 20px;
    border-radius: 10px;
    border: solid 1px #435469;
    background-color: #fdfdfd;
    color: #374146;
   
}
</style>
@include('frontend.blog.includes.header')

<h1 align="center" style="margin-top: -50px;margin-bottom: 50px;text-transform:uppercase;">Event FAQS</h1>
<div id="contentwrapper">
<div id="wrapper">
      <section class="page-section light">
            <div class="container1">
                <!--<div class="row1">
                    <div class="col-md-8 pull-left">
                        <h1 class="section-title">
                            <span data-animation="flipInY" data-animation-delay="300" class="icon-inner animated flipInY visible"><span class="fa-stack"><i class="fa fa-question fa-stack-1x"></i></span></span>
                            <span data-animation="fadeInRight" data-animation-delay="500" class="title-inner animated fadeInRight visible">Event FAQS </span>
                        </h1>
                    </div>
                   
                </div>-->
                <div class="row faq margin-top animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="col-sm-6 col-md-6 pull-left">
                        <ul id="tabs-faq" class="nav">
                            <li class="active"><a href="#tab-faq1" data-toggle="tab"><i class="fa fa-angle-right"></i> <span class="faq-inner">How to Join Event</span></a></li>
                            <li><a href="#tab-faq2" data-toggle="tab"><i class="fa fa-plus"></i> <span class="faq-inner">How to Cancel Arrival?</span></a></li>
                            <li><a href="#tab-faq3" data-toggle="tab"><i class="fa fa-plus"></i> <span class="faq-inner">Can I Get Refund?</span></a></li>
                            <li><a href="#tab-faq4" data-toggle="tab" style="padding-top:10px !important;"><i class="fa fa-plus" ></i> <span class="faq-inner">How to Make Reservation?</span></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-6 pull-right">
                        <div class="tab-content">
                            <div id="tab-faq1" class="tab-pane fade in active">
                                <div>
                                    <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat quis
                                        tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                        fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-faq2" class="tab-pane fade">
                                <div>
                                    <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat quis
                                        tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                        fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-faq3" class="tab-pane fade">
                                <div>
                                    <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat quis
                                        tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                        fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-faq4" class="tab-pane fade">
                                <div>
                                    <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat quis
                                        tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                        fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
<script src="public/assets/frontend/js/bootstrap.min.js"></script>