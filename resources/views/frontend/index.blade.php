@include('frontend.includes.links')
@include('frontend.includes.header')
<title>Marine-Home</title>
    <!-- Content area -->
    <div class="content-area">

        <div id="main">
        <!-- SLIDER -->
        <section class="page-section no-padding background-img-slider">
            <div class="container">
         
            <div id="main-slider" class="owl-carousel owl-theme owl-loaded">
            	 <video   style="width:100%;" autoplay loop >
							  <source src="public/assets/frontend/Sail-Away.webm" type="video/webm" >						  
						</video>

                </div>
            </div>

            <!-- Event description -->
            <div class="event-description">
                <div class="container">
                <div class="row">
                    <div class="col-xs-12 ">
                <div class="event-background">
                    <div class="container-fluid">
                        <div class="row">
                        	@foreach($events as $event)
                        	
                            <div class="col-xs-12 col-sm-6 col-md-3">
                            	
                                <div class="media">
                                            <span class="pull-left">
                                                <i class="fa fa-calendar fa-2x"></i>
                                            </span>
                                    <div class="media-body">
                                        <h4 class="media-heading">Date</h4>
                                        <span>{{$event->start_date}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="media">
                                            <span class="pull-left">
                                                <i class="fa fa-map-marker fa-2x"></i>
                                            </span>
                                    <div class="media-body">
                                        <h4 class="media-heading">Location</h4>
                                        <span>{{$event->location}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="media">
                                            <span class="pull-left">
                                                <i class="fa fa-group fa-2x media-object"></i>
                                            </span>
                                    <div class="media-body">
                                        <h4 class="media-heading">Remaining</h4>
                                        <span>{{$event->tickets}} Tickets</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="media">
                                            <span class="pull-left">
                                                <i class="fa fa-microphone fa-2x"></i>
                                            </span>
                                    <div class="media-body">
                                        <h4 class="media-heading">Speakers</h4>
                                        <span>{{$speaker}} Professional Speakers</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
            </div>
            <!-- /Event description -->

        </section>
        <!-- /SLIDER -->
        </div>

       <!-- PAGE LOCATION -->
        <section class="page-section" id="location" >
            <div class="container full-width gmap-background">

                <div class="container">
                    <div class="on-gmap color">
                        <h1 class="section-title">
                            <h2><i class="i-icon i-ion-person-add m-colorized"></i>Event<span class="m-colorized">Name</span></h2>
                        </h1>
                         <!<p>Subheading or short description of this section can be here</p>
                       
                        <a href="#" id="tickets" class="btn btn-theme animated flipInY visible" data-animation="flipInY" data-animation-delay="300">Get Ticket <i class="fa fa-arrow-circle-right"></i></a>
                        </br></br>
                        <a href="/Events" class="btn btn-theme animated flipInY visible" data-animation="flipInY" data-animation-delay="300">Browse More <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    <div class="">
                    	 <!-- Sign Up section -->
      <section class="b-signUp b-section b-section__background" id="signUp">
          <div class="b-section__background_overlay">

              <div class="b-section_container col-xs-12">

                  <header class="b-section_header b-section_header__stroked">
                      <h2><i class="i-icon i-ion-person-add m-colorized"></i>Register <span class="m-colorized">right now!</span></h2>
                      
                      <a href="#" class="close_signup"><i class="fa fa-times" style="font-size: 20px;color: rgb(255, 255, 255);width: 35px;height: 40px;
line-height: 38px;" ></i></a>
                  </header>

                  <div class="b-signUp_form b-form">
                      <form  method="post" action="/send" data-checkup="true" data-xhr="true" name="event" id="event">
						{{csrf_field()}}
                          <div class="row">
                          	
							@foreach($events as $event)
							    <input type="hidden" name="event_id" id="event_id" value="{{$event->event_id}}">
								<input type="hidden" name="event_name" id="event_name" value="{{$event->title}}">
								<input type="hidden" name="price" id="price" value="{{$event->price}}">}}">
							@endforeach
                              <!-- Sign Up section left -->
                              <div class="col-xs-6 col-md-6 col-lg-6 col-lg-offset-1">
                                <div class="row">
                                     <div class="col-xs-12 col-sm-6">
                                        <div class="b-form_box">
                                          <div class="b-form_box_field i-icon i-ion-person">
                                              <input name="fname" placeholder="Your First Name" data-required="" type="text">
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="b-form_box">
                                          <div class="b-form_box_field i-icon i-ion-person">
                                              <input name="lname" placeholder="Your Last Name" data-required="" type="text">
                                          </div>
                                        </div>
                                    </div>
                                      <div class="col-xs-12 col-sm-6">

                                          <div class="b-form_box">
                                              <div class="b-form_box_field i-icon i-ion-person">
                                                  <input name="dob" placeholder=" Date of Birth" data-required="" type="text">
                                              </div>

                                          </div>

                                      </div>

                                      <div class="col-xs-12 col-sm-6">

                                          <div class="b-form_box">
                                            <div class="b-form_box_field i-icon i-ion-person">
                                              <input name="phone" placeholder="Your Phone Number" data-required="" type="text">
                                            </div>
                                          </div>

                                      </div>
                                      
                                      <div class="col-xs-12 col-sm-6">

                                         <div class="b-form_box">
                                          <div class="b-form_box_field i-icon i-ion-email">
                                              <input name="email" placeholder="Your E-mail Address" data-required="" data-pattern="^[0-9a-z_.-]+@([a-z0-9_-]+\.)+[a-z]{2,}$" data-pattern-type="email" type="text">
                                          </div>
    
                                          </div>
                                      </div>
                                    <div class="col-xs-12 col-sm-6">

                                        <div class="b-form_box">
    
                                          <div class="b-form_box_field i-icon i-ion-ios-telephone ">
                                              <input name="location" placeholder="Your Location" data-required="" type="text">
                                          </div>

                                          </div>
                                      </div>
                                     <div class="col-xs-12 col-sm-12">

                                        <div class="b-form_box">
    
                                          <div class="b-form_box_field i-icon i-ion-ios-telephone ">
                                              <input name="location" placeholder="You Institution name" data-required="" type="text">
                                          </div>

                                          </div>
                                      </div>
                                  </div>
                                  <div class="b-signUp_form_bottom b-form_bottom b-form_bottom__center">

                                      <button type="submit" id="tickets1" class="btn btn-theme animated flipInY visible" data-label="Get ticket"><span>Buy Now </span> <i class="fa fa-arrow-circle-right"></i></button>
                                      <!--<a href="#"  class="btn btn-theme animated flipInY visible" data-animation="flipInY" data-animation-delay="300" data-label="Get ticket">BUY Now <i class="fa fa-arrow-circle-right"></i></a>-->

                                  </div>

                              </div>
                              <!-- Sign Up section left end -->

                              

                          </div>

                      </form>
                  </div>

              </div>
          </div>
      </section>
                    </div>
                </div>

                <!-- Google map -->
                <div class="google-map">
                    <div id="map-canvas" style="position: relative; overflow: hidden; background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0); will-change: transform;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 432px; top: 35px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 432px; top: 291px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 688px; top: 35px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 688px; top: 291px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 432px; top: 547px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 432px; top: -221px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 688px; top: -221px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 176px; top: 35px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 944px; top: 35px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 176px; top: 291px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 944px; top: 291px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 688px; top: 547px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 944px; top: -221px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 176px; top: -221px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 176px; top: 547px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 944px; top: 547px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: -80px; top: 291px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 1200px; top: 291px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 1200px; top: 35px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: -80px; top: 35px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 1200px; top: -221px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: -80px; top: -221px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: -80px; top: 547px;">
                    	
                    </div>
                    <div style="width: 256px; height: 256px; position: absolute; left: 1200px; top: 547px;">
                    	
                    </div>
                    </div>
                    </div>
                    </div>
                    <div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;">
                    	
                    </div>
                    <div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;">
                    	
                    </div>
                    <div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;">
                    	<div style="position: absolute; left: 0px; top: 0px; z-index: -1;">
                    		<div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                    			<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 432px; top: 35px;">
                    				<canvas draggable="false" height="192" width="192" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 432px; top: 291px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 688px; top: 35px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 688px; top: 291px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 432px; top: 547px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 432px; top: -221px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 688px; top: -221px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 176px; top: 35px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 944px; top: 35px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 176px; top: 291px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 944px; top: 291px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 688px; top: 547px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 944px; top: -221px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 176px; top: -221px;">
                    					
                    				</div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 176px; top: 547px;">
                    					
                    				</div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 944px; top: 547px;">
                    					
                    				</div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -80px; top: 291px;">
                    					
                    				</div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1200px; top: 291px;">
                    					
                    				</div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1200px; top: 35px;"></div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -80px; top: 35px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1200px; top: -221px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -80px; top: -221px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -80px; top: 547px;"></div>
                    				<div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1200px; top: 547px;"></div>
                    				</div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                    					<div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                    						<div style="position: absolute; left: 432px; top: 291px; transition: opacity 200ms ease-out;">
                    							<img src="public/assets/frontend/css/vt" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 432px; top: 547px; transition: opacity 200ms ease-out;">
                    								<img src="public/assets/frontend/css/vt(1)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                    								</div><div style="position: absolute; left: 688px; top: 291px; transition: opacity 200ms ease-out;">
                    									<img src="public/assets/frontend/css/vt(2)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 432px; top: 35px; transition: opacity 200ms ease-out;">
                    										<img src="public/assets/frontend/css/vt(3)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 432px; top: -221px; transition: opacity 200ms ease-out;">
                    											<img src="public/assets/frontend/css/vt(4)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 688px; top: 547px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(5)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 176px; top: 547px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(6)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 176px; top: -221px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(7)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -80px; top: 291px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(8)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 944px; top: 35px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(9)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 688px; top: -221px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(10)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1200px; top: 291px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(11)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1200px; top: 35px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(12)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 688px; top: 35px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(13)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 176px; top: 35px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(14)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 176px; top: 291px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(15)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -80px; top: -221px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(16)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1200px; top: 547px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(17)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -80px; top: 547px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(18)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -80px; top: 35px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(19)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 944px; top: -221px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(20)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 944px; top: 547px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(21)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 944px; top: 291px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(22)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1200px; top: -221px; transition: opacity 200ms ease-out;"><img src="public/assets/frontend/css/vt(23)" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=40.980765,28.986652&amp;z=12&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="public/assets/frontend/css/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 488px; top: 198px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="public/assets/frontend/css/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 167px; bottom: 0px; width: 120px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 96px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="public/assets/frontend/css/sv5.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@40.9807648,28.9866516,12z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 106px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;"><div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; width: 28px; height: 55px; background-color: rgb(255, 255, 255);"><div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="public/assets/frontend/css/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; top: 0px; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="public/assets/frontend/css/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; position: absolute; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="public/assets/frontend/css/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="public/assets/frontend/css/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="public/assets/frontend/css/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; display: none; background-color: rgb(255, 255, 255);"><img src="public/assets/frontend/css/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="public/assets/frontend/css/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show street map" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 21px; font-weight: 500; background-color: rgb(255, 255, 255); background-clip: padding-box;">Map</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 29px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show street map with terrain" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="public/assets/frontend/css/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show satellite imagery" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-left: 0px; min-width: 39px; background-color: rgb(255, 255, 255); background-clip: padding-box;">Satellite</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 29px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show imagery with street names" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="public/assets/frontend/css/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div></div></div>
                </div>
                <!-- /Google map -->

            </div>
        </section>
      
      <!-- Sign Up end -->
       

        <!-- MARKET SHOW -->
        <section class="page-section light" id="speakers">
            <div class="container">
                <h1 class="section-title">
                    <span data-animation="flipInY" data-animation-delay="300" class="icon-inner animated flipInY visible">
                        <span class="fa-stack">
                        <i class="fa fa-microphone fa-2x"></i></span></span>
                    <span data-animation="fadeInUp" data-animation-delay="500" class="title-inner animated fadeInUp visible">Esteemed Speakers</span>
                </h1>

                <!-- Speakers row -->
                <div class="row  clear">
                	@foreach($speakers as $speaker)
                	<!-- -->
                    <div class="col-md-3">
                        <article class="post-wrap animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="100" style="background:none !important;">
                            <div class="post-media">
                                
                                <img src="public/{{$speaker->author_image}}" alt="" width="890px" height="250px">
                            </div>
                            <div class="post-header">
                                <h2 class="post-title"><a href="getSpeakers/{{$speaker->speakers_id}}"><b>{{$speaker->author_name}}</b></a></h2>
                                <div class="post-meta">
                                    <span class="post-date">
                                       {{$speaker->profession}}
                                    </span>
                                    
                                </div>
                            </div>
                            
                                    <a href="getSpeakers/{{$speaker->speakers_id}}" style="float:right;color:#2592D0;">...Know more</a>
                                
                            
                        </article>
                    </div>
                    
                    @endforeach
                    
                </div>
                <!-- /Speakers row -->

                <div class="text-center margin-top">
                    <a data-animation="fadeInUp" data-animation-delay="100" href="Speakershub" class="btn btn-theme animated fadeInUp visible"><i class="fa fa-user"></i> See Speakers</a>
                </div>
            </div>
        </section>
        <!-- /MARKET SHOW -->


        <!-- PAGE POSTS -->
        <section class="page-section">
            <div class="container">
                <h1 class="section-title">
                    <span data-animation="flipInY" data-animation-delay="300" class="icon-inner animated flipInY visible"><span class="fa-stack"><i class="fa fa-file-text-o fa-stack-1x"></i></span></span>
                    <span data-animation="fadeInRight" data-animation-delay="500" class="title-inner animated fadeInRight visible">Recent Blog Posts</span>
                </h1>
               
                <div class="row post-row">
					@foreach($blogs as $blog)
                    <!-- -->
                    <div class="col-md-3">
                        <article class="post-wrap animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="100">
                            <div class="post-media">
                                
                                <img src="public/{{$blog->blog_image}}" alt="" width="890px" height="250px">
                            </div>
                            <div class="post-header">
                                <h2 class="post-title"><a href="getBlog/{{$blog->blog_id}}">{{$blog->title}}</a></h2>
                                <div class="post-meta">
                                  
                                </div>
                            </div>
                            <div class="post-body">
                                <div class="post-excerpt">
                                    <p>{{ str_limit($blog->description, $limit = 140) }}<a href="getBlog/{{$blog->blog_id}}" style="color:#2592D0;">Read more</a></p>
                                </div>
                            </div>
                            
                                
                        </article>
                    </div>

                    @endforeach
                </div>
                
                <div class="text-center margin-top">
                    <a data-animation="flipInY" data-animation-delay="100" href="Blog" class="btn btn-theme animated flipInY visible"><i class="fa fa-file-text-o"></i> Visit Blog</a>
                </div>
            </div>
        </section>
        <!-- /PAGE POSTS -->

        

   @include('frontend.includes.footer')
<script>
	$('form.subscribe-form').submit(function(event){
		 event.preventDefault();
		var token = "{{ Session::getToken() }}";
		var email=$('#email').val();
		$.ajax({
			        type: 'post',
			        url: "{{URL::to('subscribe')}}",
			        data: "_token="+ token+"&email="+email,
			        success: function (data) {
			        	if(data=='success')
			        	{
			        		$(".message1").html('Mail Send Successfully').css("color","green");
            	      		$(".widget_wysija").load(location.href + " .widget_wysija>*", "");
			        	}
            	      	else
            	      	{
            	      		$(".message1").html('Error').css("color","red");
            	      	}
            	    }
			        
		    	});
	
	});
</script>