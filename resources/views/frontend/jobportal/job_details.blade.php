<title>Marine-Job Details</title>
 
<link href="../public/assets/frontend/blogcss/blog.css" rel="stylesheet" type="text/css">
<style>
	.overlay_te
	 {
	 	position: absolute;
		top: 50%;
		color: rgb(255, 255, 255);
		font-size: 60px;
		text-align: center;
		width: 100%;
	 }
	 .post-outer3 
	 {
	    background: #fff none repeat scroll 0 0;
	    border-radius: 1px;
	    box-shadow: 0 0 5px rgba(0, 0, 0, 0.02);
	    margin: 0 auto;
	    min-height: auto;
	    width: 49%;
	    position: relative;
	}
	.post-outer {
    background: #fff none repeat scroll 0 0;
    border-radius: 1px;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.02);
    float: left;
    margin: 0.5%;
    min-height: auto;
    width: 100%;
}
.post-summary {
    background: #fff none repeat scroll 0 0;
    border-radius: 1px;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.02);
    padding: 0 0 1.5%;
    float: left;
    width: 100%;
}
</style>

@include('frontend.blog.includes.header')
<div class="featured-area section" id="featured-area"><div class="widget HTML" data-version="1" id="HTML6">
<h2 class="title" style="display: none;">Featured Slider</h2>
<div class="widget-content">
<div id="slider1"><div class="flexslider">
 
	<img draggable="false" src="../public/assets/frontend/blogimg/JOb Cover.jpg" style="width:100%;height:500px;">
			
		
			</div>
		<style type="text/css">#slider1 .flexslider.loading {min-height:530px !important;}</style>
	</div>

</div>
<div class="clear"></div>
<span class="widget-item-control">
<span class="item-control blog-admin">
<a class="quickedit" href="#" onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML6"));' target="configHTML6" title="Edit">
<img alt="" src="../public/assets/frontend/blogimg/miranda-lambert_003.jpg" height="18" width="18">
</a>
</span>
</span>
<div class="clear"></div>
</div></div>
<div id="contentwrapper" style="margin-top: -143px;">
<div id="wrapper">
<div class="main section1" id="main">


<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed">
   
   <div class="date-outer">
      <div class="date-posts">
<div id="product_container">
@foreach($datas as $data)   
     
<div class="post-outer3" style="background:none !important;">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>
<p align="center"><img src="../public/{{$data->company_logo}}" style="border-radius: 80px;"></p>
<div class="post-summary">
	  <p align="center" style="font-size:20px;text-transform:uppercase;padding: 15px 0 0;">{{$data->company_name}}</p>
	  <p align="center" style="font-size:20px;text-transform:uppercase;padding: 15px 0 0;">{{$data->title}}</p>	
	  	<p align="center" style="float:left;width:28%;">
            <i class="fa fa-map-marker fa-2x">{{$data->location}}</i>
           
	  	</p>	
	  	<p align="center" style="float:left;width:28%;border-left: 1px solid #000;border-right: 1px solid #000;">
            <i class="fa fa-map-marker fa-2x">{{$data->start_date}}</i>
           
	  	</p>	
	  	<p align="center" style="float:left;width:28%;">
            <i class="fa fa-map-marker fa-2x">{{$data->end_date}}</i>
           
	  	</p>	
	   
	 </div>
	 
	</div>
</div>
@endforeach

</div>

<div id="product_container">
@foreach($datas as $data)   
     
<div class="post-outer">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
	  
	   <p style="padding-top: 20px;font-size: 14px;line-height: 40px;">
	    {{$data->description}}
	     </p>
		
	 </div>
	 
	</div>
</div>
@endforeach

</div>
 </div>
</div>
      
</div>


</div></div>



<div class="clear"></div>
</div>
</div>
@include('frontend.blog.includes.footer')
