<link href="public/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
	.post-summary p {
    color: #191919;
    font: 14px/1.8 "Droid Serif",serif;
    margin: 0 0 0px 0 !important;
   
}
#wrapper,#contentwrapper {
    background: none;
    
}
.post-outer1 {
    background: #fff none repeat scroll 0 0;
    border-radius: 1px;
    box-shadow: 1px 3px 8px #f1f1f1;
    float: left;
    width: 100%;
    margin-bottom: 10px;
    
}
.post-summary {
    background: #fff none repeat scroll 0 0;
    border-radius: 1px;
    
    padding: 0 0 1.5%;
    
}
 .green
 {
 background:green none repeat scroll 0 0 !important;
 color:#fff !important;
 }
  .red
 { 
 background:red none repeat scroll 0 0 !important;
 color:green !important;
 }
</style>
<div id="product_container">

<div class="widget Blog" data-version="1" id="Blog1">
<div class="blog-posts hfeed casestudy">
   
   <div class="date-outer">
      <div class="date-posts">

@foreach($datas as $data)   
     
<div class="post-outer1">
  <div class="post hentry uncustomized-post-template" itemprop="blogPost" itemscope="itemscope" itemtype="#">
    <div itemtype="#" itemprop="mainEntityOfPage" itemscope="itemscope"></div>

<div class="post-summary">
 
	   <div class="col-md-3"><img src="../public/{{$data->company_logo}}" style="width: 134px;height: 150px;margin-top: 10px;border-radius: 134%;margin-left: 20px;"></div>
	   <div class="col-md-9">
	   	 <h3><a href="jobDetails/{{$data->job_id}}">{{$data->title}}</a></h3>
	   <p ><b>Company:</b> {{$data->company_name}} &nbsp;&nbsp;&nbsp;<br><b> Vacancies:</b>  {{ $data->vacancies}}</p>
	   <p>
	  <b> Start Date:</b>{{ $data->start_date}}&nbsp;&nbsp;&nbsp; <br><b>Closing Date:</b>  {{$data->end_date}}
	     </p>
	     
	   
		<div class="continue_reading" style="margin-top:-40px;" ><a @if($data->status=='Published') class="green" @elseif($data->status=='Pending') class="red" @endif >@if($data->status=='Published') Open @elseif($data->status=='Pending') Closed @endif</a></div>
	 <div class="continue_reading" style="margin-top:-40px;" ><a href="#" style="text-decoration:none" data-toggle="modal" data-target="#myModal">Apply</a></div>
	 </div>
	 </div>
	 
</div>
	
</div>
@endforeach

<div class="pagen">{{ $datas->links() }} </div>
<script src="public/assets/frontend/js/bootstrap.min.js"></script>