<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Event;
class SubEvent extends Model
{
    public function events()
    {
       return $this->belongsTo('App\Events','event_id');
    }
}
