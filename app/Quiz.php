<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $table="quiz";
    
    public  function questions()
    {
        $this->hasMany('App\Questions',quiz_id);
    }
}
