<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Auth;

use Config;

use Response;
use Mailchimp;
use Mail;

class MailChimpController extends Controller

{
    protected $mailchimp;
    protected $listId = '29bba85f6d'; 
		
   
   //Contact Page View
	public function contactus()
	{
		return view('contactus');
	}
	//Sending mail part
	public function sendmail(Request $request)	
	{		
			
		$data =[
		       
		        'to' => $request['email'],
		        'name' => $request['fname'],
		        'subject' =>$request['subject'],
		        'message' =>$request['message'],
		        ];
		        
				Mail::queue($data,$data,function ($message) use ($data) {
				$message->From('boopathit98@gmail.com', 'boopathi');	
		        $message->Subject("hi");
		        $message->to('dhanapalmca88@gmail.com','CEO');
		        });
				
		        Mail::queue('email.notify', $data, function ($message) use ($data) {		        
		        $message->Subject($data['subject']);
		       // $message->setBody($data['message']);
		        $message->to($data['to'], $data['name']);
		        });	
			
				$merge_vars = array('FNAME'=>$request['fname'],'LNAME'=>$request['lname'],'SUB'=>$request['subject'],'MSG'=>$request['message']);				
			 	$status = $this->mailchimp->lists->subscribe(
				$this->listId,
				 ['email' => $request['email']],
				 
                $merge_vars, // merge vars
                'html', // email type
                false, // requires double optin
                false, // update existing members
                true
                );
			return view('contactus');
	}
	
    
 
 }