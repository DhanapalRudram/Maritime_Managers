<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use App\Questions;
use App\Quiz;
use App\Answers;
use App\QuizUsers;
use Illuminate\Support\Facades\Input;
class QuizController extends Controller
{
    public function index(Request $request)
	{
		$datas =Quiz:: where('status','Published')->paginate(2);
		//print_r($datas);exit;
        if ($request->ajax()) {
            return view('frontend.quiz.result')->with([
            'datas' => $datas
        ]);
        }
        else {
        
        return view('frontend.quiz.index')->with([
            'datas' => $datas
        ]);
        }
	}
	public function getQuiz($id)
	{
	    $datas=Quiz::where('quiz_id',$id)->paginate(1);
	    return view('frontend.quiz.quiz_details',['datas'=>$datas]);
	}
	
	public function playQuiz(Request $request,$id)
	{
		
		$user[]=QuizUsers::where('id',$id)->get();
		$id1=$user[0][0]['quiz_id'];
	    $datas =Questions:: where('quiz_id',$id1)->paginate(1);
	    $count =Questions:: where('quiz_id',$id1)->count();
		$timer=Quiz::where('quiz_id',$id1)->get();
	//	print_r($datas);exit;
        if ($request->ajax()) {
            return view('frontend.quiz.question_result')->with([
            'datas' => $datas,
            'timer'=>$timer,
            'id'=>$id,
            'count'=>$count
            
        ]);
        }
        else {
        
        return view('frontend.quiz.start')->with([
            'datas' => $datas,
            'timer'=>$timer,
            'id'=>$id,
            'count'=>$count
        ]);
        }
	}
	public function insertQuizUsers()
	{
		
		$exis=QuizUsers::where('quiz_id',Input::get('quiz_id'))->where('email',Input::get('email'))->get();
	//	print_r($exis);
		if($exis==null)
		{
			$user=new QuizUsers();
			$user->name=Input::get('name');
			$user->email=Input::get('email');
			$user->quiz_id=Input::get('quiz_id');
			$user->quiz_name=Input::get('quiz_name');
			$user->save();
			$lastid=$user->id;
			return Response::json($lastid,200);
		}
		else
		{
			$lastid=$exis[0]->id;
			return Response::json($lastid,200);
		}
	}
	public function insertAnswers()
	{   
		$res=Questions::where('id',Input::get('question_id'))->where('quiz_id',Input::get('quiz_id'))->get();
	    $answer=$res[0]->answer;
	    if($answer==Input::get('user_answer'))
	    {
	    	$status=1;
	    }
	    else{
	    	$status=0;
	    }
	    //echo $status;exit;
	    $del=Answers::where('question_id',Input::get('question_id'))->delete();
	    $data=new Answers();
	    $data->question_id=Input::get('question_id');
	    $data->quiz_id=Input::get('quiz_id');
	    $data->user_choice=Input::get('user_answer');
	    $data->user_id=Input::get('user_id');
	    $data->status=$status;
	    $data->save();
	    
	}
	public function quizResults()
	{
	    $res=Questions::where('id',Input::get('question_id'))->where('quiz_id',Input::get('quiz_id'))->get();
	    $answer=$res[0]->answer;
	    if($answer==Input::get('user_answer'))
	    {
	    	$status=1;
	    }
	    else{
	    	$status=0;
	    }
	    $del=Answers::where('question_id',Input::get('question_id'))->delete();
	    $data=new Answers();
	    $data->question_id=Input::get('question_id');
	    $data->quiz_id=Input::get('quiz_id');
	    $data->user_choice=Input::get('user_answer');
	    $data->user_id=Input::get('user_id');
	    $data->status=$status;
	    $data->save();
	    $result=0;
	    $questions=Questions::where('quiz_id',Input::get('quiz_id'))->get();
	    $quiz_ans=Answers::where('quiz_id',Input::get('quiz_id'))->get();
	    foreach($questions as $ans)
	    {
	        foreach($quiz_ans as $quiz)
	        {
	            if($ans->answer == $quiz->user_choice)
	            {
	                $result++;
	               
	            }
	        }
	    }
	    
	    return response::json($result,200);
	}

}
