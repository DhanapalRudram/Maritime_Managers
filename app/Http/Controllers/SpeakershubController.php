<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Speakershub;
class SpeakershubController extends Controller
{
    public function index(Request $request)
	  {
		$latest=Speakershub::where('status','Published')->orderBy('created_at', 'desc')->limit(3)->get();
		$casestudy=Speakershub::where('category','CaseStudy')->count();
		$ebook=Speakershub::where('category','E-Books')->count();
		$featured=Speakershub::where('status','Legend')->get();
		$datas =Speakershub:: where('status','Published')->paginate(4);
        if ($request->ajax()) {
            return view('frontend.speakershub.result')->with([
            'datas' => $datas,
            'featured' => $featured,
            'casestudy'=>$casestudy,
            'ebook'=>$ebook,
            'latest'=>$latest
        ]);
        }
        else {
        
        return view('frontend.speakershub.index')->with([
            'datas' => $datas,
            'featured' => $featured,
            'casestudy'=>$casestudy,
            'ebook'=>$ebook,
            'latest'=>$latest
        ]);
        }
	  }
	
	public function getSpeakers($id)
	{
		  $latest=Speakershub::where('status','Published')->orderBy('created_at', 'desc')->limit(3)->get();
		  $casestudy=Speakershub::where('category','CaseStudy')->count();
		  $ebook=Speakershub::where('category','E-Books')->count();	  
		  $featured=Speakershub::where('status','Legend')->get();
		  $datas =Speakershub:: where('speakers_id',$id)->get();       
        
        return view('frontend.speakershub.speakers_details')->with([
            'datas' => $datas,
            'featured' => $featured,
            'casestudy'=>$casestudy,
            'ebook'=>$ebook,
            'latest'=>$latest
        ]);
       
	}
	public function ebookSpeakers(Request $request)
	{
		$latest=Speakershub::where('status','Published')->orderBy('created_at', 'desc')->limit(3)->get();
		$casestudy=Speakershub::where('category','CaseStudy')->count();
		$ebook=Speakershub::where('category','E-Books')->count();
		$featured=Speakershub::where('status','Legend')->get();
		$datas =Speakershub:: where('category','E-Books')->paginate(1); 
		if ($request->ajax()) {
            return view('frontend.speakershub.ebookresult')->with([
            'datas' => $datas,
            'featured' => $featured,
            'casestudy'=>$casestudy,
            'ebook'=>$ebook,
            'latest'=>$latest
        ]);
        }
		else {
        
        return view('frontend.speakershub.ebookSpeakers')->with([
            'datas' => $datas,
            'featured' => $featured,
            'casestudy'=>$casestudy,
            'ebook'=>$ebook,
            'latest'=>$latest
        ]);
       }
		
	}
	
	public function casestudySpeakers(Request $request)
	{
		$latest=Speakershub::where('status','Published')->orderBy('created_at', 'desc')->limit(3)->get();
		$casestudy=Speakershub::where('category','CaseStudy')->count();
		$ebook=Speakershub::where('category','E-Books')->count();
		$featured=Speakershub::where('status','Legend')->get();
		$datas =Speakershub:: where('category','CaseStudy')->where('status','Published')->paginate(1); 
		if ($request->ajax()) {
            return view('frontend.speakershub.ebookresult')->with([
            'datas' => $datas,
            'featured' => $featured,
            'casestudy'=>$casestudy,
            'ebook'=>$ebook,
            'latest'=>$latest
        ]);
        }
		else {
        
        return view('frontend.speakershub.casestudySpeakers')->with([
            'datas' => $datas,
            'featured' => $featured,
            'casestudy'=>$casestudy,
            'ebook'=>$ebook,
            'latest'=>$latest
        ]);
       }
	}
	
	public function speakerSearch(Request $request)
	{
	   $data=Input::get('search');
	   
	   $datas = Speakershub::where('title', 'LIKE', "%$data%")->orwhere('location',$data)->orwhere('author_name',$data)->orwhere('profession',$data)->get();
	   if ($request->ajax()) {
            return view('frontend.speakershub.searchresult')->with([
            'datas' => $datas
        ]);
        }
        else {
        
        return view('frontend.speakershub.search')->with([
            'datas' => $datas
        ]);
        }
	}
	

}
