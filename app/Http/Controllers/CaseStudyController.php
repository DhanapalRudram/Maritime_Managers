<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\CaseStudy;

class CaseStudyController extends Controller
{
    public function index(Request $request)
	  {
		
		$datas =DB::table('casestudy')->paginate(4);
        if ($request->ajax()) {
            return view('frontend.casestudy.result')->with([
            'datas' => $datas
        ]);
        }
        else {
        
        return view('frontend.casestudy.index')->with([
            'datas' => $datas
        ]);
        }
	  }
	
	
	public function Ebooks(Request $request)
	{
		
		$datas =CaseStudy:: where('category','E-Books')->where('status','Published')->paginate(4); 
		if ($request->ajax()) {
            return view('frontend.casestudy.ebookresult')->with([
            'datas' => $datas
        ]);
        }
		else {
        
        return view('frontend.casestudy.ebooks')->with([
            'datas' => $datas
        ]);
       }
		
	}
	
	public function caseStudies(Request $request)
	{
		
		$datas =CaseStudy:: where('category','CaseStudy')->paginate(4); 
		if ($request->ajax()) {
            return view('frontend.casestudy.ebookresult')->with([
            'datas' => $datas
        ]);
        }
		else {
        
        return view('frontend.casestudy.casestudies')->with([
            'datas' => $datas
        ]);
       }
	}
	
	public function getCasestudy($id)
	{
		  
		  $datas =CaseStudy:: where('casestudy_id',$id)->get();       
        
        return view('frontend.casestudy.casestudy_details')->with([
            'datas' => $datas
        ]);
       
	}
}
