<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Event;
use App\EventSlider;
use App\SubEvent;
class EventController extends Controller
{
    public function index(Request $request)
	{    
	     $event_id="";
	     $sliders=EventSlider::orderBy('id','desc')->limit(1)->get();
	     //print_r($sliders);exit;
		 $featured=Event::where('status','Published')->orderBy('id','desc')->paginate(1);
		 foreach($featured as $fea)
		 {
		     $event_id=$fea->event_id;
		 }
		 
		 $datas =SubEvent:: where('event_id',$event_id)->get();
		 
        if ($request->ajax()) {
            return view('frontend.events.result')->with([
            'datas' => $datas,
            'featured' => $featured
        ]);
        }
        else {
        
        return view('frontend.events.index')->with([
            'datas' => $datas,
            'featured' => $featured,
            'sliders'=>$sliders
        ]);
        }
	}
	
	public function getEvent($id)
	{
		 $featured=Event::where('status','Recent')->get();
		 $datas =Event:: where('event_id',$id)->get();       
        
        return view('frontend.events.event_details')->with([
            'datas' => $datas,
            'featured' => $featured
        ]);
       
	}
	public function getSubEvent($id)
	{
	    $datas =SubEvent::where('id',$id)->get();
	    
	    return view('frontend.events.sub_event_details')->with([
            'datas' => $datas
        ]);
	}
	public function eventSearch(Request $request)
	{
	   $data=Input::get('search');
	   $featured=Event::where('status','Recent')->get();
	   $datas = Event::where('title', 'LIKE', "%$data%")->orwhere('location',$data)->get();
	   if ($request->ajax()) {
            return view('frontend.events.searchresult')->with([
            'datas' => $datas,
            'featured' => $featured
        ]);
        }
        else {
        
        return view('frontend.events.search')->with([
            'datas' => $datas,
            'featured' => $featured
        ]);
        }
	}
}
