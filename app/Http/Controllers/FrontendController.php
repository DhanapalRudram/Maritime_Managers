<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Event;
use App\Speakershub;
use App\Blog;
use Mail;
use Response;
class FrontendController extends Controller
{
    public function index()
	{
		$events=Event::where('status','Recent')->get();
		$speaker=Speakershub::where('status','Published')->count();
		$blogs=Blog::where('status','Published')->orderby('created_at','desc')->limit(4)->get();
		$speakers=Speakershub::where('status','Published')->orderby('created_at','desc')->limit(4)->get();
		
		return view('frontend.index',['events'=>$events,'speaker'=>$speaker,'blogs'=>$blogs,'speakers'=>$speakers]);
	}
	
	public function about()
	{
		return view('frontend.about');
	}
	
	public function contact()
	{
		return view('frontend.contact');
	}
	
	public function faq()
	{
		return view('frontend.faq');
	}
	
	public function sendmail(Request $request)	
	{		
				$data=DB::table('contactus')->insert([
				    'email' => $request['email'],
				    'name' => $request['name'],
				    'message' =>$request['message']]);
				$data->save();
				
		$data =[
		       
		        'from' => $request['email'],
		        'name' => $request['name'],
		        'message' =>$request['message'],
		        ];
		        //echo $data['from'];exit;
				Mail::send($data,$data,function ($message) use ($data) {
				$message->From($data['from'], $data['name']);	
		        $message->Subject("Website Contact");
		        $message->to('dhanapalmca88@gmail.com','CEO');
		        });
				/*
		        Mail::queue('email.notify', $data, function ($message) use ($data) {		        
		        $message->Subject($data['subject']);
		       // $message->setBody($data['message']);
		        $message->to($data['to'], $data['name']);
		        });	
			
				$merge_vars = array('FNAME'=>$request['fname'],'LNAME'=>$request['lname'],'SUB'=>$request['subject'],'MSG'=>$request['message']);				
			 	$status = $this->mailchimp->lists->subscribe(
				$this->listId,
				 ['email' => $request['email']],
				 
                $merge_vars, // merge vars
                'html', // email type
                false, // requires double optin
                false, // update existing members
                true
                );*/
			 return Response::json('success', 200);
	}		
	public function subscribe(Request $request)	
	{		
			
		$data =[
		       
		        'from' => $request['email']
		        ];
		        
				Mail::queue($data,$data,function ($message) use ($data) {
				$message->From($data['from'], 'boopathi');	
		        $message->Subject("New Subscriber");
		        $message->to('dhanapalmca88@gmail.com','Dhanapal');
		        });
		        
		 return Response::json('success', 200);
	}
}
