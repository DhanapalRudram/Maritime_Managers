<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use File;
use App\Blog;
use App\CaseStudy;
use App\Speakershub;
use App\Jobportal;
use App\Event;
use Auth;
use App\Quiz;
use App\EventSlider;
use App\Questions;
use App\SubEvent;
use App\QuizUsers;
use App\Answers;
use Illuminate\Support\Facades\Input;
use Response;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
	{
		\Session::flash('flash_message','Welcome To Dashboard.');
	     $blogs=Blog::where('status','Published')->count();
		 $speakers=Speakershub::where('status','Published')->count();
		 $jobs=Jobportal::where('status','Published')->count();
		 $events=Event::where('status','Published')->count();
		return view('admin.index')->with(['blogs'=>$blogs,'speakers'=>$speakers,'jobs'=>$jobs,'events'=>$events]);
	}
//Blog Part	
	public function blog()
	{
		$datas=Blog::where('status','Published')->get();
		return view('admin.blog.index',['datas'=>$datas]);
	}
	
	public function addBlog()
	{
		$blog_id=str_random(10);	
		return view('admin.blog.addBlog',['blog_id'=>$blog_id]);
	}
	
	public function blogUpload()
	{
		$blog_id=Input::get('blog_id');
        $file = Input::file('image');
		//var_dump($file);exit;
		if($file!=null){
		$destinationPath = 'uploads/blog/author/'; // upload path
		$destinationPath1 = 'uploads/blog/'; // upload path
	    $fileName = Input::file('image')->getClientOriginalName();
		$fileName1 = Input::file('blog_image')->getClientOriginalName(); // getting file extension
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		$upload_path1 = Input::file('blog_image')->move($destinationPath1, $fileName1);
		//Deleting Data if same 		
		$data=Blog::where('blog_id',$blog_id)->delete();
		
		$data=new Blog();
		$data->category=Input::get('category');
		$data->author_name=Input::get('name');
		$data->title=Input::get('title');
		$data->author_image=$upload_path;
		$data->blog_image=$upload_path1;
		$data->status="Pending";
		$data->blog_id=Input::get('blog_id');
	    $data->description=Input::get('summary');
		//$data->email=Input::get('email');																																																																																																																																																																																																																																																																																																																																																																																																																	
		$data->save();
		$lastId=$data->blog_id;
		$datas=Blog::where('blog_id',$lastId)->get();
		//print_r($datas);
	    if ($upload_path) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		}
	}

	public function blogPublish()
	{
		$blog_id=Input::get('blog_id');
		$episode = Blog::where('blog_id',$blog_id)->update(['status' => 'Published']);
		return Response::json('success', 200);
	}
	
	public function deleteBlog()
	{
		$data=Blog::where('blog_id',Input::get('blog_id'))->delete();
		$data1=Blog::where('status','Published')->count();
		return Response::json($data1,200);
	}
	
	public function editBlog($id)
	{	
		//$id=Input::get('id');
		 $datas=Blog::where('blog_id',$id)->get();
		return view('admin.blog.edit_blog',['datas'=>$datas]);
	}
	
		public function blogUpdate()
	{
		$blog_id=Input::get('blog_id');
	 	$data=Blog::where('blog_id',$blog_id)->update(['author_name'=>Input::get('name'),'title'=>Input::get('title'),'description'=>Input::get('summary'),'status'=>'Published']);
	/*	$data->category=Input::get('category');
		$data->author_name=Input::get('name');
		$data->title=Input::get('title');
		$data->author_image=$upload_path;
		$data->blog_image=$upload_path1;
		$data->status="Pending";
		$data->blog_id=Input::get('blog_id');
	    $data->description=Input::get('summary');
		//$data->email=Input::get('email');																																																																																																																																																																																																																																																																																																																																																																																																																	
		$data->update();*/
	//	$lastId=$data->blog_id;
		$datas=Blog::where('blog_id',$blog_id)->get();
		//print_r($datas);
	    if ($datas) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		
	}
	
	public function blogUpdateDraft()
	{
		$blog_id=Input::get('blog_id');
	 	$data=Blog::where('blog_id',$blog_id)->update(['status'=>'Pending']);
	}
	public function selectBlog()
	{
		$datas=Blog::where('status','Pending')->get();
		return Response::json($datas,200);
	}
	
	public function selectallBlog()
	{
		$datas=Blog::where('status','Published')->get();
		return Response::json($datas,200);
	}
	public function featuredBlog () {
		$blog_id=Input::get('id');
		$sql=Blog::where('status','Featured')->get();
		
		if(count($sql)!=0)
		{	
			return Response::json('0', 200); 
			
		}
		else {
	 	$data=Blog::where('blog_id',$blog_id)->update(['status'=>'Featured']);
		return Response::json('1', 200); 
	 	}
	}
// Case Studies Part
	public function caseStudy()
	{
		$datas=CaseStudy::where('status','Published')->get();
		return view('admin.casestudies.index',['datas'=>$datas]);
	}
	public function addcaseStudy()
	{
		$casestudy_id=str_random(10);
		return view('admin.casestudies.addcaseStudy',['casestudy_id'=>$casestudy_id]);
	}
	public function caseStudyUpload()
	{
		$casestudy_id=Input::get('casestudy_id');
        $file = Input::file('image');
		//var_dump($file);exit;
		if($file!=null){
		$destinationPath = 'uploads/casestudy/author/'; // upload path
		$destinationPath1 = 'uploads/casestudy/'; // upload path
	    $fileName = Input::file('image')->getClientOriginalName();
		$fileName1 = Input::file('casestudy_image')->getClientOriginalName(); // getting file extension
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		$upload_path1 = Input::file('casestudy_image')->move($destinationPath1, $fileName1);
		//Deleting Data if same 		
		$data=CaseStudy::where('casestudy_id',$casestudy_id)->delete();
		
		$data=new CaseStudy();
		$data->category=Input::get('category');
		$data->author_name=Input::get('name');
		$data->title=Input::get('title');
		$data->author_image=$upload_path;
		$data->casestudy_image=$upload_path1;
		$data->status="Pending";
		$data->casestudy_id=Input::get('casestudy_id');
		$data->volume=Input::get('volume');
	    $data->description=Input::get('summary');
		//$data->email=Input::get('email');																																																																																																																																																																																																																																																																																																																																																																																																																	
		$data->save();
		$lastId=$data->casestudy_id;
		$datas=CaseStudy::where('casestudy_id',$lastId)->get();
		//print_r($datas);
	    if ($upload_path) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		}
	}
	
	
	public function caseStudyPublish()
	{
		$casestudy_id=Input::get('casestudy_id');
		$update= CaseStudy::where('casestudy_id',$casestudy_id)->update(['status' => 'Published']);
		return Response::json('success', 200);
	}
	
	public function deletecaseStudy()
	{
		$data=CaseStudy::where('casestudy_id',Input::get('casestudy_id'))->delete();
		$data1=CaseStudy::where('status','Published')->count();
		return Response::json($data1,200);
	}
	
	public function selectcaseStudy()
	{
		$datas=CaseStudy::where('status','Pending')->get();
		return Response::json($datas,200);
	}
	
	public function selectallcaseStudy()
	{
		$datas=CaseStudy::where('status','Published')->get();
		return Response::json($datas,200);
	}
	public function createLegend () 
	{
		$casestudy_id=Input::get('id');
		$sql=CaseStudy::where('status','Legend')->get();
		
		if(count($sql)!=0)
		{	
			return Response::json('0', 200); 
			
		}
		else {
	 	$data=CaseStudy::where('casestudy_id',$casestudy_id)->update(['status'=>'Legend']);
		return Response::json('1', 200); 
	 	}
	}
//Speakers Hub Part
	public function speakers()
	{
		$datas=Speakershub::where('status','Published')->get();
		return view('admin.speakershub.index',['datas'=>$datas]);
	}
	
	public function addSpeakers()
	{	
		$speakers_id=str_random(10);
		return view('admin.speakershub.addSpeakers',['speakers_id'=>$speakers_id]);
	}
	
	public function speakersUpload()
	{
		$speakers_id=Input::get('speakers_id');
        $file = Input::file('image');
		//var_dump($file);exit;
		
		if($file!=null){
		$destinationPath = 'uploads/speakershub/author/'; // upload path
		$destinationPath1 = 'uploads/speakershub/'; // upload path
	    $fileName = Input::file('image')->getClientOriginalName();
		$fileName1 = Input::file('speakers_image')->getClientOriginalName(); // getting file extension
	  
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		$upload_path1 = Input::file('speakers_image')->move($destinationPath1, $fileName1);
		
		//Deleting Data if same 		
		$data=Speakershub::where('speakers_id',$speakers_id)->delete();
		
		$data=new Speakershub();
		$data->category=Input::get('category');
		$data->author_name=Input::get('name');
		$data->title=Input::get('title');
		$data->start_date=Input::get('sdate');
		$data->end_date=Input::get('edate');
		$data->location=Input::get('location');
		$data->link=Input::get('link');
		$data->author_image=$upload_path;
		$data->speakers_image=$upload_path1;
		$data->status="Pending";
		$data->speakers_id=Input::get('speakers_id');
	    $data->description=Input::get('summary');																																																																																																																																																																																																																																																																																																																																																																																																															
		$data->save();
		$lastId=$data->speakers_id;
		$datas=Speakershub::where('speakers_id',$lastId)->get();
	    if ($upload_path) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		}
	}
	
	public function speakersPublish()
	{
		$speakers_id=Input::get('speakers_id');
		$update= Speakershub::where('speakers_id',$speakers_id)->update(['status' => 'Published']);
		return Response::json('success', 200);
	}
	
	public function deleteSpeakers()
	{	
		//$file=Input::get('speakers_id');
		File::Delete(Input::get('image'));
		$data=Speakershub::where('speakers_id',Input::get('speakers_id'))->delete();
		$data1=Speakershub::where('status','Published')->count();
		return Response::json($data1,200);
	}
	
	public function selectSpeakers()
	{
	   	$datas=Speakershub::where('status','Pending')->get();
	//	$data1=Speakershub::where('status','Published')->count();
		return Response::json($datas,200);
	}
	
	public function selectallSpeakers()
	{
		$datas=Speakershub::where('status','Published')->get();
		return Response::json($datas,200);
	}
	
	public function editSpeakers($id)
	{	
		//$id=Input::get('id');
		 $datas=Speakershub::where('speakers_id',$id)->get();
		return view('admin.speakershub.editSpeakers',['datas'=>$datas]);
	}
	
	public function speakersUpdate()
	{
		$blog_id=Input::get('speakers_id');
	 	$data=Speakershub::where('speakers_id',$blog_id)->update(['author_name'=>Input::get('name'),'title'=>Input::get('title'),'link'=>Input::get('link'),'description'=>Input::get('summary'),'status'=>'Published']);
	/*	$data->category=Input::get('category');
		$data->author_name=Input::get('name');
		$data->title=Input::get('title');
		$data->author_image=$upload_path;
		$data->blog_image=$upload_path1;
		$data->status="Pending";
		$data->blog_id=Input::get('blog_id');
	    $data->description=Input::get('summary');
		//$data->email=Input::get('email');																																																																																																																																																																																																																																																																																																																																																																																																																	
		$data->update();*/
	//	$lastId=$data->blog_id;
		$datas=Speakershub::where('speakers_id',$blog_id)->get();
		//print_r($datas);
	    if ($datas) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		
	}
	
	public function speakersUpdateDraft()
	{
		$blog_id=Input::get('speakers_id');
	 	$data=Speakershub::where('speakers_id',$blog_id)->update(['status'=>'Pending']);
	}
	
	public function createspeakerLegend () 
	{
		$peakers_id=Input::get('id');
		$sql=Speakershub::where('status','Legend')->get();
		
		if(count($sql)!=0)
		{	
			return Response::json('0', 200); 
			
		}
		else {
	 	$data=Speakershub::where('speakers_id',$peakers_id)->update(['status'=>'Legend']);
		return Response::json('1', 200); 
	 	}
	}
	
//JOB PORTAL PART START
	public function jobportal()
	{
		$datas=Jobportal::where('status','Published')->get();	
		return view('admin.jobportal.index',['datas'=>$datas]);
	}
	public function addJob()
	{	
		$job_id=str_random(10);
		return view('admin.jobportal.add_job',['job_id'=>$job_id]);
	}
	
	public function jobUpload()
	{
		$job_id=Input::get('job_id');
        $file = Input::file('image');
		//var_dump($file);exit;
		
		if($file!=null){
		$destinationPath = 'uploads/jobportal/logo/'; // upload path
		$destinationPath1 = 'uploads/jobportal/'; // upload path
	    $fileName = Input::file('image')->getClientOriginalName();
	 	$fileName1 = Input::file('company_image')->getClientOriginalName(); // getting file extension
	  
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		$upload_path1 = Input::file('company_image')->move($destinationPath1, $fileName1);
		
		//Deleting Data if same 		
		$data=Jobportal::where('job_id',$job_id)->delete();
		
		$data=new Jobportal();
		$data->category=Input::get('category');
		$data->company_name=Input::get('cname');
		$data->title=Input::get('title');
		$data->vacancies=Input::get('vacancies');
		$data->start_date=Input::get('sdate');
		$data->end_date=Input::get('edate');
		$data->location=Input::get('location');
		$data->link=Input::get('link');
		$data->company_image=$upload_path1;
		$data->company_logo=$upload_path;
		$data->status="Pending";
		$data->job_id=Input::get('job_id');
	    $data->description=Input::get('summary');																																																																																																																																																																																																																																																																																																																																																																																																															
		$data->save();
		$lastId=$data->job_id;
	//	print_r($lastId);exit;
		$datas=Jobportal::where('job_id',$lastId)->get();
	    if ($upload_path) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		}
	}
	public function jobPublish()
	{
		$job_id=Input::get('job_id');
		$update= Jobportal::where('job_id',$job_id)->update(['status' => 'Published']);
		return Response::json('success', 200);
	}
	public function deleteJob()
	{
		
		$data=Jobportal::where('job_id',Input::get('job_id'))->delete();
		$data1=Jobportal::where('status','Published')->count();
		return Response::json($data1,200);
	}
	public function selectdraftjob()
	{
	   	$datas=Jobportal::where('status','Pending')->get();
	//	$data1=Speakershub::where('status','Published')->count();
		return Response::json($datas,200);
	}
	
	public function selectallJob()
	{
		$datas=Jobportal::where('status','Published')->get();
		return Response::json($datas,200);
	}
	public function editJob($id)
	{	
		//$id=Input::get('id');
		 $datas=Jobportal::where('job_id',$id)->get();
		return view('admin.jobportal.edit_job',['datas'=>$datas]);
	}
	public function jobUpdate()
	{
		$job_id=Input::get('job_id');
	 	$data=Jobportal::where('job_id',$job_id)->update(['category'=>Input::get('category'),'company_name'=>Input::get('cname'),'title'=>Input::get('title'),'link'=>Input::get('link'),'vacancies'=>Input::get('vacancies'),'start_date'=>Input::get('sdate'),'end_date'=>Input::get('edate'),'description'=>Input::get('summary'),'status'=>'Published']);
		$datas=Jobportal::where('job_id',$job_id)->get();
		//print_r($datas);
	    if ($datas) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		
	}
	public function jobUpdateDraft()
	{
		$job_id=Input::get('job_id');
	 	$data=Speakershub::where('job_id',$job_id)->update(['status'=>'Pending']);
	}

//Event Manager Part
	public function eventManager()
	{
		$datas=Event::where('status','Published')->get();	
		return view('admin.eventmanager.index',['datas'=>$datas]);
	}
	
	public function addEvent()
	{
		$event_id=str_random(10);
		return view('admin.eventmanager.add_event',['event_id'=>$event_id]);
	}
	
	public function eventUpload()
	{
		$event_id=Input::get('event_id');
        $file = Input::file('image');
		//var_dump($file);exit;
		
		if($file!=null){
		$destinationPath = 'uploads/events/logo/'; // upload path
		$destinationPath1 = 'uploads/events/'; // upload path
	    $fileName = Input::file('image')->getClientOriginalName();
		$fileName1 = Input::file('event_image')->getClientOriginalName(); // getting file extension
	  
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		$upload_path1 = Input::file('event_image')->move($destinationPath1, $fileName1);
		
		//Deleting Data if same 		
		$data=Event::where('event_id',$event_id)->delete();
		
		$data=new Event();
		$data->event_id=Input::get('event_id');
		$data->title=Input::get('title');
		$data->company_image=$upload_path;
		$data->location=Input::get('location');
		$data->start_date=Input::get('sdate');
		$data->end_date=Input::get('edate');
		$data->price=Input::get('price');
		$data->members_limit=Input::get('limit');
		$data->last_date=Input::get('lastdate');		
		$data->company_name=Input::get('cname');
		$data->tickets=Input::get('tickets');		
		$data->event_image=$upload_path1;		
		$data->description=Input::get('summary');	
		$data->status="Pending";	    																																																																																																																																																																																																																																																																																																																																																																																																														
		$data->save();
		$lastId=$data->event_id;
		$datas=Event::where('event_id',$lastId)->get();
	    if ($upload_path) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		}
	}
	
	public function eventPublish()
	{
		$event_id=Input::get('event_id');
		$update= Event::where('event_id',$event_id)->update(['status' => 'Published']);
		return Response::json('success', 200);
	}
	
	public function selectdraftEvent()
	{
	   	$datas=Event::where('status','Pending')->get();
	//	$data1=Speakershub::where('status','Published')->count();
		return Response::json($datas,200);
	}
	
	public function selectallEvent()
	{
		$datas=Event::where('status','Published')->get();
		return Response::json($datas,200);
	}
	public function deleteEvent()
	{
		$data=Event::where('event_id',Input::get('event_id'))->delete();
		$data1=Event::where('status','Published')->count();
		return Response::json($data1,200);
	}
	public function editEvent($id)
	{	
		//$id=Input::get('id');
		 $datas=Event::where('event_id',$id)->get();
		return view('admin.eventmanager.edit_event',['datas'=>$datas]);
	}
	public function eventUpdate()
	{
		$event_id=Input::get('event_id');
	 	$data=Event::where('event_id',$event_id)->update(['company_name'=>Input::get('cname'),'title'=>Input::get('title'),'tickets'=>Input::get('tickets'),'start_date'=>Input::get('sdate'),'end_date'=>Input::get('edate'),'last_date'=>Input::get('latdate'),'tickets'=>Input::get('tickets'),'members_limit'=>Input::get('limit'),'description'=>Input::get('summary'),'status'=>'Published']);
		$datas=Event::where('event_id',$event_id)->get();
		//print_r($datas);
	    if ($datas) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		
	}
	public function eventDraft()
	{
		$event_id=Input::get('event_id');
	 	$data=Event::where('event_id',$event_id)->update(['status'=>'Pending']);
	}
	
	public function createeventRecent () 
	{
		$event_id=Input::get('id');
		$sql=Event::where('status','Recent')->get();
		
		if(count($sql)!=0)
		{	
			return Response::json('0', 200); 
			
		}
		else {
	 	$data=Event::where('event_id',$event_id)->update(['status'=>'Recent']);
		return Response::json('1', 200); 
	 	}
	}
	
	public function subEvent($id)
	{	
		//$datas=Event::where('event_id',$id)->get();
		$events=SubEvent::where('event_id',$id)->get();
		return view('admin.eventmanager.subevents.index',['id'=>$id,'events'=>$events]);
	}
	public function addSubEvent($id)
	{	
		
		return view('admin.eventmanager.subevents.addsubevent',['event_id'=>$id]);
	}
	
	public function subeventUpload()
	{
		$file = Input::file('event_image');
		//var_dump($file);exit;
		
		if($file!=null){
		$destinationPath = 'uploads/events/subevents'; // upload path

		$fileName = Input::file('event_image')->getClientOriginalName(); // getting file extension
	  
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('event_image')->move($destinationPath, $fileName); // uploading file to given path
		
		
		$data=new SubEvent();
		$data->event_id=Input::get('event_id');
		$data->event_name=Input::get('title');
		$data->event_image=$upload_path;
		$data->location=Input::get('location');
		$data->start_date=Input::get('sdate');
		$data->end_date=Input::get('edate');
		$data->description=Input::get('summary');
		$data->status="Published";	    																																																																																																																																																																																																																																																																																																																																																																																																														
		$data->save();
	   	return redirect('/eventManager');
		}
	
	}
	public function deletesubEvent()
	{
		$data=SubEvent::where('id',Input::get('id'))->delete();
		$data1=SubEvent::where('status','Published')->count();
		return Response::json($data1,200);
	}
	
	public function eventSlider()
	{
		$datas=EventSlider::all();
		return view('admin.eventmanager.eventslider',['datas'=>$datas]);
	}
	
	public function addEventSlider()
	{
		return view('admin.eventmanager.addeventslider');
	}
	public function eventSliderUpload()
	{
			
        $file = Input::file('image2');
		//var_dump($file);exit;
		
		if($file!=null){
		$destinationPath = 'uploads/events/slider/'; // upload path
	    $fileName = Input::file('image')->getClientOriginalName();
		$fileName1 = Input::file('image1')->getClientOriginalName(); // getting file extension
		$fileName2 = Input::file('image2')->getClientOriginalName();
		$fileName3 = Input::file('image3')->getClientOriginalName(); 
		$fileName4 = Input::file('image4')->getClientOriginalName();
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		$upload_path1 = Input::file('image1')->move($destinationPath, $fileName1);
		$upload_path2 = Input::file('image2')->move($destinationPath, $fileName2); // uploading file to given path
		$upload_path3 = Input::file('image3')->move($destinationPath, $fileName3);
		$upload_path4 = Input::file('image4')->move($destinationPath, $fileName4); 
	
		$data=new EventSlider();
		$data->image=$upload_path;
		$data->image1=$upload_path1;
		$data->image2=$upload_path2;
		$data->image3=$upload_path3;
		$data->image4=$upload_path4;
						
		$data->save();
		}
		return redirect('/eventSlider');
	   
	}
	
	
	public function deleteEventSlider()
	{
		$data=EventSlider::where('id',Input::get('id'))->delete();
		$data1=EventSlider::all()->count();
		return Response::json($data1,200);
	}
///Quiz Part
	public function quiz()
	{
		$datas=Quiz::where('status','Published')->get();
		return view('admin.quiz.index',['datas'=>$datas]);
	}
	public function addQuiz()
	{	
		$quiz_id=str_random(8);
		
		return view('admin.quiz.add_quiz',['quiz_id'=>$quiz_id]);
	}
	public function quizUpload()
	{
		$quiz_id=Input::get('quiz_id');
        $file = Input::file('image');
		//var_dump($file);exit;
		
		if($file!=null){
		$destinationPath = 'public/uploads/quiz/'; // upload path
	//	$destinationPath1 = 'uploads/events/'; // upload path
	    $fileName = Input::file('image')->getClientOriginalName();
	//	$fileName1 = Input::file('event_image')->getClientOriginalName(); // getting file extension
	  
	   // $fileName = $name; // renaming image
	    $upload_path = Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
	//	$upload_path1 = Input::file('event_image')->move($destinationPath1, $fileName1);
		
		//Deleting Data if same 		
		$data=Quiz::where('quiz_id',$quiz_id)->delete();
		
		$data=new Quiz();
		$data->quiz_id=Input::get('quiz_id');
		$data->title=Input::get('title');
		$data->category=Input::get('category');
		$data->quiz_image=$upload_path;
		$data->no_of_questions=Input::get('questions');
		$data->duration=Input::get('duration');
		$data->time_limit_questions=Input::get('time_perquestion');
		$data->description=Input::get('summary');	
		$data->status="Pending";	    																																																																																																																																																																																																																																																																																																																																																																																																														
		$data->save();
		$lastId=$data->quiz_id;
		$datas=Quiz::where('quiz_id',$lastId)->get();
	    if ($upload_path) 
		    {
		        return Response::json($datas, 200); 
		    }
	    else 
		    {
		       return Response::json('error', 400);
		    }
		}
	}
	
	public function quizPublish()
	{
		$quiz_id=Input::get('quiz_id');
		$update= Quiz::where('quiz_id',$quiz_id)->update(['status' => 'Published']);
		return Response::json('success', 200);
	}
	
	public function deleteQuiz()
	{
		$data=Quiz::where('quiz_id',Input::get('quiz_id'))->delete();
		$data1=Quiz::where('status','Published')->count();
		return Response::json($data1,200);
	}
	public function addQuestions($id)
	{
		$datas=Questions::where('quiz_id',$id)->get();
		return view('admin.quiz.add_questions',['datas'=>$datas]);
	}
	
	public function questionsInsert()
	{
		$data=new Questions();
		$data->quiz_id=Input::get('quiz_id');
		$data->question=Input::get('question');
		$data->option1=Input::get('op1');
		$data->option2=Input::get('op2');
		$data->option3=Input::get('op3');
		$data->answer=Input::get('ans');
		$data->status='Published';
		$data->save();
		//print_r($data);exit;
	//	$lastId->$data->quiz_id;
	//	$datas=Questions::where('quiz_id',$lastId)->get();
	   
	}
	public function questionsUpdate()
	{
		$question_id=Input::get('question_id');
	 	$data=Questions::where('id',$question_id)->update(['question'=>Input::get('question'),'option1'=>Input::get('op1'),'option2'=>Input::get('op2'),'option3'=>Input::get('op3'),'answer'=>Input::get('ans')]);
		
	}
	
//Quiz Report Part
	public function quizReport()
	{	
		
		$datas=QuizUsers::all();
		return view('admin.quizreports.index',['datas'=>$datas ]);
	}
	
	public function quizList($email)
	{
	
		$datas=QuizUsers::where('email',$email)->get();
		
		return view('admin.quizreports.quizlist',['datas'=>$datas]);
	}
	
	public function quizView($id)
	{	
		$score="";
		$res=explode("-",$id);
		$quiz_id=$res[0];
		$user_id=$res[1];
		$datas=Quiz::where('quiz_id',$quiz_id)->get();
		$questions=Questions::where('quiz_id',$quiz_id)->get();
		$count=Questions::where('quiz_id',$quiz_id)->count();
	//	echo "<pre>";print_r($questions);exit;
	
		$results=Answers::where('quiz_id',$quiz_id)->where('user_id',$user_id)->get();
		//echo $results;exit;
		foreach($questions as $ques)
		{
			foreach($results as $res)
			{
				if($ques->answer==$res->user_choice)
				{
					$score++;
				}
			}
		}
		
		
		return view('admin.quizreports.quizview',['datas'=>$datas,'results'=>$results,'questions'=>$questions,'score'=>$score,'count'=>$count]);
	}
}
