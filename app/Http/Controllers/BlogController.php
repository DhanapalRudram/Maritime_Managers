<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Blog;
use App\Comments;
use Response;
class BlogController extends Controller
{
    public function index(Request $request)
	{
		//$datas=Blog::where('status','Published')->paginate(2);
		//print_r($datas);exit;
		//return view('frontend.blog.index',['datas'=>$datas]);
		 $featured=Blog::where('status','Featured')->get();
		  $datas =Blog:: where('status','Published')->paginate(2);
        if ($request->ajax()) {
            return view('frontend.blog.presult')->with([
            'datas' => $datas,
            'featured' => $featured
        ]);
        }
        else {
        
        return view('frontend.blog.index')->with([
            'datas' => $datas,
            'featured' => $featured
        ]);
        }
	}
	
	public function getBlog(Request $request,$id)
	{	
		 $coments=Comments::where('blog_id',$id)->paginate(1);
		 $featured=Blog::where('status','Featured')->get();
		 $datas =Blog:: where('blog_id',$id)->get();   
		 
		 if ($request->ajax()) {
             return view('frontend.blog.comments')->with([
            'datas' => $datas,
            'featured' => $featured,
            'comments'=>$coments
         ]);
        }
        else {
        
        return view('frontend.blog.blog_details')->with([
            'datas' => $datas,
            'featured' => $featured,
            'comments'=>$coments
        ]);
        }
	        
        
        
       
	}
	public function insertComments()
	{
	    $data=new Comments();
	    $data->blog_id=Input::get('blog_id');
	    $data->name=Input::get('name');
	    $data->email=Input::get('email');
	    $data->message=Input::get('message');
	    $data->save();
	    return Response::json('success', 200);
	}
}
