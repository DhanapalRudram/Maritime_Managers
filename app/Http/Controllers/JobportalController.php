<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Jobportal;
use Illuminate\Support\Facades\Input;
class JobportalController extends Controller
{
    public function index(Request $request)
	  {
		//$latest=Jobportal::where('status','Published')->orderBy('created_at', 'desc')->limit(3)->get();
		//$Jobportal=Jobportal::where('category','Jobportal')->count();
		//$ebook=Jobportal::where('category','E-Books')->count();
		//$featured=Jobportal::where('status','Legend')->get();
		//$datas =Jobportal:: where('status','Published')->paginate(2);
		$datas =DB::table('jobportal')->paginate(4);
        if ($request->ajax()) {
            return view('frontend.jobportal.result')->with([
            'datas' => $datas
            //'featured' => $featured,
           // 'Jobportal'=>$Jobportal,
           // 'ebook'=>$ebook,
            //'latest'=>$latest
        ]);
        }
        else {
        
        return view('frontend.jobportal.index')->with([
            'datas' => $datas
           // 'featured' => $featured,
            //'Jobportal'=>$Jobportal,
           // 'ebook'=>$ebook,
            //'latest'=>$latest
        ]);
        }
	  }
	
	public function jobDetails($id)
	{
		  $datas =Jobportal:: where('job_id',$id)->get();
		  return view('frontend.jobportal.job_details')->with([
            'datas' => $datas
        ]);
       
	}

	public function jobApplication()
	{
	    $data=new Comments();
	    $data->blog_id=Input::get('blog_id');
	    $data->name=Input::get('name');
	    $data->email=Input::get('email');
	    $data->message=Input::get('message');
	    $data->save();
	    return Response::json('success', 200);
	}
	
	public function jobSearch(Request $request)
	{
	   $data=Input::get('search');
	   
	   $datas = Jobportal::where('title', 'LIKE', "%$data%")->orwhere('location',$data)->orwhere('company_name',$data)->get();
	   if ($request->ajax()) {
            return view('frontend.jobportal.searchresult')->with([
            'datas' => $datas
        ]);
        }
        else {
        
        return view('frontend.jobportal.search')->with([
            'datas' => $datas
        ]);
        }
	}
}

