<?php


Route::auth();

//Admin Part

	Route::group(['middleware' => ['web']], function () {
		
	Route::get('/','AdminController@index');
	
	Route::get('/blog','AdminController@blog');
// Blog Part
	Route::get('/Blog_details','BlogController@getBlog');
	Route::get('/addBlog','AdminController@addBlog');
	Route::post('/blogUpload','AdminController@blogUpload');
	Route::post('/blogPublish','AdminController@blogPublish');
	Route::delete('/deleteBlog','AdminController@deleteBlog');
	Route::post('/selectBlog','AdminController@selectBlog');
	Route::post('/selectallBlog','AdminController@selectallBlog');
	Route::get('/editBlog/{id}','AdminController@editBlog');
	Route::post('/blogUpdate','AdminController@blogUpdate');
	Route::post('/blogUpdateDraft','AdminController@blogUpdateDraft');
	Route::post('/featuredBlog','AdminController@featuredBlog');
//Digital Library Part
	Route::get('/caseStudy','AdminController@caseStudy');
	Route::get('/addcaseStudy','AdminController@addcaseStudy');
	Route::post('/caseStudyUpload','AdminController@caseStudyUpload');
	Route::post('/caseStudyPublish','AdminController@caseStudyPublish');
	Route::delete('/deletecaseStudy','AdminController@deletecaseStudy');
	Route::post('/selectcaseStudy','AdminController@selectcaseStudy');
	Route::post('/selectallcaseStudy','AdminController@selectallcaseStudy');
	Route::post('/createLegend','AdminController@createLegend');
	
//Speakers Hub Part
	Route::get('/speakershub','AdminController@speakers');
	Route::get('/addSpeakers','AdminController@addSpeakers');
	Route::post('/speakersUpload','AdminController@speakersUpload');
	Route::post('/speakersPublish','AdminController@speakersPublish');
	Route::delete('/deleteSpeakers','AdminController@deleteSpeakers');
	Route::post('/selectSpeakers','AdminController@selectSpeakers');
	Route::post('/selectallSpeakers','AdminController@selectallSpeakers');
	Route::get('/editSpeakers/{id}','AdminController@editSpeakers');
	Route::post('/speakersUpdate','AdminController@speakersUpdate');
	Route::post('/speakersUpdateDraft','AdminController@speakersUpdateDraft');
	Route::post('/createspeakerLegend','AdminController@createspeakerLegend');
	
//JOB PORTAL PART
	Route::get('/jobportal','AdminController@jobportal');
	Route::get('/addJob','AdminController@addJob');
	Route::post('/jobUpload','AdminController@jobUpload');
	Route::post('/jobPublish','AdminController@jobPublish');
	Route::delete('/deleteJob','AdminController@deleteJob');
	Route::post('/selectdraftjob','AdminController@selectdraftjob');
	Route::post('/selectallJob','AdminController@selectallJob');
	Route::get('/editJob/{id}','AdminController@editJob');
	Route::post('/jobUpdate','AdminController@jobUpdate');
	Route::post('/jobUpdateDraft','AdminController@jobUpdateDraft');
	Route::post('/jobPublish','AdminController@jobPublish');
	
//Event Manager Part
	Route::get('/eventManager','AdminController@eventManager');
	Route::get('/addEvent','AdminController@addEvent');
	Route::post('/eventUpload','AdminController@eventUpload');
	Route::post('/eventPublish','AdminController@eventPublish');
	Route::post('/selectdraftEvent','AdminController@selectdraftEvent');
	Route::post('selectallEvent','AdminController@selectallEvent');
	Route::delete('/deleteEvent','AdminController@deleteEvent');
	Route::get('/editEvent/{id}','AdminController@editEvent');
	Route::post('/eventUpdate','AdminController@eventUpdate');
	Route::post('/eventDraft','AdminController@eventDraft');
	Route::post('/createeventRecent','AdminController@createeventRecent');
	Route::get('/subEvent/{id}','AdminController@subEvent');
	Route::get('/addSubEvent/{id}','AdminController@addSubEvent');
	Route::post('/subeventUpload','AdminController@subeventUpload');
	Route::delete('/deletesubEvent','AdminController@deletesubEvent');
	
	Route::get('/eventSlider','AdminController@eventSlider');
	Route::get('addEventSlider','AdminController@addEventSlider');
	Route::post('/eventSliderUpload','AdminController@eventSliderUpload');
	Route::delete('/deleteEventSlider','AdminController@deleteEventSlider');
	
//Quiz Part
	Route::get('/quiz','AdminController@quiz');
	Route::get('/addQuiz','AdminController@addQuiz');
	Route::post('/quizUpload','AdminController@quizUpload');
	Route::post('/quizPublish','AdminController@quizPublish');
	Route::delete('/deleteQuiz','AdminController@deleteQuiz');
	Route::get('/addQuestions/{id}','AdminController@addQuestions');
//Questions Part
	Route::post('/questionsInsert','AdminController@questionsInsert');
	Route::post('/questionsUpdate','AdminController@questionsUpdate');

//Quiz Report Part
	Route::get('/quizReport','AdminController@quizReport');
	Route::get('/quizList/{email}','AdminController@quizList');
	Route::get('/quizView/{id}','AdminController@quizView');
});
// FrontEnd Part

	Route::get('/Frontend','FrontendController@index');	
	Route::get('/About','FrontendController@about');
	Route::get('/Contact','FrontendController@contact');
	Route::get('Faq','FrontendController@faq');
	Route::post('subscribe',['as'=>'subscribe','uses'=>'FrontendController@subscribe']);
	Route::post('sendmail',['as'=>'sendCompaign','uses'=>'FrontendController@sendmail']);
	
//Blog Part
	Route::get('/Blog','BlogController@index');
	Route::get('getBlog/{id}','BlogController@getBlog');
	Route::post('/insertComments','BlogController@insertComments');
//Case Study part
	Route::get('/CaseStudy','CaseStudyController@index');
	Route::get('/getCasestudy/{id}','CaseStudyController@getCasestudy');
	Route::get('/Ebooks','CaseStudyController@Ebooks');
	Route::get('/caseStudies','CaseStudyController@caseStudies');
//Speakershub Part
	Route::get('/Speakershub','SpeakershubController@index');
	Route::get('/getSpeakers/{id}','SpeakershubController@getSpeakers');
	Route::get('/ebookSpeakers','SpeakershubController@ebookSpeakers');
	Route::get('/casestudySpeakers','SpeakershubController@casestudySpeakers');
	Route::post('/speakerSearch','SpeakershubController@speakerSearch');
// JOB PORTAL PART
	Route::get('/Jobportal','JobportalController@index');
	Route::get('/jobDetails/{id}','JobportalController@jobDetails');
	Route::post('/jobApplication','JobportalController@jobApplication');
	Route::post('/jobSearch','JobportalController@jobSearch');
// EVENTS Part
	Route::get('/Events','EventController@index');
	Route::get('/getEvent/{id}','EventController@getEvent');
	Route::post('/eventBooking','EventController@eventBooking');
	Route::post('/eventSearch','EventController@eventSearch');
	Route::get('/getSubEvent/{id}','EventController@getSubEvent');
//Paypal Part
	Route::get('payPremium', ['as'=>'payPremium','uses'=>'PaypalController@payPremium']);

    Route::post('getCheckout', ['as'=>'getCheckout','uses'=>'PaypalController@getCheckout']);

    Route::get('getDone', ['as'=>'getDone','uses'=>'PaypalController@getDone']);

    Route::get('getCancel', ['as'=>'getCancel','uses'=>'PaypalController@getCancel']);
    
    
    Route::post('/send','PayumoneyController@bookingconfirm');
    Route::post('/paymentsuccess','PayumoneyController@paymentsuccess');
    Route::post('/paymentfailure','PayumoneyController@paymentfailure');
// QUIZ Part
	Route::get('/Quiz','QuizController@index');
	Route::get('/getQuiz/{id}','QuizController@getQuiz');
	Route::get('/playQuiz/{id}','QuizController@playQuiz');
	Route::post('/insertQuizUsers','QuizController@insertQuizUsers');
	Route::post('/insertAnswers','QuizController@insertAnswers');
	Route::post('/quizResults','QuizController@quizResults');
	
// MailChimp Part
	