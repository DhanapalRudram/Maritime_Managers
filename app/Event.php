<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SubEvent;
class Event extends Model
{
    public function subEvent()
    {
       return $this->hasMany('App\SubEvent','event_id');
    }
}
