<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSlider extends Model
{
    protected $table='event_slider';
}
