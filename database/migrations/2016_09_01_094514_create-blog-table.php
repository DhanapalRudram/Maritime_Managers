<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
			$table->string('blog_id');
			$table->string('category');
            $table->string('title');
            $table->string('author_image');
            $table->string('blog_image');
			$table->string('description');
			$table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog');
    }
}
