<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
			$table->string('event_id');
            $table->string('title');
            $table->string('company_image');
			$table->string('location');
			$table->string('start_date');
			$table->string('end_date');
			$table->string('price');			
            $table->string('members_limit');
			$table->string('last_date');
			$table->string('event_image');
			$table->string('description');
			$table->string('status');			
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
