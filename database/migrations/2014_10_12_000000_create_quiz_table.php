<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz', function (Blueprint $table) {
            $table->increments('id');
			$table->string('quiz_id');
			$table->string('category');
            $table->string('title');
            $table->string('quiz_image');
            $table->integer('no_of_questions');
            $table->string('duration');
            $table->string('time_limit_questions');
			$table->text('description');
			$table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quiz');
    }
}
