<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobportalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobportal', function (Blueprint $table) {
            $table->increments('id');
			$table->string('job_id');
			$table->string('category');
	        $table->string('title');
	        $table->string('logo');
	        $table->string('company_image');
			$table->string('description');
			$table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobportal');
    }
}
