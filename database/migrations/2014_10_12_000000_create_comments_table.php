<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
			$table->string('blog_id');
            $table->string('name');
            $table->string('email');
			$table->text('comment');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::drop('comments');
    }
}
