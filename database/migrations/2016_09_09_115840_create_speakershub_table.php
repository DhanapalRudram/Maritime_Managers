<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeakershubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakershub', function (Blueprint $table) {
            $table->increments('id');
			$table->string('speakers_id');
			$table->string('category');
            $table->string('title');
            $table->string('author_image');
            $table->string('speakers_image');
			$table->string('description');
			$table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('speakershub');
    }
}
